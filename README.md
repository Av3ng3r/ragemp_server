### Avenger's RAGEMP server

An MIT licensed ragemp server. Do note that this has been an experiment of mine since the
8th of April 2019. As I've experimented, I've added and disabled code. There is much that
has been disabled - syncing between different ragemp instances as an example. I suppose I
should have cleaned up the code.

### building
npm install
npm run build

### installing
copy the dist files to the correct locations in the ragmp server directory.

### conf.json
there are extra options to be added to the ragemp conf.json file:

- "datadir"
	This directory will contain the user data files.
