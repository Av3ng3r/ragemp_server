(function() {
	mp.av = {};

	require("./modules/logger.js");
	const fs = require("fs");
	const config = require("./config.js");
	mp.log("config = ", config);

	if (mp.config.name.includes('1.41')) {
		mp.config.version = "1.41";
	} else {
		mp.config.version = "1.46";
	}

	require("./data/handling");
	// cannot use console at least in wine as it breaks the network
	// require("./modules/console");
	require("./modules/anticheat");
	require("./modules/minigame");
	require("./modules/crypt");
	require("./modules/ack");
	require("./modules/hospitals");
	require("./modules/vehicles");
	require("./modules/user");
	require("./modules/chat");
	require("./modules/parachute");
	require("./modules/watchdog");
	require("./modules/weather");
	require("./modules/rewards");
	require("./modules/admin");
	require("./modules/games/bounty");

	mp.events.add("playerJoin", (player) => {
		player.dimension = 69;
		player.alpha = 0;
	});
})();
