(function() {
	const fs = require("fs");

	mp.av.handling = {};

	mp.av.handling.getHandlingFor = (name) => {
		const filename = __dirname + `/handling/${name.toUpperCase()}.json`;
		try {
			const data = fs.readFileSync(filename);
			return JSON.parse(data);
		} catch (ex) {}
	};
})();
