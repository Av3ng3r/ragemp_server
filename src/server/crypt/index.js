// borrowed from rage complex and modified for my needs
var Base64 = require("./base64.js");
var Utf8 = require("./utf8.js");

mp.crypt = {};

mp.crypt.encrypt = (a, b) => {
	if(0==a.length) return '';
	var c=mp.crypt.strToLongs(Utf8.encode(a));
	1 >= c.length && (c[1]=0);
	for (var d,f,e=mp.crypt.strToLongs(Utf8.encode(b).slice(0,16)),g=c.length,h=c[g-1],i=c[0],j=Math.floor(6+52/g),k=0;0<j--;) {
		k+=2654435769,f=3&k>>>2;
		for (var l=0;l<g;l++) {
			i=c[(l+1)%g],d=(h>>>5^i<<2)+(i>>>3^h<<4)^(k^i)+(e[3&l^f]^h),h=c[l]+=d
		}
	}
	var m=mp.crypt.longsToStr(c);
	return Base64.encode(m)
};

mp.crypt.decrypt = (a, b) => {
	if (0==a.length) return '';
	for(var c,d,e=mp.crypt.strToLongs(Base64.decode(a)),f=mp.crypt.strToLongs(Utf8.encode(b).slice(0,16)),g=e.length,h=e[g-1],i=e[0],j=2654435769,k=Math.floor(6+52/g),l=k*j;0!=l;){
		d=3&l>>>2;
		for(var m=g-1;0<=m;m--)h=e[0<m?m-1:g-1],c=(h>>>5^i<<2)+(i>>>3^h<<4)^(l^i)+(f[3&m^d]^h),i=e[m]-=c;l-=j
	}
	var n=mp.crypt.longsToStr(e);
	return n=n.replace(/\0+$/,''),Utf8.decode(n)
};

mp.crypt.strToLongs = (a) => {
	for(var b=Array(Math.ceil(a.length/4)),c=0;c<b.length;c++)b[c]=a.charCodeAt(4*c)+(a.charCodeAt(4*c+1)<<8)+(a.charCodeAt(4*c+2)<<16)+(a.charCodeAt(4*c+3)<<24);return b
};

mp.crypt.longsToStr = (b) => {
	for(var c=Array(b.length),a=0;a<b.length;a++)c[a]=String.fromCharCode(255&b[a],255&b[a]>>>8,255&b[a]>>>16,255&b[a]>>>24);return c.join('')
}
