exports.encode=(a)=>{
	var b=String.fromCharCode,c=(a+"").replace(/[\u0080-\u07ff]/g,(a)=>{
		var c=a.charCodeAt(0);
		return b(192|c>>6,128|63&c)
	});
	return c=c.replace(/[\u0800-\uffff]/g,(a)=>{
		var c=a.charCodeAt(0);
		return b(224|c>>12,128|63&c>>6,128|63&c)
	}),c
};

exports.decode=(a)=>{
	var b=String.fromCharCode,c=a.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,(a)=>{var c=(15&a.charCodeAt(0))<<12|(63&a.charCodeAt(1))<<6|63&a.charCodeAt(2);return b(c)});return c=c.replace(/[\u00c0-\u00df][\u0080-\u00bf]/g,(a)=>{var c=(31&a.charCodeAt(0))<<6|63&a.charCodeAt(1);return b(c)}),c
};
