var Utf8 = require("./utf8.js");

exports.code='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

exports.encode = (a, b) => {
	b='undefined'!=typeof b&&b;let d,f,g,h,i,j,k,l,m,c,n,o=[],e='',p=this.code;if(c=b?Utf8.encode(a):a,m=c.length%3,0<m)for(;3>m++;)e+='=',c+='\0';for(m=0;m<c.length;m+=3)d=c.charCodeAt(m),f=c.charCodeAt(m+1),g=c.charCodeAt(m+2),h=d<<16|f<<8|g,i=63&h>>18,j=63&h>>12,k=63&h>>6,l=63&h,o[m/3]=p.charAt(i)+p.charAt(j)+p.charAt(k)+p.charAt(l);return n=o.join(''),n=n.slice(0,n.length-e.length)+e,n};

exports.decode = (a, b) => {
	var e=String.fromCharCode;b='undefined'!=typeof b&&b;let f,g,h,i,j,k,l,m,n,o,p=[],d=this.code;o=b?Utf8.decode(a):a;for(var q=0;q<o.length;q+=4)i=d.indexOf(o.charAt(q)),j=d.indexOf(o.charAt(q+1)),k=d.indexOf(o.charAt(q+2)),l=d.indexOf(o.charAt(q+3)),m=i<<18|j<<12|k<<6|l,f=255&m>>>16,g=255&m>>>8,h=255&m,p[q/4]=e(f,g,h),64==l&&(p[q/4]=e(f,g)),64==k&&(p[q/4]=e(f));return n=p.join(''),b?Utf8.decode(n):n
};
