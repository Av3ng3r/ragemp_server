(function() {
	mp.events.add("onPlayerParachute", (player, state) => {
		try {
			if (+state === -1) {
				if (player.getVariable("parachuteState") != -1) {
					mp.players.call("removeParachuteFor", [player.id]);
					player.setVariable("fallingFixed", false);
					player.setVariable("parachuteFixed", false);
				}
			} else {
				if (+state === 0 && !player.getVariable("fallingFixed")) {
					player.setVariable("fallingFixed", true);
					mp.players.call("fixFallingFor", [player.id]);
				}
				if (+state === 1 && !player.getVariable("parachuteFixed")) {
					player.setVariable("parachuteFixed", true);
					mp.players.call("fixParachuteFor", [player.id]);
				}
			}
			player.setVariable("parachuteState", state);
		} catch (ex) {
			mp.log("onPlayerParachute ex: " + ex.message);
		}
	});

	mp.events.add("playerJoin", (player) => {
		try {
			player.setVariable("parachuteState", -1);
			player.setVariable("fallingFixed", false);
			player.setVariable("parachuteFixed", false);
		} catch (ex) {
			mp.log("playerJoin parachute ex: " + ex.message);
		}
	});

	mp.events.add("playerQuit", (player) => {
		try {
			mp.players.call("removeParachuteFor", [player.id]);
		} catch (ex) {
			mp.log("playerJoin parachute ex: " + ex.message);
		}
	});
})();
