(function() {
	const fs = require("fs");

	let DATADIR=mp.config.version === "1.46" ? "/opt/ragemp-1.46" : "/opt/ragemp-1.41";
	if (mp.config.datadir) DATADIR=mp.config.datadir;

	const readStore = (path) => {
		let store = null;
		const fullPath = `${DATADIR}/${path}`;

		try {
			let data = fs.readFileSync(fullPath, "utf-8");
			if (data[0] === "{") try {
				store = JSON.parse(data);
				data = mp.crypt.encrypt(data, "readFileSync");
				fs.writeFileSync(fullPath, data);
			} catch (ex) { }
			if (store === null) store = JSON.parse(mp.crypt.decrypt(data, "readFileSync"));
		} catch (ex) {
			store = { path, data: {} };
		}
		return store;
	}

	const createStore = (path) => {
		try {
			const splits = path.split("/");
			let cwd = DATADIR;
			for (let i = 0; i < splits.length-1; i++) {
				cwd = `${cwd}/${splits[i]}`;
				try { fs.mkdirSync(cwd); } catch (ex) {}
			}
		} catch (ex) {
			mp.log("createStore got ex: " + ex.message);
		}
		return { path, data: {} };
	}

	const writeStore = (store) => {
		try {
			const fullPath = `${DATADIR}/${store.path}`;
			const encoded = mp.crypt.encrypt(JSON.stringify(store), "readFileSync");
			fs.writeFileSync(fullPath, encoded);
		} catch (ex) {
			mp.log("writeStore got ex: " + ex.message);
		}
	}

	const getValue = (store, key, dfl) => {
		try {
			if (store) return store.data[key];
		} catch (ex) {
			mp.log("getValue got ex: " + ex.message);
		}
		return dfl;
	}

	const setValue = (store, key, value) => {
		try {
			if (store) {
				if (store.data[key] !== value) {
					store.data[key] = value;
				}
			}
		} catch (ex) {
			mp.log("setValue got ex: " + ex.message);
		}
	}

	exports = {
		readStore,
		writeStore,
		createStore,
		getValue,
		setValue
	}
	if (module) module.exports = exports;
})();
