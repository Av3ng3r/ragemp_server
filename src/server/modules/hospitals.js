(function() {
	const hospitals = [
	{
		n: "Mount Zonah Medical Center",
		x: -497.8622131347656,
		y: -335.6024475097656,
		z: 34.502,
		h: 253.04083251953125
	},
	{
		n: "Central Los Santos Medical Center",
		x: 295.9252624511719,
		y: -1447.0020751953125,
		z: 29.97,
		h: 319.38153076171875
	},
	{
		n: "Sandy Shores Medical Center",
		x: 1839.5242919921875,
		y: 3672.22509765625,
		z: 34.28,
		h: 221.348342955078
	},
	{
		n: "The Bay Care Center",
		x: -246.9646453857422,
		y: 6330.485315625,
		z: 32.43,
		h: 231.3373803710938
	},
	{
		n: "Eclipse Medical Tower",
		x: -676.9755249032438,
		y: 310.6949157714844,
		z: 83.09,
		h: 175.6649017339844
	},
	{
		n: "St. Fiacre Hospital",
		x: 1150.8885498046875,
		y: -1530.0849609375,
		z: 35.38,
		h: 317.126953125
	},
	{
		n: "Portola Trinity Medical Center",
		x: -875.0014038085938,
		y: -309.209228515625,
		z: 39.54,
		h: 0
	}
	];

	const getClosestHospital = function(player)
	{
		let closest = hospitals[0];
		let closestDistance = 1000000;
		const p1 = player.position;
		try {
			for (let i = 0; i < hospitals.length; i++) {
				const p2 = hospitals[i];
				const d = Math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2 + (p2.z - p1.z) ** 2);
				if (d < closestDistance) {
					closest = hospitals[i];
					closestDistance = d;
				}
			}
		} catch (ex) {
			mp.log("got ex: " + ex.message);
		}
		return closest;
	}

	const getFurthestHospital = function(p1) {
		let furthest = hospitals[0];
		let furthestDistance = 0;
		try {
			for (let i = 0; i < hospitals.length; i++) {
				const p2 = hospitals[i];
				const d = Math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2 + (p2.z - p1.z) ** 2);
				if (d > furthestDistance) {
					furthest = hospitals[i];
					furthestDistance = d;
				}
			}
		} catch (ex) {
			furthest = global.getRandomHospital(null);
		}
		return furthest;
	}

	const getRandomHospital = function(player)
	{
		let i = Math.floor(Math.random() * hospitals.length);
		return hospitals[i];
	}

	exports = {
		getClosestHospital,
		getFurthestHospital,
		getRandomHospital
	}

	if (module) module.exports = exports;
})();
