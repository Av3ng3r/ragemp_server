(function() {
	const tankWarsPlayers = [];
	const scores = [];

	const spawnPoints = [
		{ x: -1769.742919921875, y: 2993.390380859375, z: 37.67, a: 90 },
		{ x: -2406.65380859375, y: 3251.363525390625, z: 34.799495697021484, a: 180 },
		{ x: -2357.31689453125, y: 3261.78173828125, z: 34.99404907226525, a: 70 },
		{ x: -2440.8623046875, y: 2951.517822265625, z: 34.84852981567383, a: 350 },
		{ x: -2132.523193359375, y: 2821.19775390625, z: 34.8159469604492, a: 0 },
		{ x: -2084.078857421875, y: 2819.927978515625, z: 34.841487884521484, a: 0 }
	];

	const broadcastTopTen = (log) => {
		try {
			scores.sort((a, b) => b.kills - a.kills);
			const o = [];
			scores.forEach(score => {
				try {
				const pIndex = tankWarsPlayers.findIndex(entry => entry.id == score.id);
				if (pIndex > -1) {
					const name = tankWarsPlayers[pIndex].playerName;
					if (name) o.push({ name, kills: score.kills });
				}
				} catch (ex) {
					mp.log("bctt ex 1");
				}
			});

			if (log) {
				mp.log("TANKWARS TOP TEN =================");
				mp.log(JSON.stringify(o, null, '\t'));
			}

			tankWarsPlayers.forEach(p => {
				try {
				if (mp.players.exists(p.id)) p.call("tankWarsScoreboard", [ JSON.stringify(o) ]);
				} catch (ex) {
					mp.log("bctt ex 2");
				}
			});
		} catch (ex) {
			mp.log("bctt ex 3");
		}
	}

	const respawn = (player) => {
		if (mp.players.exists(player.id)) try {
			player.health = 100;
			const index = Math.floor(Math.random() * spawnPoints.length);
			const pos = spawnPoints[index];
			player.spawn(pos);
			player.heading = pos.a;
		} catch (ex) {
			mp.log("tw ex 1");
		}
	};

	mp.events.add("playerJoinTankWars", (player) => {
		if (mp.players.exists(player.id)) try {
			mp.log(`${player.name} has entered TANK WARS!!!`);
			mp.minigame.setPlayerIsPlaying(player);
			mp.players.broadcast(`${player.name} has entered tankwars`);
		} catch (ex) {}
	});

	mp.events.add("playerLeftTankWars", (player, data) => {
		if (mp.players.exists(player.id)) try {
			let args = [];
			if (data) args = JSON.parse(data);
			mp.minigame.setPlayerIsNotPlaying(player);
			const sIndex = scores.findIndex(entry => entry.id == player.id);
			if (sIndex > -1) scores.splice(sIndex, 1);
			broadcastTopTen();
			const index = tankWarsPlayers.findIndex(entry => entry.id == player.id);
			if (index > -1) {
				tankwarsPlayers.splice(index, 1);
				mp.players.broadcast(`${player.name} has left tank wars`);
			}
		} catch (ex) {}
	});

	mp.events.add("playerQuit", (player) => {
		if (mp.players.exists(player.id)) try {
			const index = tankWarsPlayers.findIndex(entry => entry.id == player.id);
			if (index > -1) {
				tankWarsPlayers.splice(index, 1);
				mp.players.broadcast(`${player.name} has left tank wars`);
			}

			const sIndex = scores.findIndex(entry => entry.id == player.id);
			if (sIndex > -1) scores.splice(sIndex, 1);
			broadcastTopTen();
		} catch (ex) {}
	});

	mp.events.add("playerDeath", (player, reason, killer) => {
	});
})();
