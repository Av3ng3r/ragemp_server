(function() {
	let currentBountyPlayer = null;
	let bountyGetsNothing = false;
	let currentBountyOffer = 0;
	let currentBountyCounter = 0;
	let minUsers = 3;

	const clearBountyState = () => {
		try {
		currentBountyOffer = 0;
		bountyGetsNothing = false;
		if (currentBountyPlayer) {
			currentBountyPlayer.setVariable("wanted", false);
			currentBountyPlayer = null;
		}
		} catch (ex) {
			mp.log("bounty ex 1: " + ex.message);
		}
	};

	const setBounty = (target, again) => {
		try {
		if (currentBountyPlayer && mp.players.exists(currentBountyPlayer.id)) {
			currentBountyCounter--;
			let stars = currentBountyCounter;
			currentBountyPlayer.call("wanted", [ stars ]);
			if (currentBountyCounter <= 0) {
				currentBountyPlayer.setVariable("wanted", false);
				mp.log("BOUNTY: the bounty evaded capture");
				mp.players.call("notify", [ "The bounty evaded capture" ]);
				if (bountyGetsNothing) {
					mp.av.rewards.addToXP(currentBountyPlayer, currentBountyOffer);
				}
				clearBountyState();
			}
			return;
		} else if (currentBountyPlayer) {
			currentBountyPlayer = null;
			currentBountyOffer = 0;
			mp.log("BOUNTY: the bounty has gone");
			mp.players.call("notify", [ "The bounty has gone" ]);
			return;
		}

		if (target) {
			clearBountyState();
			currentBountyCounter = 6;
			currentBountyOffer = Math.floor(Math.random() * 500) + 100;
			currentBountyPlayer = target;
			target.setVariable("wanted", true);
			target.call("wanted", [ 5 ]);
			mp.log("BOUNTY: A bounty has been set on " + target.name);
			if (again) {
				mp.players.call("notify", [ "The bounty ~r~" + target.name + " ~w~ has returned"]);
			} else {
				mp.players.call("notify", [ "A bounty has been set on ~r~" + target.name ]);
            }
			return;
		}

		let players = mp.players.toArray().filter(p => p.getVariable("loggedIn") && !p.getVariable("banished"));
		let num = players.length;
		if (num >= minUsers) {
			if (Math.random() > 0.85) {
				let r = Math.floor(Math.random() * num);
				let p = players[r];
				if (p && mp.players.exists(p.id)) {
					if (!p.getVariable("following") && !p.getVariable("godmode")) {
						clearBountyState();
						currentBountyCounter = 6;
						currentBountyOffer = Math.floor(Math.random() * 500) + 100;
						currentBountyPlayer = p;
						p.setVariable("wanted", true);
						p.call("wanted", [ 5 ]);
						mp.log("BOUNTY: A bounty has been set on " + p.name);
						mp.players.call("notify", [ "A bounty has been set on ~r~" + p.name ]);
					}
				}
			}
		}
		} catch (ex) {
			mp.log("bounty ex 2: " + ex.message);
		}
	};

	mp.events.add("playerLoggedIn", (player) => {
		try {
		if (mp.players.exists(player.id)) {
			if (currentBountyPlayer && mp.players.exists(currentBountyPlayer)) {
				player.call("notify", [ "There is a bounty on ~r~" + currentBountyPlayer.name ]);
			}
		}
		} catch (ex) {
			mp.log("bounty ex 3: " + ex.message);
		}
	});

	mp.events.add("playerQuit", (player) => {
		try {
		if (currentBountyPlayer === player) {
			mp.log("BOUNTY: The bounty has left the server");
			mp.players.call("notify", [ "The bounty has left the server" ]);
			currentBountyPlayer = null;
			bountyGetsNothing = true;
			currentBountyOffer = 0;
		}
		} catch (ex) {
			mp.log("bounty ex 4: " + ex.message);
		}
	});

	mp.events.add("playerDeath", (player, reason, killer) => {
		try {
		if (currentBountyPlayer) {
			if (mp.players.exists(player.id) && mp.players.exists(currentBountyPlayer.id)) {
				if (killer && killer !== player) {
					if (player === currentBountyPlayer) {
						mp.av.rewards.addToXP(killer, currentBountyOffer);
						currentBountyPlayer.call("wanted", [ 0 ]);
						currentBountyPlayer.setVariable("wanted", false);
						clearBountyState();
						mp.log("BOUNTY: the bounty has been collected by " + killer.name);
						mp.players.call("notify", [ "Bounty has been collected by ~r~" + killer.name ]);
					}
				} else {
					bountyGetsNothing = true;
				}
			}
		}
		} catch (ex) {
			mp.log("bounty ex 5: " + ex.message);
		}
	});

	mp.events.add("am_bounty", (player, id) => {
		try {
		if (mp.players.exists(player.id)) {
			if (player.getVariable("admin")) {
				if (undefined !== id) {
					let target = mp.players.at(id);
					if (target) {
						if (!currentBountyPlayer) {
							setBounty(target);
						}
					}
				} else {
					currentBountyCounter = 0;
					setBounty();
				}
			}
		}
		} catch (ex) {
			mp.log("bounty ex 6: " + ex.message);
		}
	});

	mp.events.add("bounty_update", (player, data) => {
		try {
		if (mp.players.exists(player.id)) {
			player.setVariable("bounty_data", data);
		}
		} catch (ex) {
			mp.log("bounty ex 7: " + ex.message);
		}
	});

	setInterval(setBounty, 100000);
})();
