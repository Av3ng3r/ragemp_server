(function() {
	mp.events.add("am_blammo", (player, id, reason) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin") || player.id === id) {
					const target = mp.players.at(id);
					if (target) {
						mp.log("BLAMMO " + player.name + " " + target.name + " " + reason);
						target.health = 0;
						target.call("health", [ 0 ]);
					}
				}
			}
		} catch (ex) {
			mp.log("admin ex 1");
			// mp.log("admin ex 1: " + ex.message);
		}
	});

	mp.events.add("am_banish", (player, id, ...reason) => {
		try {
			if (reason) reason = reason.join(" ");
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin") || player.id === id) {
					const target = mp.players.at(id);
					if (target) {
						mp.log("BANISH: " + player.name + " " + target.name + " " + reason);
						target.call("playerBanished", [ true, reason ]);
						target.position = new mp.Vector3(33.3965, 7689.6235351, 3.05);
						target.dimension = Math.ceil(Math.random() * 20 + 100);
						if (player.id !== id) player.call("notify", [ "You have banished " + target.name + " [" + target.id + "]" ]);
						target.setVariable("banished", true);
					}
				}
			}
		} catch (ex) {
			mp.log("admin ex 2");
			// mp.log("admin ex 2: " + ex.message);
		}
	});

	mp.events.add("am_unbanish", (player, id) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin")) {
					const target = mp.players.at(id);
					if (target) {
						target.dimension = 0;
						target.call("playerUnbanished", [ true ]);
						player.call("notify", [ "You have unbanished " + target.name + " [" + target.id + "]" ]);
						target.setVariable("banished", false);
					}
				}
			}
		} catch (ex) {
			mp.log("admin ex 3");
			// mp.log("admin ex 3: " + ex.message);
		}
	});

	mp.events.add("am_explode", (player, id) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin")) {
					const target = mp.players.at(id);
					if (target) {
						if (target.vehicle) try {
							target.vehicle.explode();
						} catch (ex) {}
					}
				}
			}
		} catch (ex) {
			mp.log("admin ex 4: " + ex.message);
		}
	});

	mp.events.add("am_cheat", (player, cheat) => {
		try {
			if (mp.players.exists(player.id)) {
				mp.log("CHEAT: " + player.name + " - detected " + cheat + " cheat");
			}
		} catch (ex) {
			mp.log("admin ex 5: " + ex.message);
		}
	});

	mp.events.add("am_follow", (player, id) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin")) {
					player.setVariable("target", id);
					startFollowMode(player);
				}
			}
		} catch (ex) {
			mp.log("admin ex 6");
			// mp.log("admin ex 6: " + ex.message);
		}
	});

	mp.events.add("am_unfollow", (player) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin")) {
					stopFollowMode(player);
				}
			}
		} catch (ex) {
			mp.log("admin ex 7");
			// mp.log("admin ex 7: " + ex.message);
		}
	});

	mp.events.add("am_mute", (player, id) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin")) {
					const target = mp.players.at(id);
					if (target) {
						target.setVariable("muted", true);
					}
				}
			}
		} catch (ex) {
			mp.log("admin ex 8");
			// mp.log("admin ex 8: " + ex.message);
		}
	});

	mp.events.add("am_unmute", (player, id) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("admin")) {
					const target = mp.players.at(id);
					if (target) {
						target.setVariable("muted", false);
					}
				}
			}
		} catch (ex) {
			mp.log("admin ex 9");
			// mp.log("admin ex 9: " + ex.message);
		}
	});

	const saveDefaultVehicles = () => {
		const vehicles = [];
		mp.vehicles.forEach(v => {
			if (v.getVariable("default") === true) {
				vehicles.push({
					m: v.model,
					p: v.position,
					r: mp.relativeRotation(v.rotation),
					d: {
						m: v.getVariable("mods"),
						n: v.getVariable("name"),
						c: v.getVariable("class"),
						d: v.getVariable("default"),
						p: v.numberPlate,
					}
				});
			}
		});
		if (vehicles.length > 0) {
			fs.writeFileSync("/default.vehicles.json", JSON.stringify(vehicles));
		}
	};

	mp.events.add("am_abandon", (player) => {
		const vehicleId = player.getVariable("vehicleId");
		if (vehicleId !== false) try {
			const vehicle = mp.vehicles.at(vehicleId);
			if (vehicle) {
				vehicle.setVariable("default", true);
				// vehicle.setVariable("abandoned", true);
				player.setVariable("vehicleId", false);
				vehicle.numberPlate = Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(0, 8).toUpperCase();
				saveDefaultVehicles();
			}
		} catch (ex) {
			mp.log("admin ex 10");
		}
	});

	const startFollowMode = (player) => {
		try {
			let interval = player.interval;
			if (interval) {
				clearInterval(interval);
				player.interval = 0;
			}

			player.followData = {
				dimension: player.dimension,
				position: player.position
			};
		} catch (ex) {
			mp.log("admin ex 11");
			// mp.log("admin ex 10: " + ex.message);
		}

		player.interval = setInterval(function() {
			try {
				const id = this.getVariable("target");
				if (!this.getVariable("following")) {
					stopFollowMode(this);
				} else {
					const target = mp.players.at(id);
					if (!target) {
						stopFollowMode(this);
					}else {
						let { x, y, z } = target.position;
						this.dimension = target.dimension;
						this.position = new mp.Vector3(x, y, z + 50);
						this.alpha = 0;
					}
				}
			} catch (ex) {
			mp.log("admin ex 12");
				stopFollowMode(this);
				return;
			}
		}.bind(player), 100);
		player.setVariable("following", true);
	};

	const stopFollowMode = (player) => {
		try {
			if (player.interval) clearInterval(player.interval);
			player.interval = 0;
			player.position = player.followData.position;
			player.dimension = player.followData.dimension;
			player.followData = null;
			player.setVariable("following", false);
			player.setVariable("target", false);
			player.alpha = 255;
		} catch (ex) {
			mp.log("admin ex 13");
		}
	};
})();
