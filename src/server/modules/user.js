(function() {
	const fs = require("fs");
	const db = require("./db");
	const hospitals = require("./hospitals");

	const OK = 0;
	const ERROR_INVALID = 1;
	const ERROR_EXISTS = 2;
	const ERROR_OTHER = 3;
	const ERROR_ALREADY_LOGGED_IN = 4;
	const ERROR_BANNED = 5;
	const BANNED_JSON_FILE="/banned_addresses.json";

	const getRandomSkin = () => {
		const i = Math.floor(Math.random() * 3);
		return [ "player_one", "player_two", "player_three" ][i];
	}

	const login = (username, password) => {
		let rv = ERROR_INVALID;
		try {
		if (username) {
			const store = db.readStore(`${username}/account`);
			const hash = db.getValue(store, "hash", false);
			const banned = db.getValue(store, "banned", false);
			if (banned) {
				rv = ERROR_BANNED;
			} else if (hash === password) rv = OK;
		}
		} catch (ex) {
			mp.log("user ex 1");
		}
		return rv;
	}

	const newUser = (username, password) => {
		let rv = OK;
		try {
		if (username) {
			const store = db.readStore(`${username}/account`);
			if (db.getValue(store, "username", false)) {
				rv = ERROR_EXISTS;
			} else {
				const acctStore = db.createStore(`${username}/account`);
				db.createStore(`${username}/gamedata`);

				db.setValue(acctStore, "username", username);
				db.setValue(acctStore, "hash", password);
				db.writeStore(acctStore);
			}
		}
		} catch (ex) {
			mp.log("user ex 2");
		}
		return rv;
	}

	const checkIfLoggedIn = (name) => {
		mp.log("checking if " + name + " is already logged in");
		let rv = false;
		try {
		const arr = mp.players.toArray();
		for (let i = 0; i < arr.length; i++) {
			if (arr[i].getVariable("guest")) continue;
			if (arr[i].getVariable("username") === name && arr[i].password) {
				rv = true;
				break;
			}
		}
		} catch (ex) {
			mp.log("user ex 3");
		}
		return rv;
	}

	mp.events.add("saveData", (player, name, val) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("guest") !== true) {
					const username = player.getVariable("username");
					if (username) {
						const store = db.readStore(`${username}/gamedata`);
						db.setValue(store, name, JSON.parse(val));
						db.writeStore(store);
					}
				}
			}
		} catch (ex) {
			mp.log("saveData got ex", ex);
		}
	});

	mp.events.add("character", (player, character) => {
		try {
			const model = mp.joaat(character);
			if (model) player.model = model;
		} catch (ex) {
			mp.log("character ex: ", ex);
		}
	});

	mp.events.add("requestLogin", (player, name, password) => {
		try {
			if (player.getVariable("lr")) return;
			player.setVariable("lr", true);
			mp.log("request login " + name + " " + password);
			if (checkIfLoggedIn(name)) {
				mp.log("already exists");
				player.call("failedLogin", [ "already" ]);
				player.setVariable("loggedIn", false);
				player.setVariable("lr", false);
				return;
			}
			const res = login(name, password);
			if (res === ERROR_BANNED) {
				mp.log("banned user " + name);
				player.kick("banned");
			} else if (res === OK) {
				mp.log("[USER] " + player.name + " --> " + name + " successfully logged in");
				player.setVariable("name", player.name);
				player.setVariable("username", name);
				player.password = password;
				player.dimension = 0;
				player.alpha = 255;
				player.name = `${name} [${player.id}]`;
				player.setVariable("vehicleId", false);
				player.setVariable("loggedIn", true);
				checkAdmin(player);

				const store = db.readStore(`${name}/gamedata`);
				const character = db.getValue(store, "character", getRandomSkin());
				player.model = mp.joaat(character);
				player.call("successfulLogin", [ JSON.stringify(store.data) ]);
				player.call("vehsync");
				mp.players.call("notify", [ player.name + " joined"]);
			} else {
				mp.log("failed " + name);
				player.call("failedLogin", [ "wrong password" ]);
				player.setVariable("loggedIn", false);
				player.setVariable("lr", false);
			}
			player.setVariable("lr", false);
		} catch (ex) {
			mp.log("requestLogin got ex: " + ex.message);
		}
	});

	mp.events.add("requestNewUser", (player, name, password) => {
		try {
			if (player.getVariable("lr")) return;
			player.setVariable("lr", true);
			mp.log("request new user");
			if (newUser(name, password) === ERROR_EXISTS) {
				player.setVariable("loggedIn", false);
				player.call("newUserError");
			} else {
				mp.log("[USER] " + player.name + " --> " +
					name + " successfully created and logged in");
				player.setVariable("name", player.name);
				player.setVariable("username", name);
				player.name = `${name} [${player.id}]`;
				player.password = password;
				player.dimension = 0;
				player.alpha = 255;
				player.setVariable("vehicleId", false);
				player.setVariable("loggedIn", true);

				const store = db.readStore(`${name}/gamedata`);
				const character = db.getValue(store, "character", getRandomSkin());
				player.model = mp.joaat(character);
				player.call("successfulLogin", [ JSON.stringify(store.data) ]);
				player.call("vehsync");
				mp.players.call("notify", [ player.name + " joined"]);
			}
		} catch (ex) {
			mp.log("requestNewUser got ex: " + ex.message);
		}
	});

	mp.events.add("requestGuest", (player) => {
		try {
			if (player.getVariable("lr")) return;
			player.setVariable("lr", true);
			mp.log("request guest");
			player.setVariable("name", player.name);
			player.setVariable("username", player.name);
			player.name = `${player.name} [${player.id}]`;
			player.password = "guest";
			player.dimension = 0;
			player.alpha = 255;
			player.setVariable("vehicleId", false);
			player.setVariable("loggedIn", true);
			player.setVariable("guest", true);
			const character = getRandomSkin();
			player.model = mp.joaat(character);
			player.call("successfulLogin", [ JSON.stringify({}) ]);
			player.call("vehsync");
			mp.players.call("notify", [ player.name + " joined"]);
		} catch (ex) {
			mp.log("requestGuest got ex: " + ex.message);
		}
	});

	mp.events.add("playerDeath", (player, reason, killer) => {
		try {
			if (mp.players.exists(player.id)) {
				if (mp.minigame.isPlayingMinigame(player)) return;
				if (player.isReallyDead) return;
				player.isReallyDead = true;

				setTimeout(() => {
					try {
						let msg = `${player.name} died`;
						if (killer && killer !== player) {
							msg = `${killer.name} killed ${player.name}`;
						} else if (killer && killer === player) {
							msg = `${player.name} wasted`;
						}

						if (mp.players.exists(player.id)) {
							player.isReallyDead = false;
							// if (player.health <= 0) {
								mp.log("USER: " + msg);
								mp.players.call("notify", [ msg ]);
								player.health = 100;
								const pos = hospitals.getRandomHospital(player);
								player.spawn(pos);
								player.heading = pos.h;
							// }
						}
					} catch (ex) {
						mp.log("user ex 7");
					}
				}, 2500);
			}
		} catch (ex) {
			mp.log("user ex 8");
		}
	});

	mp.events.add("playerJoin", (player) => {
		try {
			mp.log("=========================================");
			mp.log((player.name) + " joined");
			mp.log(player.ip);
			mp.log(player.socialClub);
			mp.log(player.serial);
			mp.log("=========================================");

			if (banned(player)) {
				mp.log("got banned player");
				player.kick("banned");
				return;
			}

			player.setVariable("vehicleId", false);
			player.setVariable("loggedIn", false);
		} catch (ex) {
			mp.log("playerJoin ex: " + ex.message);
		}
	});

	mp.events.add("playerQuit", (player) => {
		try {
			const vehicleId = player.getVariable("vehicleId");
			const loggedIn = player.getVariable("loggedIn");
			if (loggedIn) mp.players.call("notify", [ player.name + " quit"]);
			mp.log((player.getVariable("username") || player.name) + " quit");
		} catch (ex) {
			mp.log("playerQuit got ex: " + ex.message);
		}
	});

	mp.events.add("warp", (player, playerId) => {
		try {
			const target = mp.players.at(playerId);
			if (target) {
				const n1 = target.getVariable("username");
				const n2 = player.getVariable("username");
				if (target.getVariable("following") ||
					target.getVariable("playerBlockedWarp")) {
					player.notify(`${n1} has blocked warping`);
					target.notify(`${n2} tried to warp to you`);
				} else {
					player.position = target.position;
					player.dimension = target.dimension;
					target.notify(`${n2} warped to you`);
				}
			}
		} catch (ex) {
			mp.log("warp got ex: " + ex.message);
		}
	});

	mp.events.add("setWarpBlock", (player, blocked) => {
		try {
			player.setVariable("playerBlockedWarp", blocked);
		} catch (ex) {
			mp.log("setWarpBlock ex: " + ex.message);
		}
	});

	const checkAdmin = (player) => {
		try {
		const username = player.getVariable("username");
		const store = db.readStore(`${username}/account`);
		const admin = db.getValue(store, "admin", false);
		if (admin) {
			mp.log(`Admin has logged in [${username}]`);
			player.setVariable("admin", true);
		}
		} catch (ex) {
			mp.log("checkAdmin ex 1");
		}
	}

	const banned = (player) => {
		let banned = false;
		const { ip, name, socialClub, serial } = player;
		if (fs.existsSync(BANNED_JSON_FILE)) {
			let content = fs.readFileSync(BANNED_JSON_FILE, "utf-8");
			try {
				const decoded = JSON.parse(content);
                for (let i = 0; i < decoded.length; i++) {
                    const entry = decoded[i];
                    if (entry.ip === ip) {
                        mp.log("banned on ip address " + ip);
                        banned = true;
                        break;
                    }
                    if (entry.name === name) {
                        mp.log("banned on name " + name);
                        banned = true;
                        break;
                    }
                    if (entry.socialClub === socialClub) {
                        mp.log("banned on social club " + socialClub);
                        banned = true;
                        break;
                    }
                    if (entry.serial === serial) {
                        mp.log("banned on serial " + serial);
                        banned = true;
                        break;
                    }
				}
			} catch (ex) {
				mp.log("banned got ex: " + ex.message);
			}
		}

		return banned;
	};

	setInterval(function() {
		try {
			const players = mp.players.toArray();
			for (let i = 0; i < players.length; i++) {
				const player = players[i];
				if (player.getVariable("loggedIn")) {
					let pos = player.position;
					let heading = player.heading;
					let cls = -1;
					if (player.vehicle) {
						pos = player.vehicle.position;
						heading = player.vehicle.heading;
						cls = player.vehicle.getVariable("class");
					}
					let data = JSON.stringify({ x: pos.x, y: pos.y, z: pos.z, h: heading, c: cls});
					player.setVariable("position", data);
				}
			}
		} catch (ex) {
			mp.log("pos update ex 1");
		}
	}, 2000);
})();
