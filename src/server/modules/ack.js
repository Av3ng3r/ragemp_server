(function() {
	mp.events.add("syn", (player, data) => {
		try {
			const time = Date.now();
			if (mp.players.exists(player.id)) player.call("ack", [ data, time.toString() ]);
		} catch (ex) {
			console.log("syn ex: ", ex);
		}
	});
})();
