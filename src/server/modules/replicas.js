(function() {
	if (false) {
	const messenger = require("../lib/messenger/messenger.js");

	let sPort = 8000;
	let cPort = 8001;

	if (mp.config.version === "1.41") {
		mp.log("in 1.41 server version");
		sPort = 8001; cPort = 8000;
	} else {
		mp.log("in 1.46 server version");
	}

	const client = messenger.createSpeaker(cPort);
	const server = messenger.createListener(sPort);

	// replica (player) events
	server.on('rsync', (message, data) => {
		mp.players.call('rsync', data);
	});
	server.on('rcreate', (message, data) => {
		const obj = JSON.parse(data);
		if (obj) players[obj.id] = obj;
	});
	server.on('rdelete', (message, data) => {
		const obj = JSON.parse(data);
		if (obj) delete players[obj.id];
		mp.players.call('rdelete', data);
	});

	// vehicle events
	server.on('vsync', (message, data) => {
		const obj = JSON.parse(data);
		obj.forEach(entry => {
			try {
				const vehicle = mp.vehicles.at(entry.id);
				if (vehicle) {
					let rot = relativeRotation(entry.rotation);
					if (modsChanged(vehicle, entry.mods)) {
						vehicle.setVariable("mods", entry.mods);
						mp.players.call("vehsync");
					}
					vehicle.position = entry.position;
					vehicle.rotation = new mp.Vector3(rot.x, rot.y, rot.z);
					mp.players.call("freezeVehicle",
						[
							vehicle.id,
							entry.velocity.x,
							entry.velocity.y,
							entry.velocity.z,
							entry.steer
						]
					);
				}
			} catch (ex) {
				mp.log("got ex: " + ex.message);
			}
		});
	});
	server.on('vcreate', (message, data) => {
		mp.log("got vcreate " + data);
		try {
			const obj = JSON.parse(data);
			const vehicle = mp.vehicles.new(obj.model, obj.position, {
				heading: obj.heading,
				alpha: 0
			});
			if (data.mods) {
				vehicle.setVariable("mods", mods);
				mp.players.call("vehsync");
			}
			vehicle.remoteId = data.id;
			const resp = {
				c: 'vcreate',
				id: data.id,
				remoteId: vehicle.id
			}
			message.reply(JSON.stringify(resp));
		} catch (ex) {
			mp.log("got ex: ", ex);
		}
	});
	server.on('vdelete', (message, data) => {
		try {
			const obj = JSON.parse(data);
			if (obj) {
				const vehicle = mp.vehicles.at(obj.id);
				if (vehicle) {
					vehicle.alpha = 0;
					vehicle.destroy();
				}
			}
		} catch (ex) {
			mp.log("vdelete ex: ", ex);
		}
	});

	mp.events.add("entityCreated", (entity) => {
		if (entity.alpha === 0) {
			entity.alpha = 255;
			return;
		}

		if (entity.type === "vehicle") {
			const data = {
				id: entity.id,
				model: entity.model,
				position: entity.position,
				rotation: {
					x: entity.rotation.x,
					y: entity.rotation.y,
					z: entity.rotation.z
				},
				velocity: {
					x: entity.velocity.x,
					y: entity.velocity.y,
					z: entity.velocity.z
				},
				steer: entity.steerAngle,
				mods: entity.getVariable("mods")
			}
			client.request("vcreate", JSON.stringify(data), (resp) => {
				try {
					const obj = JSON.parse(resp);
					if (obj && obj.c === "vcreate") entity.remoteId = obj.remoteId;
				} catch (ex) {}
			});
		} else if (entity.type === "player") {
			/*
			const data = {
				id: entity.id,
				model: entity.model,
				position: entity.position,
				heading: entity.heading
			}
			client.request("rcreate", JSON.stringify(data), (resp) => {});
			*/
		}
	});

	mp.events.add("entityDestroyed", (entity) => {
		mp.log("entity destroyed ");
		if (entity.alpha === 0) {
			mp.log("alpha = 0");
			return;
		}
		if (entity.type === "vehicle") {
			mp.log("got vehicle");
			const data = { id: entity.remoteId };
			client.request("vdelete", JSON.stringify(data), (resp) => {});
		} else if (entity.type === "player") {
			// const data = { id: player.id };
			// client.request("rdelete", JSON.stringify(data), (resp) => {});
		}
	});

	/*
	setInterval(() => {
		const data = {
			players: [],
			vehicles: [],
		};

		mp.players.forEach((player) => {
			data.players.push({
				id: player.id,
				position: player.position,
				heading: player.heading,
				model: player.model,
				dimension: player.dimension
			});
		});

		mp.vehicles.forEach((vehicle) => {
			try {
			const occupants = vehicle.getOccupants();
			let isDriver = false;
			occupants.forEach(o => isDriver = isDriver || o.seat === -1);
			if (isDriver) {
				mp.players.call("unfreezeVehicle", [ vehicle.id ]);
				data.vehicles.push({
					id: vehicle.id,
					model: vehicle.model,
					position: vehicle.position,
					rotation: {
						x: vehicle.rotation.x,
						y: vehicle.rotation.y,
						z: vehicle.rotation.z
					},
					velocity: {
						x: vehicle.velocity.x,
						y: vehicle.velocity.y,
						z: vehicle.velocity.z
					},
					steer: vehicle.steerAngle,
					mods: vehicle.getVariable("mods")
				});
			}
			} catch (ex) {
				mp.log("got ex: " + ex.message);
			}
		});

		if (data.players.length > 0) {
			client.request("rsync", JSON.stringify(data.players), (resp) => {});
		}
		if (data.vehicles.length > 0) {
			client.request("vsync", JSON.stringify(data.vehicles), (resp) => {});
		}
	}, 40);
	*/

	const relativeRotation = (rotation) => {
		const {x, y, z} = rotation;
		const rx = (360-z) / 180 * Math.PI;
		const ry = z / 180 * Math.PI;

		return {
			x: x * -Math.cos(rx) + y * Math.sin(rx),
			y: y * -Math.cos(ry) + x * Math.sin(ry),
			z
		};
	};

	const modsChanged = (vehicle, mods) => {
		// TODO: actually compare the mods
		return true;
	};
	}
})();
