(function() {
	function cookMessage(player, message, targetId, ncolor, mcolor) {
		try {
			ncolor = ncolor || "!{00ff00}";
			mcolor = mcolor || "!{white}";
			targetId = targetId || false;

			const tokens = message.split(" ");
			const references = [];
			for (let i = 0; i < tokens.length; i++) {
				const token = tokens[i];
				switch (token[0]) {
					case "@":
						let id = token.substr(1);
						let p = mp.players.at(id);
						if (p) {
							tokens[i] = ncolor + (p.getVariable("username") || p.name) + mcolor;
							if (targetId && id || !targetId) references[id] = p;
						}
						break;
				}
			}
			let res = tokens.join(" ");
			references.forEach(function(p) { p.call("mentioned"); });
			return mcolor + res;
		} catch (ex) {
			mp.log("chat ex 1");
		}
		return message;
	}

	mp.events.add("playerChat", function(player, message) {
		try {
			const cooked = cookMessage(player, message);
			if (player.getVariable("following")) {
				player.call("chat", [ player.name, `you're muted when following` ]);
			} else if (player.getVariable("muted")) {
				player.call("chat", [ player.name, cooked ]);
				mp.log("muted user chat: " + player.name + ": " + cooked);
			} else {
				mp.players.broadcast(`${player.name}: ${cooked}`);
			}
		} catch (ex) {
			mp.log("playerChat ex: " + ex.message);
		}
	});

	mp.events.add("dm", function(player, id, message) {
		try {
			id = parseInt(id);
			const target = mp.players.at(id);
			if (target) {
				const cooked = cookMessage(player, message, id, null, "!{70c0ff}");
				if (!player.getVariable("muted") || player.id === id) {
					if (player.id !== id) {
						const pun = player.getVariable("username");
						const tun = target.getVariable("username");
						mp.log(`DM: ${pun} --> ${tun}: ${cooked}`);
					}
					target.call("chat", [ player.name, cooked ]);
				}
				if (player.getVariable("muted")) {
					mp.log("muted user dm: " + player.name + ": " + cooked);
				}
			}
		} catch (ex) {
			mp.log("dm ex: " + ex.message);
		}
	});

	mp.events.add("me", function(player, id, message) {
		try {
			const target = mp.players.at(id);
			if (target) {
				const cooked = cookMessage(player, message, id, null, "!{ff70c0}");
				if (!player.getVariable("muted") || player.id === id) {
					target.call("chat", [ player.getVariable("username"), cooked ]);
				}
				if (player.getVariable("muted")) {
					mp.log("muted user me: " + player.name + ": " + cooked);
				}
			}
		} catch (ex) {
			mp.log("me ex: " + ex.message);
		}
	});
})();
