(function() {
	mp.log = (...args) => {
		console.log(`[ ${(new Date()).toLocaleString()} ]`, args);
	}
})();
