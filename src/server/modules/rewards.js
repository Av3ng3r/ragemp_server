(function() {
	const db = require("./db");

	mp.av.rewards = {};

	mp.av.rewards.addToXP = (player, value) => {
		try {
			if (mp.players.exists(player.id)) {
				if (player.getVariable("guest") !== true) {
					const username = player.getVariable("username");
					if (username) {
						const store = db.readStore(`${username}/gamedata`);
						let rp = db.getValue(store, "rp", "0") || 0;
						rp = parseInt(rp);
						rp += parseFloat(value);
						rp = Math.round(rp);
						checkLevel(rp, player);
					}
				}
			}
		} catch (ex) {
			mp.log("addToXP got ex: " + ex.message);
		}
	};

	mp.events.add("playerDeath", (player, reason, killer) => {
		try {
			if (mp.players.exists(player.id)) {
				if (killer && killer.id !== player.id) {
					if (killer.getVariable("guest") !== true) mp.av.rewards.addToXP(killer, 3);
					if (player.getVariable("guest") !== true) mp.av.rewards.addToXP(player, 1);
				}
			}
		} catch (ex) {
			mp.log("playerDeath (rewards) got ex: " + ex.message);
		}
	});

	mp.events.add("onPlayerDriftEnding", (player, score) => {
		try {
			if (player.getVariable("guest") !== true) {
				if (mp.players.exists(player.id)) {
					// const delta = Math.log(parseInt(score)) * 2;
					const delta = Math.floor(parseInt(score) / 250) + 3;
					mp.av.rewards.addToXP(player, delta);
				}
			}
		} catch (ex) {
			mp.log("onPlayerDriftingEnd got ex: " + ex.message);
		}
	});

	const checkLevel = (rp, player) => {
		try {
			if (player.getVariable("guest") !== true) {
				const username = player.getVariable("username");
				const store = db.readStore(`${username}/gamedata`);
				let l = parseInt(db.getValue(store, "level", "0")) || 0;
				let maxXP = Math.floor(Math.pow(2.2, l + 20) / Math.pow(2, l + 20) * 2);
				if (maxXP <= rp) {
					while (maxXP <= rp) {
						l += 1;
						rp -= maxXP;
					}
					mp.players.call("playerLevelUp", [ username, l ]);
				}
				maxXP = Math.floor(Math.pow(2.2, l + 20) / Math.pow(2, l +20) * 2);
				db.setValue(store, "level", l);
				db.setValue(store, "rp", rp);
				db.setValue(store, "maxLevel", maxXP);
				db.writeStore(store);
				player.call("levelUp", [ l, rp, maxXP ]);
			}
		} catch (ex) {
			mp.log("checkLevel ex 1");
		}
	};
})();
