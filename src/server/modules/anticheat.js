(function() {
	mp.events.add("playerJoin", (player) => {
		try {
			player.setVariable("cheat", false);
			player.setVariable("godmode", false);
		} catch (ex) {
			mp.log("ac ex 1");
		}
	});

	mp.events.add("cheat", (player, cheat) => {
		try {
			player.setVariable("cheat", true);
			mp.log(`${player.name} cheat detected [${cheat}]`);
		} catch (ex) {
			mp.log("ac ex 2");
		}
	});

	mp.events.add("godmode", (player, stat) => {
		try {
			if (player.getVariable("godmode") !== (stat === "on")) {
				if (stat !== "on") {
					const vehicleId = player.getVariable("vehicleId");
					if (false !== vehicleId) {
						const vehicle = mp.vehicles.at(vehicleId);
						if (vehicle && mp.vehicles.exists(vehicle.id)) {
							if (vehicle.getVariable("owner") === player.id) {
								player.setVariable("vehicleId", false);
								vehicle.destroy();
							}
						}
					}
				}

				player.setVariable("godmode", stat === "on");
				mp.log(`${player.name} godmode is ${stat}`);
			}
		} catch (ex) {
			mp.log("ac ex 3");
		}
	});
})();
