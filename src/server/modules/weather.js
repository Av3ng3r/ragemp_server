(function() {
	const decisions = {
		"EXTRASUNNY": [ "CLEAR" ],
		"CLEAR": [ "CLOUDS", "EXTRASUNNY" ],
		"CLOUDS": [ "OVERCAST", "CLEAR" ],
		"OVERCAST": [ "RAIN", "CLOUDS" ],
		"RAIN": [ "THUNDER", "CLEARING" ],
		"THUNDER": [ "RAIN" ],
		"CLEARING": [ "OVERCAST" ],
	};

	let currentWeather = mp.world.weather || "EXTRASUNNY";
	let dropping = true;

	const getNextWeather = (weather) => {
		try {
			let rate = dropping ? 0.8 : 0.3;
			if (Math.random() > rate) {
				let choices = decisions[weather];
				if (choices) {
					const len = choices.length;
					let choice = 0;
					if ((choices.length > 1) && (!dropping)) choice = 1;
					return choices[choice];
				}
			}
		} catch (ex) {
			mp.log("getNextWeather got ex: " + ex.message);
		}
		return currentWeather;
	};

	const checkDateBetween = (dateFrom, dateTo) => {
		const d1 = dateFrom.split("/");
		const d2 = dateTo.split("/");

		const from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);
		const to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
		const check = new Date();

		return (check >= from && check <= to);
	};

	const setNextWeather = () => {
		try {
			const d = new Date();
			const thisYear = d.getFullYear();
			if (checkDateBetween(`24/12/${thisYear}`, `31/12/${thisYear}`)) {
				if (mp.world.weather !== "XMAS") mp.world.weather = "XMAS";
			} else if (checkDateBetween(`1/1/${thisYear}`, `5/1/${thisYear}`)) {
				if (mp.world.weather !== "XMAS") mp.world.weather = "XMAS";
			} else if (checkDateBetween(`30/10/${thisYear}`, `01/11/${thisYear}`)) {
				if (mp.world.weather !== "HALLOWEEN") mp.world.weather = "HALLOWEEN";
				return;
			} else if (mp.world.weather === "XMAS" || mp.world.weather === "HALLOWEEN") {
				mp.world.weather = "EXTRASUNNY";
			}
		} catch (ex) {}

		try {
			const nextWeather = getNextWeather(currentWeather);
			mp.players.call("setWeather", [ nextWeather, 15.0 ]);
			currentWeather = nextWeather;
			if (currentWeather === "EXTRASUNNY") dropping = true;
			else if (currentWeather === "THUNDER") dropping = false;
		} catch (ex) {
			mp.log("setNextWeather got ex: " + ex.message);
		}
	};

	const checkBarometer = () => {
		if (currentWeather === "EXTRASUNNY") dropping = true;
		else if (currentWeather === "THUNDER") dropping = false;
        else dropping = (Math.random() > 0.7) ? true : false;
	};

	setNextWeather();
    setInterval(setNextWeather, 300 * 1000);
    setInterval(checkBarometer, 1800 * 1000);

	mp.events.add("playerJoin", (player) => {
		try {
			player.call("setWeather", [ currentWeather, 15.0 ]);
		} catch (ex) {
			mp.log("playerJoin - weather ex: " + ex.message);
		}
	});
})();
