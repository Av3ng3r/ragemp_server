(function() {
	const fs = require("fs");
	const ABANDONED_VEHICLES = "/abandoned.json";
	const DEFAULT_VEHICLES = "/default.vehicles.json";

	const autosubs = {
		"landshark": [ "bati", "seashark" ],
	};

	mp.events.add("spawnVehicle", (player, name, getIn, mods, isBig, cls) => {
		try {
			if (mp.players.exists(player.id)) {
				const pos = player.position;
				let heading = player.heading;
				if (!getIn) pos.x += 2;

				if (player.vehicle) try {
					heading = player.vehicle.heading;
				} catch (ex) {
					heading = player.heading;
				}

				const vehicleId = player.getVariable("vehicleId");
				if (false !== vehicleId) {
					const vehicle = mp.vehicles.at(vehicleId);
					if (vehicle && mp.vehicles.exists(vehicle.id)) {
						if (vehicle.getVariable("owner") === player.id) {
							player.setVariable("vehicleId", false);
							vehicle.destroy();
						}
					}
				}

				const sname = name;
				const autosub = autosubs[name] || false;
				if (autosub) name = autosub[1];

				if (name) {
					let plate = player.getVariable("username") || player.name;
					plate = plate.replace("-", " ").replace("_", " ");
					if (plate.length > 8 && plate.indexOf(" ") > 5) {
						plate = plate.substring(0, plate.indexOf(" "));
					} else if (plate.length > 8) plate = plate.replace(/\s/g, "");

					if (plate.length < 6) plate += " 01";
					else if (plate.length < 7) plate += " 1";
					else if (plate.length < 8) plate += "1";

					const joaat = mp.joaat(name);
					if (isBig) pos.z += 3;

					const vehicle = mp.vehicles.new(joaat, pos, {
						heading: heading,
						dimension: player.dimension
					});
					vehicle.numberPlate = plate;

					if (getIn) player.putIntoVehicle(vehicle, -1);

					if (!mods || mods === "{}") {
						const one = Math.random() < 0.5;
						const color1 = Math.floor(Math.random() * 160);
						const color2 = one ? color1 : Math.floor(Math.random() * 160);

						mods = JSON.stringify({
							"color1": color1,
							"color2": color2,
							"pearlColor": Math.floor(Math.random() * 160),
							"primaryRgb": JSON.stringify({"r":0,"g":0,"b":0}),
							"secondaryRgb": JSON.stringify({"r":0,"g":0,"b":0})
						});
					}

					player.setVariable("vehicleId", vehicle.id);
					vehicle.setVariable("name", name);
					vehicle.setVariable("mods", mods);
					vehicle.setVariable("owner", player.id);
					vehicle.setVariable("class", `${cls}`);
					vehicle.setColorRGB(0,0,0,0,0,0);

					if (autosub !== false) {
						vehicle.setVariable("sub", autosub[0]);
						vehicle.setVariable("sname", sname);
					}

					const handling = mp.av.handling.getHandlingFor(name);
					if (handling) player.call("handling", [ name, JSON.stringify(handling) ]);

					mp.players.call("vehsync");
				}
			}
		} catch (ex) {
			mp.log("spawnVehicle got ex: " + ex.message);
		}
	});

	mp.events.add("mod", (player, vehicleId, mod, val) => {
		try {
			if (mp.players.exists(player.id)) {
				const vehicle = mp.vehicles.at(vehicleId);
				if (vehicle) {
					let mods = {};
					const data = vehicle.getVariable("mods");
					if (data) mods = JSON.parse(data);
					if (mods && mods[mod] !== val) {
						mods[mod] = val;
						vehicle.setVariable("mods", JSON.stringify(mods));
						mp.players.call("vehsync");
					}
				}
			}
		} catch (ex) {
			mp.log("mod got ex: " + ex.message);
		}
	});

	mp.events.add("repairVehicle", (player, vehicleId) => {
		try {
			const vehicle = mp.vehicles.at(vehicleId);
			if (vehicle) {
				vehicle.repair();
				vehicle.setVariable("dirt", 0);
				mp.players.call("vehsync");
			}
		} catch (ex) {
			mp.log("repairVehicle got ex: " + ex.message);
		}
	});

	mp.events.add("toggleDoor", (player, id, open, door) => {
		try {
			mp.players.call("toggleDoor", [ id, open, door ]);
		} catch (ex) {
			mp.log("toggleDoor ex: " + ex.message);
		}
	});

	mp.events.add("claimVehicle", (player, vehicleId) => {
		try {
			const vehicle = mp.vehicles.at(vehicleId);
			if (vehicle) {
				player.setVariable("vehicleId", vehicle.id);
				vehicle.setVariable("owner", player.id);
				// vehicle.setVariable("abandoned", false);
			}
		} catch (ex) {
			mp.log("claimVehicle got ex: " + ex.message);
		}
	});

	mp.events.add("abandonVehicle", (player, vehicleId) => {
		return;

		try {
			const vehicle = mp.vehicles.at(vehicleId);
			if (vehicle && vehicle.bodyHealth && vehicle.engineHealth && !vehicle.dead) {
				// vehicle.setVariable("abandoned", true);
				vehicle.repair();
			} else if (vehicle) vehicle.destroy();
		} catch (ex) {
			mp.log("abandonVehicle got ex: " + ex.message);
		}
	});

	mp.events.add("playerQuit", (player) => {
		try {
			const vehicleId = player.getVariable("vehicleId");
			const loggedIn = player.getVariable("loggedIn");
			if (vehicleId !== false && loggedIn) {
				const vehicle = mp.vehicles.at(vehicleId);
				if (vehicle) vehicle.destroy();
				/*
				if (vehicle && vehicle.bodyHealth && vehicle.engineHealth && !vehicle.dead) {
					const players = vehicle.getOccupants();
					if (players && players.length === 0) {
						vehicle.setVariable("abandoned", true);
						vehicle.repair();
					} else vehicle.destroy();
				} else if (vehicle) vehicle.destroy();
				*/
			}
		} catch (ex) {
			mp.log("playerQuit got ex: " + ex.message);
		}
	});

	mp.relativeRotation = (rotation) => {
		const {x, y, z} = rotation;
		const rx = (360-z) / 180 * Math.PI;
		const ry = z / 180 * Math.PI;

		return {
			x: x * -Math.cos(rx) + y * Math.sin(rx),
			y: y * -Math.cos(ry) + x * Math.sin(ry),
			z
		};
	};

	mp.events.add("lock", (player, newState) => {
		const vehicleId = player.getVariable("vehicleId");
		if (false !== vehicleId) {
			const vehicle = mp.vehicles.at(vehicleId);
			if (vehicle && mp.vehicles.exists(vehicle.id)) {
				vehicle.locked = newState === "true";
				if (newState === "true") {
					player.notify(`vehicle doors now ~r~locked`);
				} else {
					player.notify(`vehicle doors now ~g~unlocked`);
				}
			}
		}
	});

	// TODO: save the abandoned vehicles
	/*
	mp.events.add("restarting", () => {
		try {
			const vehicles = [];
			mp.vehicles.forEach(v => {
				try {
					if (v.getVariable("default") !== true) {
						vehicles.push({
							m: v.model,
							p: v.position,
							r: mp.relativeRotation(v.rotation),
							d: {
								m: v.getVariable("mods"),
								n: v.getVariable("name"),
								c: v.getVariable("class"),
								d: v.getVariable("default"),
								p: v.numberPlate,
							}
						});
					}
				} catch (ex) {
					mp.log("pushing got ex: " + ex.message);
				}
			});
			fs.writeFileSync(ABANDONED_VEHICLES, JSON.stringify(vehicles));
		} catch (ex) {
			mp.log("restarting got ex");
		}
	});
	*/

	/*
	const shuffle = (array) => {
		let currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;
			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	};

	const shouldCreateVehicleAt = (pos, data) => {
		try {
			for (let i = 0; i < data.length; i++) {
				const { x, y, z } = data[i].p;
				const dx = (x-pos.x)*(x-pos.x);
				const dy = (y-pos.y)*(y-pos.y);
				if (Math.sqrt(dx+dy) < 3) return false;
			}
		} catch (ex) {}
		return true;
	};

	const respawnSavedVehicles = (filename, max, dfl, existing) => {
		mp.log("creating vehicles from " + filename);
		if (fs.existsSync(filename)) try {
			const data = fs.readFileSync(filename, "utf-8");
			let vehicles = JSON.parse(data);
			if (!dfl) vehicles = vehicles.filter(v => !v.d.d);

			shuffle(vehicles).forEach((v, i) => {
				if (!shouldCreateVehicleAt(v.p, existing)) {
					mp.log("not spawning vehicle at ", v.p);
					return;
				}

				if (max == 0 || i < max) try {
					const vehicle = mp.vehicles.new(v.m, v.p, { heading: v.r.z });
					vehicle.setVariable("mods", v.d.m);
					vehicle.setVariable("name", v.d.n);
					vehicle.setVariable("class", v.d.c);
					vehicle.setVariable("abandoned", true);
					if (dfl) vehicle.setVariable("default", true);
					vehicle.numberPlate = v.d.p || Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(0, 8).toUpperCase();
				} catch (ex) {
					mp.log("respawning ex 1");
				}
			});
		} catch (ex) {
			mp.log("respawning ex 2");
		}
	};

	setTimeout(() => {
		let data = [];
		try {
			data = fs.readFileSync(ABANDONED_VEHICLES, "utf-8");
			data = JSON.parse(data);
		} catch (ex) {
			data = [];
		}
		respawnSavedVehicles(DEFAULT_VEHICLES, 0, true, data);
		respawnSavedVehicles(ABANDONED_VEHICLES, 512, false, []);
	}, 1000);
	*/
})();
