(function() {
	mp.minigame = {};
	mp.minigame.setPlayerIsPlaying = player => {
		try {
			if (mp.players.exists(player.id)) {
				player.setVariable("isPlayingMinigame", true);
			}
		} catch (ex) {
			mp.log("minigame ex 1");
		}
	};

	mp.minigame.setPlayerIsNotPlaying = player => {
		try {
			if (mp.players.exists(player.id)) {
				player.setVariable("isPlayingMinigame", false);
			}
		} catch (ex) {
			mp.log("minigame ex 2");
		}
	};

	mp.minigame.isPlayingMinigame = (player) => {
		try {
			if (mp.players.exists(player.id)) {
				return player.getVariable("isPlayingMinigame") === true;
			}
			return false;
		} catch (ex) {
			mp.log("minigame ex 3");
		}
	};
})();
