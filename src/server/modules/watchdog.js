(function() {
	let restartTimer = 0;
	function stopRestartTimer() {
		if (restartTimer) {
			const t = restartTimer;
			restartTimer = 0;
			clearTimeout(t);
		}
	}

	function startRestartTimer() {
		try {
		stopRestartTimer();
		mp.events.call("restarting");
		restartTimer = setTimeout(function() {
			mp.log("SIGKILL");
		}, 30000);
		} catch (ex) {
			mp.log("start restart ex 1");
		}
	}

	mp.events.add("playerJoin", function(player) {
		try {
			const len = mp.players.toArray().length;
			mp.log("PLAYERS=" + len);
			stopRestartTimer();
		} catch (ex) {
			mp.log("playerJoin - watchdog ex: " + ex.message);
		}
	});

	mp.events.add("playerQuit", function(player) {
		try {
			const len = mp.players.toArray().length - 1;
			mp.log("PLAYERS=" + len);
			if (len === 0) startRestartTimer();
		} catch (ex) {
			mp.log("playerQuit - watchdog ex: " + ex.message);
		}
	});
})();
