(function() {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
		terminal: false
	});

	const processCommand = (args) => {
		if (args.length) {
			const command = args.shift();
			switch (command) {
				case "BROADCAST":
					msg = args.join(" ");
					mp.players.broadcast(`!{ffff00}BROADCAST: ${msg}`);
					console.log("broadcasting " + msg);
					break;
				case "BANISH":
					console.log("implement me: banish");
					break;
				default:
					mp.log("unknown command: ", command);
					break;
			}
		}
	};

	const pr = () => process.stdout.write("\n> ");
	rl.on('line', (line) => {
		try {
			line = line.trim();
			if (line.length) processCommand(line.trim().split(" "));
		} catch (ex) {
			console.log("got ex: " + ex.message);
		}
		pr();
	})

	setTimeout(() => pr, 1000);
})();
