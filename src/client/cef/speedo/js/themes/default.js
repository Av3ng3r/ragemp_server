Speedometer.themes['default'] = {
  dial       : 'Black',
  rim        : 'White',
  rimArc     : 'Gray',
  thresh     : 'Transparent',
  center     : 'Orange',
  nose       : 'Orange',
  hand       : 'Orange',
  handShine  : 'Orange',
  handShineTo: 'Orange',
  ticks      : 'White',
  marks      : 'White',
  strings    : 'White',
  digits     : 'White',
  font       : 'Sans-Serif'
};
