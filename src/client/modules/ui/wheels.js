mp.events.add("client_loaded", () => {
	mp.events.add("client_start", () => {
		mp.av.menu.wheels = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.wheels = createMenu();
	});

	let wheelData = [];
	const getWheelMods = () => {
		wheelData = [];
		const names = [
			"Sport", "Muscle", "Lowrider", "SUV", "Offroad",
			"Tuner", "Bike Wheels", "High End"
		];

		try {
			const vehicle = mp.players.local.vehicle;
			for (let wheelType = 0; wheelType < names.length; wheelType++) {
				if (mp.game.vehicle.isThisModelABike(vehicle.model)) {
					if (names[wheelType] !== "Bike Wheels") continue;
				} else {
					if (names[wheelType] === "Bike Wheels") continue;
				}

				vehicle.setWheelType(wheelType);
				const entry = {
					"type": wheelType,
					"name": T(names[wheelType]),
					"wheels": getModValues(vehicle, 23)
				};
				if (entry["wheels"].length > 0) wheelData.push(entry);
			}
		} catch (ex) {
			mp.gui.chat.push("getWheelMods got ex: " + ex.message);
			return [];
		}
		return wheelData;
	};

	const getModValues = (vehicle, modType) => {
		let rv = [];
		try {
			let numMods = vehicle.getNumMods(modType);
			for (let modValue = 0; modValue < numMods; modValue++) {
				const modName = vehicle.getModTextLabel(modType, modValue);
				if (modName) {
					let s = mp.game.ui.getLabelText(modName);
					if (s === "NULL") s = modName;
					rv.push({ "modName":  s, "modValue":  modValue });
				}
			}
		} catch (ex) {
			mp.gui.chat.push("getModValues got ex: " + ex.message);
			return [];
		}
		return rv;
	};

	const getWheelNamesForEntry = (entry) => {
		const rv = [];
		try {
			for (let i = 0; i < entry["wheels"].length; i++) {
				const w = entry["wheels"][i];
				rv.push(w["modName"]);
			}
		} catch (ex) {
			mp.gui.chat.push("getWheelNamesForEntry got ex: " + ex.message);
			return [];
		}
		return rv;
	};

	const populateMenu = (menu) => {
		let rv = false;
		menu.Clear();

		if (!mp.players.local.vehicle) {
			menu.AddItem(new mp.ui.UIMenuItem(T("Not in a vehicle")));
			return;
		}

		try {
			const data = getWheelMods();
			for (let i = 0; i < data.length; i++) {
				const entry = data[i];
				rv = true;
				menu.AddItem(new mp.ui.UIMenuListItem(
					entry["name"], "",
					new mp.ui.ItemsCollection(getWheelNamesForEntry(entry))
				));
			}
		} catch (ex) {
			mp.gui.chat.push("populateMenu got ex: " + ex.message);
		}
		return rv;
	};

	const createMenu = () => {
		let menu = new mp.ui.Menu(T("Wheels"), "",
			new mp.ui.Point(50, 50));

		menu.ItemSelect.on(item => {
			const vehicle = mp.players.local.vehicle;
			let group = wheelData.find(entry => entry.name == item.Text);
			if (group && vehicle) {
				let entry = group["wheels"].find(w => w.modName == item.SelectedItem.DisplayText);
				if (entry) {
					const data = {
						type: group.type,
						value: entry.modValue
					};
					mp.events.callRemote("mod", vehicle.remoteId, "wheels", JSON.stringify(data));
					return;
				}
			}
		});

		menu._Open = menu.Open;
		menu.Open = function() {
			if (populateMenu(this)) this._Open();
			else mp.av.menu.showMenu("mods");
		}.bind(menu);

		menu.Close();
		return menu;
	};
});
