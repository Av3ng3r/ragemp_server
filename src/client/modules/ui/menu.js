mp.events.add("client_loaded", () => {
	mp.av.menu = {};

	const getMenuFromName = (name) => {
		return mp.av.menu[name];
	};

	mp.av.menu.menuVisible = () => {
		return (mp.av.menu.main && mp.av.menu.main.Visible) ||
			(mp.av.menu.vehicle && mp.av.menu.vehicle.Visible) ||
			(mp.av.menu.weapon && mp.av.menu.weapon.Visible) ||
			(mp.av.menu.user && mp.av.menu.user.Visible) ||
			(mp.av.menu.skin && mp.av.menu.skin.Visible) ||
			(mp.av.menu.mods && mp.av.menu.mods.Visible) ||
			(mp.av.menu.showcase && mp.av.menu.showcase.Visible) ||
			(mp.av.menu.paint && mp.av.menu.paint.Visible) ||
			(mp.av.menu.neon && mp.av.menu.neon.Visible) ||
			(mp.av.menu.wheels && mp.av.menu.wheels.Visible) ||
			(mp.av.menu.mechanic && mp.av.menu.mechanic.Visible) ||
			(mp.av.menu.users && mp.av.menu.users.Visible) ||
			(mp.av.menu.settings && mp.av.menu.settings.Visible);
	};

	mp.av.menu.hideMenu = (name) => {
		const menu = getMenuFromName(name);
		if (menu) {
			menu.Close();
			menu.isVisible = false;
		}
	};

	mp.av.menu.hideAll = () => {
		mp.av.menu.hideMenu("main");
		mp.av.menu.hideMenu("vehicle");
		mp.av.menu.hideMenu("users");
		mp.av.menu.hideMenu("weapon");
		mp.av.menu.hideMenu("skin");
		mp.av.menu.hideMenu("settings");
		mp.av.menu.hideMenu("showcase");
		mp.av.menu.hideMenu("mods");
		mp.av.menu.hideMenu("paint");
		mp.av.menu.hideMenu("neon");
		mp.av.menu.hideMenu("wheels");
		mp.av.menu.hideMenu("mechanic");
		mp.av.menu.hideMenu("mechanic");
	};

	mp.av.menu.showMenu = (name, data) => {
		mp.av.menu.hideAll();
		const menu = getMenuFromName(name);
		if (menu) {
			if (data && menu.setData) menu.setData(data);
			menu.Open();
			menu.isVisible = true;
		}
	};

	mp.av.menu.back = () => {
		if (mp.av.menu.users && mp.av.menu.users.isVisible) {
			mp.av.menu.hideMenu("users");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.users && mp.av.menu.users.isVisible) {
			mp.av.menu.hideMenu("users");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.mechanic && mp.av.menu.mechanic.isVisible) {
			mp.av.menu.hideMenu("mechanic");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.wheels && mp.av.menu.wheels.isVisible) {
			mp.av.menu.hideMenu("wheels");
			mp.av.menu.showMenu("mods");
		} else if (mp.av.menu.neon && mp.av.menu.neon.isVisible) {
			mp.av.menu.hideMenu("neon");
			mp.av.menu.showMenu("mods");
		} else if (mp.av.menu.paint && mp.av.menu.paint.isVisible) {
			mp.av.menu.hideMenu("paint");
			mp.av.menu.showMenu("mods");
		} else if (mp.av.menu.showcase && mp.av.menu.showcase.isVisible) {
			mp.av.menu.hideMenu("showcase");
			mp.av.menu.showMenu("vehicle");
		} else if (mp.av.menu.mods && mp.av.menu.mods.isVisible) {
			mp.av.menu.hideMenu("mods");
			mp.av.menu.showMenu("vehicle");
		} else if (mp.av.menu.vehicle && mp.av.menu.vehicle.isVisible) {
			mp.av.menu.hideMenu("vehicle");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.weapon && mp.av.menu.weapon.isVisible) {
			mp.av.menu.hideMenu("weapon");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.skin && mp.av.menu.skin.isVisible) {
			mp.av.menu.hideMenu("skin");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.settings && mp.av.menu.settings.isVisible) {
			mp.av.menu.hideMenu("settings");
			mp.av.menu.showMenu("main");
		} else if (mp.av.menu.main && mp.av.menu.main.isVisible) {
			mp.av.menu.hideMenu("main");
		}
	};

	mp.events.add("client_start", () => {
		mp.keys.bind(113, false, () => {
			if (mp.players.local.getVariable("banished")) {
				mp.av.menu.hideAll();
				return;
			}

			// a minigame is responsible for its own menu
			if (mp.av.minigame.isPlayingMinigame) return;

			if (mp.av.menu.menuVisible()) {
				mp.av.menu.hideAll();
			} else {
				mp.av.menu.showMenu("main");
			}
		});

		mp.keys.bind(8, false, () => {
			if (mp.players.local.getVariable("banished")) {
				mp.av.menu.hideAll();
				return;
			}

			mp.av.menu.back();
		});
	});
});
