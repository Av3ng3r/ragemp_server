mp.events.add("client_loaded", () => {
	mp.events.add("client_start", () => {
		mp.av.menu.weapon = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.weapon = createMenu();
	});

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Weapons"), T("all the best from ammunation"),
				new mp.ui.Point(50, 50));

		const types = getWeaponTypes();
		types.forEach(type => {
			const weapons = getWeaponNamesForType(type);
			if (weapons && weapons.length > 0) {
				menu.AddItem(new mp.ui.UIMenuListItem(
					type, T("give it to me"),
					new mp.ui.ItemsCollection(weapons)
				));
			}
		});

		menu.ItemSelect.on(item => {
			const model = getWeaponModelFromName(item.SelectedItem.DisplayText);
			if (model) try {
				mp.players.local.giveWeapon(mp.game.joaat(model), 1000, true);
			} catch (ex) {}
		});

		menu.Close();
		return menu;
	};

	const weapons = [
		["Melee", [
			["Knife", "weapon_knife"],
			["Police bat", "weapon_nightstick"],
			["Hammer", "weapon_hammer"],
			["Bat", "weapon_bat"],
			["Crowbar", "weapon_crowbar"],
			["Golf Club", "weapon_golfclub"],
			["Broken Bottle", "weapon_bottle"],
			["Dagger", "weapon_dagger"],
			["Hatchet", "weapon_hatchet"],
			["Brass Knuckles", "weapon_knuckle"],
			["Machete", "weapon_machete"],
			["Flashlight", "weapon_flashlight"],
			["Switchblade", "weapon_switchblade"],
			["Wrench", "weapon_wrench"],
			["Battle Axe", "weapon_battleaxe"]
		]],
		["Pistols", [
			["Pistol", "weapon_pistol"],
			["Pistol MK2", "weapon_pistol_mk2"],
			["Combat Pistol", "weapon_combatpistol"],
			["Pistol .50", "weapon_pistol50"],
			["SNS Pistol", "weapon_snspistol"],
			["Heavy Pistol", "weapon_heavypistol"],
			["Vintage Pistol", "weapon_vintagepistol"],
			["Marksman Pistol", "weapon_marksmanpistol"],
			["Revolver", "weapon_revolver"],
			["AP Pistol", "weapon_appistol"]
		]],
		["SMG", [
			["Micro SMG", "weapon_microsmg"],
			["Machine Pistol", "weapon_machinepistol"],
			["SMG", "weapon_smg"],
			["SMG MK2", "weapon_smg_mk2"],
			["Assault SMG", "weapon_assaultsmg"],
			["Combat PDW", "weapon_combatpdw"],
			["Mini SMG", "weapon_minismg"]
		]],
		["Machineguns", [
			["Machinegun", "weapon_mg"],
			["Combat Machinegun", "weapon_combatmg"],
			["Combat Machinegun MK2", "weapon_combatmg_mk2"],
			["Machinegun Gusenberg", "weapon_gusenberg"]
		]],
		["Assault Rifles", [
			["Assault Rifle", "weapon_assaultrifle"],
			["Assault Rifle MK2", "weapon_assaultrifle_mk2"],
			["Carbine Rifle", "weapon_carbinerifle"],
			["Carbine Rifle MK2", "weapon_carbinerifle_mk2"],
			["Advanced Rifle", "weapon_advancedrifle"],
			["Special Carbine", "weapon_specialcarbine"],
			["Bullpup Riffle", "weapon_bullpuprifle"],
			["Compact Rifle", "weapon_compactrifle"]
		]],
		["Sniper Rifles", [
			["Sniper Rifle", "weapon_sniperrifle"],
			["Heavy Sniper Rifle", "weapon_heavysniper"],
			["Heavy Sniper Rifle MK2", "weapon_heavysniper_mk2"],
			["Marksman Rifle", "weapon_marksmanrifle"]
		]],
		["Shotguns", [
			["Pump Shotgun", "weapon_pumpshotgun"],
			["Sawn-off Shotgun", "weapon_sawnoffshotgun"],
			["Bullpup Shotgun", "weapon_bullpupshotgun"],
			["Assault Shutgun", "weapon_assaultshotgun"],
			["Musket", "weapon_musket"],
			["Heavy Shotgun", "weapon_heavyshotgun"],
			["Double Barrel Shotgun", "weapon_dbshotgun"],
			["Auto-Shotgun", "weapon_autoshotgun"]
		]],
		["Heavy Weapons", [
			["RPG", "weapon_rpg"],
			["Firework Launcher", "weapon_firework"],
			["Homing Launcher", "weapon_hominglauncher"],
			["Grenade Launcher", "weapon_grenadelauncher"],
			["Compect Grenade Launcher", "weapon_compactlauncher"],
			["Minigun", "weapon_minigun"],
			["Railgun", "weapon_railgun"]
		]],
		["Throwing Weapons", [
			["Grenade", "weapon_grenade"],
			["Sticky Bomb", "weapon_stickybomb"],
			["Proximity Mine", "weapon_proximitymine"],
			["Molotov", "weapon_molotov"],
			["Petrol Canister", "weapon_petrolcan"],
			["Flare", "weapon_flare"],
			["Ball", "weapon_ball"],
			["Showball", "weapon_snowball"],
			["Smoke Grenade", "weapon_smokegrenade"]
		]]
	];

	const getWeaponTypes = () => {
		return weapons.map(entry => entry[0]);
	}

	const getWeaponNamesForType = (type) => {
		for (let i = 0; i < weapons.length; i++) {
			const group = weapons[i];
			if (group[0] === type) {
				return group[1].map(entry => entry[0]);
			}
		}
		return [];
	}

	const getWeaponModelFromName = (name) => {
		let model = "";
		for (let i = 0; i < weapons.length; i++) {
			const entry = weapons[i];
			model = entry[1].find(item => item[0] == name);
			if (model) break;
		}
		if (model) return model[1];
		return null;
	}
});
