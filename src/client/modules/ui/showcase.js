mp.events.add("client_loaded", () => {
	mp.events.add("toggleDoor", (id, open, door) => {
		const vehicle = mp.vehicles.atRemoteId(id);
		if (vehicle) {
			if (door < 10) {
				if (open) vehicle.setDoorOpen(door, false, false);
				else vehicle.setDoorShut(door, false);
			} else switch (door) {
				case 100:
					vehicle.setEngineOn(open, true, false);
					break;
			}
		}
	});

	let state = {};
	mp.events.add("resetVehicleStuff", () => state = {});

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Showcase"), "",
			new mp.ui.Point(50, 50));

		menu.AddItem(new mp.ui.UIMenuItem(T("Open/Close hood")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Open/Close door 1")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Open/Close door 2")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Open/Close door 3")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Open/Close door 4")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Open/Close trunk")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Toggle engine")));

		menu.ItemSelect.on(item => {
			const remoteId = mp.players.local.getVariable("vehicleId");
			switch (item.Text) {
				case T("Open/Close hood"):
					if (state.door4) {
						state.door4 = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 4);
					} else {
						state.door4 = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 4);
					}
					break;
				case T("Open/Close door 1"):
					if (state.door0) {
						state.door0 = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 0);
					} else {
						state.door0 = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 0);
					}
					break;
				case T("Open/Close door 2"):
					if (state.door1) {
						state.door1 = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 1);
					} else {
						state.door1 = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 1);
					}
					break;
				case T("Open/Close door 3"):
					if (state.door2) {
						state.door2 = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 2);
					} else {
						state.door2 = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 2);
					}
					break;
				case T("Open/Close door 4"):
					if (state.door3) {
						state.door3 = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 3);
					} else {
						state.door3 = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 3);
					}
					break;
				case T("Open/Close trunk"):
					if (state.door5) {
						state.door5 = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 5);
					} else {
						state.door5 = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 5);
					}
					break;
				case T("Toggle engine"):
					if (state.engine) {
						state.engine = false;
						mp.events.callRemote("toggleDoor", remoteId, false, 100);
					} else {
						state.engine = true;
						mp.events.callRemote("toggleDoor", remoteId, true, 100);
					}
					break;
			}
		});

		menu.Close();
		return menu
	};

	mp.events.add("client_start", () => {
		mp.av.menu.showcase = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.showcase = createMenu();
	});
});
