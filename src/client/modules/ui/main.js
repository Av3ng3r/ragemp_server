mp.events.add("client_loaded", () => {
	mp.events.add("client_start", () => {
		mp.av.menu.main = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.main = createMenu();
	});

	const createMenu = () => {
		let menu = new mp.ui.Menu(T("Main Menu"), "",
			new mp.ui.Point(50, 50));

		menu.AddItem(new mp.ui.UIMenuItem(T("Player...")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Mechanic...")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Vehicles...")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Weapons...")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Users...")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Settings...")));

		menu.ItemSelect.on(item => {
			switch (item.Text) {
				case T("Player..."):
					mp.av.menu.hideMenu("main");
					mp.av.menu.showMenu("skin");
					break;
				case T("Mechanic..."):
					mp.av.menu.hideMenu("main");
					mp.av.menu.showMenu("mechanic");
					break;
				case T("Vehicles..."):
					mp.av.menu.hideMenu("main");
					mp.av.menu.showMenu("vehicle");
					break;
				case T("Weapons..."):
					mp.av.menu.hideMenu("main");
					mp.av.menu.showMenu("weapon");
					break;
				case T("Users..."):
					mp.av.menu.hideMenu("main");
					mp.av.menu.showMenu("users");
					break;
				case T("Settings..."):
					mp.av.menu.hideMenu("main");
					mp.av.menu.showMenu("settings");
					break;
			}
		});

		menu.onLanguageChange = createMenu;
		menu.Close();
		return menu;
	};
});
