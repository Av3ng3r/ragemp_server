mp.events.add("client_loaded", () => {

	mp.events.add("client_start", () => {
		mp.av.menu.paint = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.paint = createMenu();
	});

	const primary = { r: 0, g: 0, b: 0 };
	const secondary = { r: 0, g: 0, b: 0 };

	let colour1 = 0;
	let colour2 = 0;
	let pearlColor = 0;
	let wheelColor = 0;
	let primaryEntry = null;
	let secondaryEntry = null;

	const rgbToMenu = (col) => {
		return {
			r: Math.floor(col.r / 8) * 8,
			g: Math.floor(col.g / 8) * 8,
			b: Math.floor(col.b / 8) * 8,
		}
	};

	const getPrimaryColour = () => {
		const col = mp.players.local.vehicle.getCustomPrimaryColour(255, 255, 255);
		primary.r = +col.r;
		primary.g = +col.g;
		primary.b = +col.b;
		return col;
	};

	const getSecondaryColour = () => {
		const col = mp.players.local.vehicle.getCustomSecondaryColour(255, 255, 255);
		secondary.r = +col.r;
		secondary.g = +col.g;
		secondary.b = +col.b;
		return col;
	};

	const standardColors = [
		"Custom RGB", "Metallic Graphite Black", "Metallic Black Steal", "Metallic Dark Silver",
		"Metallic Silver", "Metallic Blue Silver", "Metallic Steel Gray", "Metallic Shadow Silver",
		"Metallic Stone Silver", "Metallic Midnight Silver", "Metallic Gun Metal",
		"Metallic Anthracite Grey", "Matte Black", "Matte Gray", "Matte Light Grey", "Util Black",
		"Util Black Poly", "Util Dark silver", "Util Silver", "Util Gun Metal", "Util Shadow Silver",
		"Worn Black", "Worn Graphite", "Worn Silver Grey", "Worn Silver", "Worn Blue Silver",
		"Worn Shadow Silver", "Metallic Red", "Metallic Torino Red", "Metallic Formula Red",
		"Metallic Blaze Red", "Metallic Graceful Red", "Metallic Garnet Red", "Metallic Desert Red",
		"Metallic Cabernet Red", "Metallic Candy Red", "Metallic Sunrise Orange",
		"Metallic Classic Gold", "Metallic Orange", "Matte Red", "Matte Dark Red", "Matte Orange",
		"Matte Yellow", "Util Red", "Util Bright Red", "Util Garnet Red", "Worn Red",
		"Worn Golden Red", "Worn Dark Red", "Metallic Dark Green", "Metallic Racing Green",
		"Metallic Sea Green", "Metallic Olive Green", "Metallic Green",
		"Metallic Gasoline Blue Green", "Matte Lime Green", "Util Dark Green", "Util Green",
		"Worn Dark Green", "Worn Green", "Worn Sea Wash", "Metallic Midnight Blue",
		"Metallic Dark Blue", "Metallic Saxony Blue", "Metallic Blue", "Metallic Mariner Blue",
		"Metallic Harbor Blue", "Metallic Diamond Blue", "Metallic Surf Blue",
		"Metallic Nautical Blue", "Metallic Bright Blue", "Metallic Purple Blue",
		"Metallic Spinnaker Blue", "Metallic Ultra Blue", "Metallic Bright Blue", "Util Dark Blue",
		"Util Midnight Blue", "Util Blue", "Util Sea Foam Blue", "Util Lightning blue",
		"Util Maui Blue Poly", "Util Bright Blue", "Matte Dark Blue", "Matte Blue",
		"Matte Midnight Blue", "Worn Dark blue", "Worn Blue", "Worn Light blue",
		"Metallic Taxi Yellow", "Metallic Race Yellow", "Metallic Bronze", "Metallic Yellow Bird",
		"Metallic Lime", "Metallic Champagne", "Metallic Pueblo Beige", "Metallic Dark Ivory",
		"Metallic Choco Brown", "Metallic Golden Brown", "Metallic Light Brown",
		"Metallic Straw Beige", "Metallic Moss Brown", "Metallic Biston Brown", "Metallic Beechwood",
		"Metallic Dark Beechwood", "Metallic Choco Orange", "Metallic Beach Sand",
		"Metallic Sun Bleeched Sand", "Metallic Cream", "Util Brown", "Util Medium Brown",
		"Util Light Brown", "Metallic White", "Metallic Frost White", "Worn Honey Beige",
		"Worn Brown", "Worn Dark Brown", "Worn straw beige", "Brushed Steel", "Brushed Black steel",
		"Brushed Aluminium", "Chrome", "Worn Off White", "Util Off White", "Worn Orange",
		"Worn Light Orange", "Metallic Securicor Green", "Worn Taxi Yellow", "police car blue",
		"Matte Green", "Matte Brown", "Worn Orange", "Matte White", "Worn White",
		"Worn Olive Army Green", "Pure White", "Hot Pink", "Salmon pink", "Metallic Vermillion Pink",
		"Orange", "Green", "Blue", "Mettalic Black Blue", "Metallic Black Purple",
		"Metallic Black Red", "hunter green", "Metallic Purple", "Metaillic V Dark Blue",
		"MODSHOP BLACK1", "Matte Purple", "Matte Dark Purple", "Metallic Lava Red",
		"Matte Forest Green", "Matte Olive Drab", "Matte Desert Brown", "Matte Desert Tan",
		"Matte Foilage Green", "DEFAULT ALLOY COLOR", "Epsilon Blue", "Pure Gold", "Brushed Gold"
	];
	const wheelColors = standardColors.filter((a, i) => i);

	const populateMenu = (menu) => {
		menu.Clear();

		if (!mp.players.local.vehicle) {
			menu.AddItem(new mp.ui.UIMenuItem(T("Not in a vehicle")));
			return;
		}

		colour1 = mp.players.local.vehicle.color1;
		colour2 = mp.players.local.vehicle.color2;

		let extra = mp.players.local.vehicle.getExtraColours(0, 0);
		pearlescentColor = extra.pearlescentColor;
		wheelColor = extra.wheelColor;

		primaryEntry = new mp.ui.UIMenuListItem(
			T("Primary Colour"), "",
			new mp.ui.ItemsCollection(standardColors),
			colour1
		);
		secondaryEntry = new mp.ui.UIMenuListItem(
			T("Secondary Colour"), "",
			new mp.ui.ItemsCollection(standardColors),
			colour2
		);
		menu.AddItem(primaryEntry);
		menu.AddItem(secondaryEntry);

		menu.AddItem(
			new mp.ui.UIMenuListItem(
				T("Pearlescent Colour"), "",
				new mp.ui.ItemsCollection(standardColors),
				pearlescentColor
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuListItem(
				T("Wheel Colour"), T("Carbon wheels cannot be painted"),
				new mp.ui.ItemsCollection(wheelColors),
				wheelColor
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Primary Red"),
				newArray(0, 255, 8),
				rgbToMenu(getPrimaryColour()).r / 8,
				T("Only used with Custom RGB"), false
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Primary Green"),
				newArray(0, 255, 8),
				rgbToMenu(getPrimaryColour()).g / 8,
				T("Only used with Custom RGB"), false
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Primary Blue"),
				newArray(0, 255, 8),
				rgbToMenu(getPrimaryColour()).b / 8,
				T("Only used with Custom RGB"), false
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Secondary Red"),
				newArray(0, 255, 8),
				rgbToMenu(getSecondaryColour()).r / 8,
				T("Only used with Custom RGB"), false
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Secondary Green"),
				newArray(0, 255, 8),
				rgbToMenu(getSecondaryColour()).g / 8,
				T("Only used with Custom RGB"), false
			)
		);
		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Secondary Blue"),
				newArray(0, 255, 8),
				rgbToMenu(getSecondaryColour()).b / 8,
				T("Only used with Custom RGB"), false
			)
		);
	};

	const newArray = (min, max, step) => {
		if (!step) step = 1;
		step = Math.abs(step);

		const rv = [];
		if (min < max) for (let i = min; i <= max; i+= step) rv.push(i.toString());
		if (min > max) for (let i = min; i >= max; i-= step) rv.push(i.toString());
		return rv;
	};

	const setPrimaryColour = (colour) => {
		if (Array.isArray(colour)) {
			if (colour.includes(standardColors[colour1])) return;
			colour = colour[0];
		}
		colour1 = standardColors.indexOf(colour);
		primaryEntry.Index = colour1;
		mp.events.callRemote(
			"mod", mp.players.local.vehicle.remoteId,
			"color1", colour1
		);
	};

	const setSecondaryColour = (colour) => {
		if (Array.isArray(colour)) {
			if (colour.includes(standardColors[colour2])) return;
			colour = colour[0];
		}
		colour2 = standardColors.indexOf(colour);
		secondaryEntry.Index = colour2;
		mp.events.callRemote(
			"mod", mp.players.local.vehicle.remoteId,
			"color2", colour2
		);
	};

	const setPearlescentColour = (colour) => {
		pearlColor = standardColors.indexOf(colour);
		mp.events.callRemote(
			"mod", mp.players.local.vehicle.remoteId,
			"pearlColor", pearlColor
		);
	};

	const setWheelColour = (colour) => {
		wheelColor = standardColors.indexOf(colour);
		mp.events.callRemote(
			"mod", mp.players.local.vehicle.remoteId,
			"wheelColor", wheelColor
		);
	};

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Paint Menu"), "",
			new mp.ui.Point(50, 50));

		populateMenu(menu);

		menu.ItemSelect.on(item => {
			switch (item.Text) {
				case T("Primary Colour"):
					setPrimaryColour(item.SelectedItem.DisplayText);
					break;
				case T("Secondary Colour"):
					setSecondaryColour(item.SelectedItem.DisplayText);
					break;
				case T("Pearlescent Colour"):
					setPearlescentColour(item.SelectedItem.DisplayText);
					break;
				case T("Wheel Colour"):
					setWheelColour(item.SelectedItem.DisplayText);
					break;
			}
		});

		menu.SliderChange.on((item, index, value) => {
			switch (item.Text) {
				case T("Primary Red"):
					setPrimaryColour(["Custom RGB", "Chrome"]);
					primary.r = parseInt(value);
					break;
				case T("Primary Green"):
					setPrimaryColour(["Custom RGB", "Chrome"]);
					primary.g = parseInt(value);
					break;
				case T("Primary Blue"):
					setPrimaryColour(["Custom RGB", "Chrome"]);
					primary.b = parseInt(value);
					break;
				case T("Secondary Red"):
					setSecondaryColour(["Custom RGB", "Chrome"]);
					secondary.r = parseInt(value);
					break;
				case T("Secondary Green"):
					setSecondaryColour(["Custom RGB", "Chrome"]);
					secondary.g = parseInt(value);
					break;
				case T("Secondary Blue"):
					setSecondaryColour(["Custom RGB", "Chrome"]);
					secondary.b = parseInt(value);
					break;
			}

			const remoteId = mp.players.local.vehicle.remoteId;
			mp.events.callRemote("mod", remoteId, "primaryRgb", JSON.stringify(primary));
			mp.events.callRemote("mod", remoteId, "secondaryRgb", JSON.stringify(secondary));
		});

		menu._Open = menu.Open;
		menu.Open = function() {
			populateMenu(this);
			this._Open();
		}.bind(menu);
		menu.Close();
		return menu;
	};
});
