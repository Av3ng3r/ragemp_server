mp.events.add("client_loaded", () => {
	const neon = { r: 0, g: 0, b: 0 };

	mp.events.add("client_start", () => {
		mp.av.menu.neon = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.neon = createMenu();
	});

	const createMenu = () => {
		let menu = new mp.ui.Menu(T("Neon Menu"), "",
			new mp.ui.Point(50, 50));

		populateMenu(menu);

		menu.SliderChange.on((item, index, value) => {
			switch (item.Text) {
				case T("Red"):
					neon.r = parseInt(value);
					break;
				case T("Green"):
					neon.g = parseInt(value);
					break;
				case T("Blue"):
					neon.b = parseInt(value);
					break;
			}
			const vehicle = mp.players.local.vehicle;
			if (vehicle) {
				mp.events.callRemote("mod", vehicle.remoteId, "neon", JSON.stringify(neon));
			}
		});

		menu._Open = menu.Open;
		menu.Open = Open.bind(menu);

		menu.Close();
		return menu;
	}

	function rgbToMenu(col)
	{
		return {
			r: Math.floor(col.r / 8) * 8,
			g: Math.floor(col.g / 8) * 8,
			b: Math.floor(col.b / 8) * 8,
		}
	}

	function getNeonColour()
	{
		const col = mp.players.local.vehicle.getNeonLightsColour(255, 255, 255);
		neon.r = col.r;
		neon.g = col.g;
		neon.b = col.b;
		return col;
	}

	function populateMenu(menu)
	{
		menu.Clear();

		if (!mp.players.local.vehicle) {
			menu.AddItem(new mp.ui.UIMenuItem(T("Not in a vehicle")));
			return;
		}

		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Red"),
				newArray(0, 255, 8),
				rgbToMenu(getNeonColour()).r / 8, T("The red tint"), false
			)
		);

		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Green"),
				newArray(0, 255, 8),
				rgbToMenu(getNeonColour()).g / 8, T("The green tint"), false
			)
		);

		menu.AddItem(
			new mp.ui.UIMenuSliderItem(T("Blue"),
				newArray(0, 255, 8),
				rgbToMenu(getNeonColour()).b / 8, T("The blue tint"), false
			)
		);
	}

	function Open()
	{
		populateMenu(this);
		this._Open();
	}

	function newArray(min, max, step)
	{
		if (!step) step = 1;
        step = Math.abs(step);

        let rv = [];
        if (min < max) for (let i = min; i <= max; i+= step) rv.push(i.toString());
        if (min > max) for (let i = max; i >= min; i-= step) rv.push(i.toString());
        return rv;
	}
});
