mp.events.add("client_loaded", () => {
	mp.events.add("client_start", () => {
		mp.av.menu.users = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.users = createMenu();
	});

	const populateMenu = (menu) => {
		menu.Clear();

		const players = mp.players.toArray();
		players.sort((a, b) => (a.name < b.name ? -1 : 1));
		players.forEach(player => {
			if (player.getVariable("loggedIn")) {
				menu.AddItem(new mp.ui.UIMenuItem(player.name));
			}
		});
	};

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Users"), "", new mp.ui.Point(50, 50));

		populateMenu(menu);
		menu.ItemSelect.on(item => {
			const players = mp.players.toArray();
			for (let i = 0; i < players.length; i++) {
				const player = players[i];
				if (item.Text === player.name) {
					mp.events.callRemote("warp", player.remoteId);
					break;
				}
			}
		});

		menu.Close();
		menu._Open = menu.Open;
		menu.Open = function() {
			populateMenu(this);
			this._Open();
		}.bind(menu);

		return menu;
	}
});
