mp.events.add("client_loaded", () => {
	const WINDOW_TINT = [
		"none", "black", "darksmoke", "lightsmoke", "stock", "limo", "green"
	];

	let modData = [];

	mp.events.add("client_start", () => {
		mp.av.menu.mods = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.mods = createMenu();
	});

	const getModForSlotName = (slotName) => {
		if (modData) {
			for (let i = 0; i < modData.length; i++) {
				if (modData[i].slotName === slotName) return modData[i];
			}
		}
		return null;
	};

	const getModValueForModName = (mod, modName) => {
		if (mod && modName) {
			for (let i = 0; i < mod.mods.length; i++) {
				if (mod.mods[i].modName == modName) return mod.mods[i];
			}
		}
		return null;
	}

	const getCurrentIndex = (entry) => {
		const player = mp.players.local;
		if (player) {
			const vehicle = player.vehicle;
			if (vehicle) {
				let rv = vehicle.getMod(parseInt(entry.modType));
				return rv < 0 ? 0 : rv;
			}
		}
		return 0;
	};

	const populateModsMenu = (menu) => {
		const vehicle = mp.players.local.vehicle;
		menu.Clear();
		if (!vehicle) {
			menu.AddItem(new mp.ui.UIMenuItem(T("Not in a vehicle")));
			modData = [];
		} else {
			let mods = mp.av.vehicle.getMods(vehicle);
			menu.AddItem(new mp.ui.UIMenuItem(T("Wheels...")));
			menu.AddItem(new mp.ui.UIMenuItem(T("Paint...")));
			menu.AddItem(new mp.ui.UIMenuItem(T("Neon...")));
			menu.AddItem(new mp.ui.UIMenuListItem(
				T("Window Tint"), "",
				new mp.ui.ItemsCollection(WINDOW_TINT),
				mods["windowTint"]
			));

			modData = mp.av.vehicles.getAllVehicleMods(vehicle)

			modData.forEach(mod => {
				menu.AddItem(new mp.ui.UIMenuListItem(
					mod.slotName, "",
					new mp.ui.ItemsCollection(mod.mods.map(m => m.modName)),
					getCurrentIndex(mod)
				));
			});
		}
	};

	// cannot do this as an arrow function for we will bind it at runtime
	function Open() {
		populateModsMenu(this);
		this._Open();
	};

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Vehicle Mods Menu"), "",
			new mp.ui.Point(50, 50));

		populateModsMenu(menu);

		menu.ItemSelect.on(item => {
			const vehicle = mp.players.local.vehicle;
			switch (item.Text) {
				case T("Wheels..."):
					mp.av.menu.showMenu("wheels");
					break;
				case T("Paint..."):
					mp.av.menu.showMenu("paint");
					break;
				case T("Neon..."):
					mp.av.menu.showMenu("neon");
					break;
				case T("Window Tint"):
					try {
						const i = WINDOW_TINT.indexOf(item.SelectedItem.DisplayText);
						mp.events.callRemote("mod", vehicle.remoteId, "windowTint", i);
					} catch (ex) {}
					break;
				default:
					const t = getModForSlotName(item.Text);
					if (t) {
						const v = getModValueForModName(t, item.SelectedItem.DisplayText);
						if (v) {
							mp.events.callRemote("mod", vehicle.remoteId, t.modType, v.modValue);
						}
					}
					break;
			}
		});

		menu.Close();
		menu.onLanguageChange = createMenu;
		menu._Open = menu.Open;
		menu.Open = Open.bind(menu);
		return menu;
	};
});
