mp.events.add("client_loaded", () => {
	let speedo = true;
	let nametags = true;
	let peds = true;
	let traffic = true;
	let warpblocked = false;
	let chatpos = "bottom";
	let lang = "English";
	let god = false;

	mp.events.add("loggedin", () => {
		peds = mp.av.storage.get("peds", true);
		traffic = mp.av.storage.get("traffic", true);
		warpblocked = mp.av.storage.get("warpblocked", false);
		chatpos = mp.av.storage.get("chatpos", "bottom");
		lang = mp.av.storage.get("language", "English");
		god = mp.players.local.getVariable("godmode");
		mp.av.menu.settings = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.settings = createMenu();
	});

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Settings"), "",
				new mp.ui.Point(50, 50));

		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("God mode"), god, ""));
		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("Show speedo"), speedo, ""));
		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("Show nametags"), nametags, ""));
		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("Pedestrians"), peds, ""));
		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("Traffic"), traffic, ""));
		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("Block warp to me"), warpblocked, ""));
		menu.AddItem(new mp.ui.UIMenuListItem(
			T("Chat position"), "",
			new mp.ui.ItemsCollection([T("left"), T("bottom"), T("right")])
		));
		menu.AddItem(new mp.ui.UIMenuListItem(
			T("Language"), "",
			new mp.ui.ItemsCollection(mp.av.lang.languages)
		));

		menu.CheckboxChange.on(item => {
			switch (item.Text) {
				case T("God mode"):
					const player = mp.players.local;
					// as the vehicle will be destroyed, we need to turn off
					// substitute as destroying it while sub is active will
					// cause the client to crash
					if (item.Checked && player.getVariable("wanted")) {
						item.Checked = false;
						break;
					}

					if (!item.Checked && mp.game.player.getInvincible()) {
						const vehicle = mp.av.fun.getCurrentVehicle();
						if (vehicle) mp.av.fun.toggleSubstitute(vehicle);
					}
					mp.game.player.setInvincible(item.Checked);
					break;
				case T("Show speedo"):
					if (item.Checked) mp.av.speedo.show();
					else mp.av.speedo.hide();
					break;
				case T("Show nametags"):
					mp.nametags.enabled = item.Checked;
					break;
				case T("Pedestrians"):
					if (item.Checked) mp.av.pedestrians.enablePeds();
					else mp.av.pedestrians.disablePeds();
					break;
				case T("Traffic"):
					if (item.Checked) mp.av.pedestrians.enableTraffic();
					else mp.av.pedestrians.disableTraffic();
					break;
				case T("Block warp to me"):
					mp.av.user.setWarpBlock(item.Checked);
					break
			}
		});

		menu.ItemSelect.on(item => {
			switch (item.Text) {
				case T("Chat position"):
					mp.av.chat.position(item.SelectedItem.DisplayText);
					break;
				case T("Language"):
					mp.av.lang.setLanguage(item.SelectedItem.DisplayText);
					setTimeout(() => mp.av.menu.showMenu("settings"), 500);
					break;
			}
		});

		menu.Close();
		return menu;
	}
});
