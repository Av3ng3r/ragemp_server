mp.events.add("client_loaded", () => {
	const SUBS = [
		"besra",
		"hydra",
		"futo",
		"akuma",
		"dinghy",
		"seabreeze"
	];

	mp.events.add("client_start", () => {
		mp.av.menu.mechanic = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.mechanic = createMenu();
	});

	const maxSlots = 30;

	const getStoredVehicleEntries = () => {
		const entries = [];
		for (let i = 0; i < maxSlots; i++) {
			const data = mp.av.user.getData(`vehicle${i}`, null);
			let name = "empty";
			if (data) {
				name = data.name || "mt";
			}
			entries.push((i+1) + ":" + name);
		}
		return entries;
	};

	const createMenu = () => {
		let drift = mp.av.storage.get("drifting", false);

		let entries = getStoredVehicleEntries();
		let savedVehicleMenuItem = new mp.ui.UIMenuListItem(
			T("Save current vehicle"),
			T("Store the vehicle in the selected space"),
			new mp.ui.ItemsCollection(entries)
		);
		let spawnVehicleMenuItem = new mp.ui.UIMenuListItem(
			T("Spawn saved vehicle"),
			T("Retrieve the vehicle from the selected space"),
			new mp.ui.ItemsCollection(entries)
		);

		let menu = new mp.ui.Menu(T("Mechanic Menu"), "",
			new mp.ui.Point(50, 50));

		menu.AddItem(spawnVehicleMenuItem);
		menu.AddItem(savedVehicleMenuItem);

		menu.AddItem(new mp.ui.UIMenuItem(T("Repair vehicle")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Toggle autopilot")));
		menu.AddItem(new mp.ui.UIMenuCheckboxItem(T("Drift"), drift, T("Drift tuning")));
		menu.AddItem(new mp.ui.UIMenuListItem(
			T("Substitute Vehicle"), "",
			new mp.ui.ItemsCollection(SUBS),
			SUBS.indexOf(mp.av.fun.getSubstituteVehicleModel())
		));

		menu.CheckboxChange.on(item => {
			switch (item.Text) {
				case T("Drift"):
					drift = item.Checked;
					if (drift) mp.av.drift.enableDriftMode();
					else mp.av.drift.disableDriftMode();
					break;
			}
		});

		menu.ItemSelect.on(item => {
			const vehicle = mp.players.local.vehicle;
			let i = -1;
			switch (item.Text) {
				case T("Save current vehicle"):
					i = parseInt(item.SelectedItem.DisplayText) - 1;
					mp.av.mechanic.saveVehicle(vehicle, i);
					setTimeout(() => {
						const entries = getStoredVehicleEntries();
						savedVehicleMenuItem.Collection = new mp.ui.ItemsCollection(entries).getListItems();
						spawnVehicleMenuItem.Collection = new mp.ui.ItemsCollection(entries).getListItems();
					}, 500);
					break;
				case T("Spawn saved vehicle"):
					i = parseInt(item.SelectedItem.DisplayText) - 1;
					mp.av.mechanic.restoreVehicle(i);
					break;
				case T("Repair vehicle"):
					if (vehicle) {
						mp.events.callRemote("repairVehicle", vehicle.remoteId);
						vehicle.setDirtLevel(0);
					}
					break;
				case T("Toggle autopilot"):
					mp.av.mechanic.toggleAutopilot();
					break;
				case T("Substitute Vehicle"):
					const model = item.SelectedItem.DisplayText;
					mp.av.fun.setSubstituteVehicleModel(model);
					if (vehicle && vehicle.getVariable("sub")) return;
					mp.av.fun.toggleSubstitute();
					break;
			}
		});

		menu._Open = menu.Open;
		menu.Open = function() {
			const entries = getStoredVehicleEntries();
			savedVehicleMenuItem.Collection = new mp.ui.ItemsCollection(entries).getListItems();
			spawnVehicleMenuItem.Collection = new mp.ui.ItemsCollection(entries).getListItems();
			this._Open();
		}.bind(menu);

		menu.onLanguageChange = createMenu;
		menu.Close();
		return menu;
	};
});
