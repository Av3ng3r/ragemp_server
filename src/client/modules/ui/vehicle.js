mp.events.add("client_loaded", () => {
	mp.events.add("client_start", () => {
		mp.av.menu.vehicle = createMenu();
	});

	mp.events.add("languageChanged", () => {
		mp.av.menu.hideAll();
		mp.av.menu.vehicle = createMenu();
	});

	const createMenu = () => {
		const menu = new mp.ui.Menu(T("Vehicles"), "",
			new mp.ui.Point(50, 50));

		populateMenu(menu);

		menu.ItemSelect.on(item => {
			switch (item.Text) {
				case T("Showcase..."):
					mp.av.menu.showMenu("showcase");
					break;
				case T("Mods..."):
					mp.av.menu.showMenu("mods");
					break;
				default:
					mp.events.call("spawnVehicle", item.SelectedItem.DisplayText);
					break;
			}
		});

		menu._Open = menu.Open;
		menu.Open = function() {
			populateMenu(this);
			this._Open();
		}.bind(menu);

		menu.Close();
		return menu;
	};

	const populateMenu = (menu) => {
		const types = mp.av.vehicles.getVehicleTypes();

		menu.Clear();
		menu.AddItem(new mp.ui.UIMenuItem(T("Showcase...")));
		menu.AddItem(new mp.ui.UIMenuItem(T("Mods...")));

		types.forEach((type) => {
			const vehicles = mp.av.vehicles.getVehiclesForType(type, false);
			if (vehicles && vehicles.length > 0) {
				menu.AddItem(new mp.ui.UIMenuListItem(
					type, T("Spawn vehicle"),
					new mp.ui.ItemsCollection(vehicles)
				));
			}
		});
	};
});
