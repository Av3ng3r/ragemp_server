mp.events.add("client_loaded", () => {
	let pBudget = 3;
	let tBudget = 3;
	let gotTick = false;

	mp.av.pedestrians = {};

	mp.events.add("loggedin", () => {
		const peds = mp.av.storage.get("peds", true);
		const traffic = mp.av.storage.get("traffic", true);
		setBudget(peds ? 3 : 0, traffic ? 3 : 0);
	});

	const setBudget = (p, t) => {
		if (null !== p) pBudget = p;
		if (null !== t) tBudget = t;
		if (gotTick) {
			mp.game.streaming.setPedPopulationBudget(pBudget);
			mp.game.streaming.setVehiclePopulationBudget(tBudget);
			mp.game.vehicle.setNumberOfParkedVehicles(500);
		}
	};

	mp.events.add("ack", () => {
		if (!gotTick) {
			gotTick = true;
			setBudget(pBudget, tBudget);
		}
	});

	mp.av.pedestrians.enablePeds = () => {
		setBudget(3, null);
		mp.av.storage.set("peds", true, true);
	};

	mp.av.pedestrians.disablePeds = () => {
		setBudget(0, null);
		mp.av.storage.set("peds", false, true);
	};

	mp.av.pedestrians.enableTraffic = () => {
		setBudget(null, 3);
		mp.av.storage.set("traffic", true, true);
	};

	mp.av.pedestrians.disableTraffic = () => {
		setBudget(null, 0);
		mp.av.storage.set("traffic", false, true);
	};
});
