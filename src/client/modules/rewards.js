mp.events.add("client_loaded", () => {
	mp.av.rewards = {};

	mp.av.rewards.previousrp = 0;
	mp.av.rewards.previouslevel = 0;
	mp.av.rewards.previousmaxLevel = 0;

	mp.av.rewards.rp = 0;
	mp.av.rewards.level = 0;
	mp.av.rewards.maxLevel = 0;

	mp.events.add("levelUp", (rp, level, maxLevel) => {
		mp.av.rewards.previousrp = mp.av.rewards.rp;
		mp.av.rewards.previouslevel = mp.av.rewards.level;
		mp.av.rewards.previousmaxLevel = mp.av.rewards.maxLevel;
		mp.av.rewards.rp = rp;
		mp.av.rewards.level = level;
		mp.av.rewards.maxLevel = maxLevel;

		if (mp.av.rewards.previouslevel > mp.av.rewards.level) {
			mp.av.rewards.previouslevel = 0;
		}

		mp.av.rewards.updateRankBar(true);
	});

	mp.events.add("playerLevelUp", (name, rp) => {
		if (name) mp.game.graphics.notify(`${name} reached level ${rp}`);
	});

	const hudId = 19;
	const rankBarColor = 116;

	// from: https://gist.github.com/Illusivee/7fc0f7bcc1bf4f80dceeb2eeba0e3ed1 (with changes)
	mp.av.rewards.updateRankBar = (animate) => {
		const a = animate ? mp.av.rewards.previouslevel : mp.av.rewards.level;
		if (!mp.game.graphics.hasHudScaleformLoaded(hudId)) {
			mp.game.graphics.requestHudScaleform(hudId);
			while (!mp.game.graphics.hasHudScaleformLoaded(hudId)) mp.game.wait(0);

			mp.game.graphics.pushScaleformMovieFunctionFromHudComponent(hudId, "SET_COLOUR");
			mp.game.graphics.pushScaleformMovieFunctionParameterInt(rankBarColor);
			mp.game.graphics.popScaleformMovieFunctionVoid();
		}

		mp.game.graphics.pushScaleformMovieFunctionFromHudComponent(hudId, "SET_RANK_SCORES");
		mp.game.graphics.pushScaleformMovieFunctionParameterInt(0);
		mp.game.graphics.pushScaleformMovieFunctionParameterInt(mp.av.rewards.maxLevel);
		mp.game.graphics.pushScaleformMovieFunctionParameterInt(a);
		mp.game.graphics.pushScaleformMovieFunctionParameterInt(mp.av.rewards.level);
		mp.game.graphics.pushScaleformMovieFunctionParameterInt(mp.av.rewards.rp);
		mp.game.graphics.popScaleformMovieFunctionVoid();
	};

	mp.events.add("loggedin", () => {
		mp.av.rewards.rp = mp.av.storage.get("level", 0);
		mp.av.rewards.level = mp.av.storage.get("rp", 0);
		mp.av.rewards.maxLevel = mp.av.storage.get("maxLevel", 0);
	});

	mp.keys.bind(90, false, () => {
		mp.av.rewards.updateRankBar(false);
	});
});
