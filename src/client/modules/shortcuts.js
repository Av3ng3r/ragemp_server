mp.events.add("client_loaded", () => {
	mp.av.shortcuts = {};

	mp.av.shortcuts.commands = [
		"Nothing",
		"Clear Chat",
		"Repair Vehicle",
		"Save Current Vehicle",
		"Respawn Last Vehicle",
		"Toggle Drift",
		"Toggle Autopilot"
	];

	mp.av.shortcuts.keys = {
		114: "F3",
		115: "F4",
		116: "F5",
		117: "F6",
		118: "F7",
		119: "F8",
		120: "F9",
		121: "F10",
		122: "F11",
		123: "F12"
	};

	let shortcuts = { 
		114: "Clear Chat",
		115: "Nothing",
		116: "Toggle Drift",
		117: "Repair Vehicle",
		118: "Respawn Last Vehicle",
		119: "Save Current Vehicle",
		120: "Toggle Autopilot",
		121: "Nothing",
		122: "Nothing",
		123: "Nothing"
	};

	mp.events.add("client_start", () => {
		shortcuts = mp.av.storage.get("shortcuts", shortcuts);

		mp.av.shortcuts.execute = (key) => {
			switch (mp.av.shortcuts.get(key)) {
				case "Nothing":
					break;
				case "Clear Chat":
					mp.av.chat.clear();
					break;
				case "Repair Vehicle":
					mp.av.mechanic.repairVehicle(mp.players.local.vehicle);
					break;
				case "Save Current Vehicle":
					mp.av.mechanic.saveVehicle(mp.players.local.vehicle, -1);
					break;
				case "Respawn Last Vehicle":
					mp.av.mechanic.restoreVehicle(-1);
					break;
				case "Toggle Drift":
					mp.av.drift.toggle();
					break;
				case "Toggle Autopilot":
					mp.av.mechanic.toggleAutopilot();
					break;
			}
		};

	});

	mp.av.shortcuts.set = (key, command) => {
		// implement me
	}

	mp.av.shortcuts.get = (key) => {
		return shortcuts[key] || "Nothing";
	};

	mp.keys.bind(113, false, () => mp.av.shortcuts.execute(113));
	mp.keys.bind(114, false, () => mp.av.shortcuts.execute(114));
	mp.keys.bind(115, false, () => mp.av.shortcuts.execute(115));
	mp.keys.bind(116, false, () => mp.av.shortcuts.execute(116));
	mp.keys.bind(117, false, () => mp.av.shortcuts.execute(117));
	mp.keys.bind(118, false, () => mp.av.shortcuts.execute(118));
	mp.keys.bind(119, false, () => mp.av.shortcuts.execute(119));
	mp.keys.bind(120, false, () => mp.av.shortcuts.execute(120));
	mp.keys.bind(121, false, () => mp.av.shortcuts.execute(121));
	mp.keys.bind(122, false, () => mp.av.shortcuts.execute(122));
});
