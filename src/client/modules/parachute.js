mp.events.add("client_loaded", () => {
	const chutes = [];

	const createEntity = (entity, player) => {
		let model = isNaN(entity)? mp.game.joaat(entity) : parseInt(entity)
		let obj = mp.objects.new(model, player.position, {
			rotation: player.getRotation(),
			dimension: player.dimension
		})
		obj.name = entity
		return obj
	};

	/** 
	 * Attach parachute object to player 
	 * and delete it when player is very close to the ground  
	 */
	const createChute = (player) => {
		if (player && undefined === chutes[player.remoteId]) {
			let parachuteObject = createEntity('p_parachute1_mp_s', player);
			parachuteObject.attachTo(
				player.handle, // entity
				0x5c01, // bone index
				0, // xPosOffset
				0, // yPosOffset
				3.7, // zPosOffset
				0, // xRot
				0, // yRot
				0, // zRot
				true, // p9
				false, // useSoftPinning
				false, // collision
				false, // isPed
				2, // vertexIndex
				true // fixedRot
			);
			parachuteObject.player = player;
			chutes[player.remoteId] = parachuteObject;
		}
	};

	const deleteChute = (remoteId) => {
		const chute = chutes[remoteId];
		if (chute) {
			delete chutes[remoteId];
			chute.destroy();
		}
	};

	/**
	 *  Send parachute state to server
	 */
	setInterval(() => {
		const currentState = mp.players.local.getVariable("parachuteState");
		const parachuteState = mp.players.local.getParachuteState();
		if (parachuteState !== currentState) {
			if (parachuteState >= 0 && parachuteState <= 2) {
				mp.events.callRemote('onPlayerParachute', parachuteState);
			} else if (currentState != -1) {
				mp.events.callRemote('onPlayerParachute', -1);
			}
		}
	}, 100);

	mp.events.add("render", () => {
		chutes.forEach(chute => {
			chute.setHeading(chute.player.getHeading());
		});
	});


	/**
	 * Fix parachute falling animation
	 */
	mp.events.add('fixFallingFor', (remoteId) => {
		mp.players.forEachInStreamRange((player) => {
			try {
				if (player.remoteId === remoteId && player !== mp.players.local) {
					player.taskParachute(true);
				}
			} catch (ex) {}
		});
	});

	/**
	 * Fix parachute object
	 */
	mp.events.add('fixParachuteFor', (remoteId) => {
		mp.players.forEachInStreamRange((player) => {
			try {
			if (player.remoteId === remoteId && player !== mp.players.local) {
				createChute(player);
			}
			} catch (ex) {}
		});
	});

	mp.events.add('removeParachuteFor', (remoteId) => {
		const player = mp.players.atRemoteId(remoteId);
		if (player) player.clearTasksImmediately();
		deleteChute(remoteId);
	});

	/**
	 * Handle if a player has streamed in and has a chute
	 */
	mp.events.add("entityStreamIn", (entity) => {
		if (entity.type == "player") {
			let state = entity.getVariable("parachuteState");
			if (+state >= 0) entity.taskParachute(false);
			if (+state >= 1) setTimeout(() => createChute(entity), 500);
		}
	});

	mp.events.add("entityStreamOut", (entity) => {
		if (entity.type === "player") {
			deleteChute(entity.remoteId);
		}
	});

	// an always alive interval to refresh the parachutes each half second
	// to avoid them disappearing
	setInterval(() => {
		mp.players.forEachInStreamRange(player => {
			if (player && chutes[player.remoteId]) try {
				deleteChute(player.remoteId);
				createChute(player);
			} catch (ex) {}
		});
	}, 500);
});