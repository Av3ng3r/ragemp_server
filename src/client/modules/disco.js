mp.events.add("client_loaded", () => {
	mp.av.disco = {};

	let peds = [];
	let inDisco = false;
	let dancing = false;

	const pedData = [
		{
			model: "cs_tracydisanto",
			pos: { x: -1387.922241, y: -617.790641, z: 30.81922, a: 200 },
			scenario: "WORLD_HUMAN_PARTYING"
		},
		{
			model: "a_f_m_beach_01",
			pos: { x: -1387.4904785, y: -618.98777, z: 30.81922, a: 30 },
			scenario: "WORLD_HUMAN_PARTYING"
		},
		{
			model: "a_m_y_stbla_02",
			pos: { x: -1381.980835, y: -614.89148, z: 31.497911, a: 100 },
			scenario: "WORLD_HUMAN_CHEERING"
		},
		{
			model: "s_m_y_barman_01",
			pos: { x: -1391.2301, y: -606.2323, z: 30.3196, a: 140 },
			scenario: "WORLD_HUMAN_COP_IDLES"
		},
		{
			model: "mp_f_boatstaff_01",
			pos: { x: -1392.520019, y: -603.760925, z: 30.31955, a: 100 },
			scenario: "WORLD_HUMAN_COP_IDLES"
		}
	];

	const createPeds = () => {
		for (let i = 0; i < pedData.length; i++) {
			const pos = new mp.Vector3(pedData[i].pos.x, pedData[i].pos.y, pedData[i].pos.z);
			const ped = mp.peds.new(mp.game.joaat(pedData[i].model),pos,pedData[i].pos.a,function(p) {
                var scenario = p.scenario || pedData[i].scenario;
                p.taskStartScenarioInPlace(scenario, 0, true);
            }, mp.players.local.dimension);

			if (ped) {
				ped.scenario = pedData[i].scenario;
				peds.push(ped);
			}
		}
	};

	const destroyPeds = () => {
		for (let i = 0; i < peds.length; i++) {
			peds[i].destroy();
		}
		peds = [];
	};

	mp.events.add("enter_disco", () => {
		inDisco = true;
		createPeds();
	});

	mp.events.add("exit_disco", () => {
		inDisco = false;
		destroyPeds();
	});

	mp.keys.bind(220, false, () => {
		if (inDisco) try {
			dancing = !dancing;
			if (dancing) {
				// 'mp_arrest_paired', 'cop_p2_back_right', 8.0, 3.0, -1, 10, 0.0, false, false, false);
				const crazies = [ "crazy_dance_1", "crazy_dance_2", "crazy_dance_3", "crazy_dance_base" ];
				const crazy = crazies[Math.floor(Math.random() * 4)];
				// mp.gui.chat.push("going to play " + crazy);
				mp.players.local.taskPlayAnim(
					"misschinese1crazydance",
					crazy,
					8.0,
					1.0,
					-1,
					33,
					1.0,
					false,
					false,
					false
				);
			} else {
				mp.players.local.clearTasks();
			}
		} catch (ex) {}
	});
});
