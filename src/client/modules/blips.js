mp.events.add("client_loaded", () => {
	let blips = {};
	let heliRotation = 0;

	const SPRITE_PLAYER = 1;
	const SPRITE_HELI = 153;
	const SPRITE_EMERGENCY = 558;
	const SPRITE_MOTORCYCLE = 226;
	const SPRITE_PLANE = 307;
	const SPRITE_BOAT = 427;
	const SPRITE_BICYCLE = 559;
	const SPRITE_VEHICLE = 562

	const EMERGENCY_COLORS = [ 1, 29 ];
	const PLAYER_COLOR = 0;
	const GUEST_COLOR = 22;
	const WANTED_COLOR = 1;
	const GOD_COLOR = 2;

	let currentEC = 0;
	setInterval(() => currentEC = 1 - currentEC, 500);

	function getPosition(player) {
		let pos = player.getVariable("position");
		if (!pos) return { x: 0, y: 0, z: 0, h: 0, c: -1 };
		pos = JSON.parse(pos);

		const { x, y, z } = player.position;
		const h = player.getHeading();
		let c = player.vehicle ? player.vehicle.getVariable("class") : 0;
		return { x: x || pos.x, y: y || pos.y, z: z || pos.z, h: h || pos.h, c: c || pos.c };
	}

	function getBlipType(cls)
	{
		const map = {
			"8": SPRITE_MOTORCYCLE,
			"13": SPRITE_BICYCLE,
			"14": SPRITE_BOAT,
			"15": SPRITE_HELI,
			"16": SPRITE_PLANE,
			"18": SPRITE_EMERGENCY,
			"-1": SPRITE_PLAYER
		};
		return map[`${cls}`] || SPRITE_VEHICLE;
	}

	function getColor(player, sprite)
	{
		let color = PLAYER_COLOR;
		if (player.getVariable("wanted")) {
			color = WANTED_COLOR;
		} else if (player.getVariable("godmode")) {
			color = GOD_COLOR;
		} else if (sprite === SPRITE_EMERGENCY) {
			color = EMERGENCY_COLORS[currentEC];
		} else if (player.getVariable("guest")) {
			color = GUEST_COLOR;
		}

		return color;
	}

	function adjustRotationForSprite(sprite, h)
	{
		switch (sprite) {
			case SPRITE_HELI:
				h = heliRotation;
				break;
			case SPRITE_MOTORCYCLE:
			case SPRITE_BICYCLE:
				h = 0;
				break;
		}
		return Math.round(h);
	}

	function createBlip(player)
	{
		if (player.getVariable("loggedIn")) {
			let { x, y, z, h, c } = getPosition(player);
			const sprite = getBlipType(c);
			const color = getColor(player, sprite);
			const pos = new mp.Vector3(x, y, z);
			blips[player.id] = mp.blips.new(sprite, pos, {
				name: player.name,
				dimension: player.dimension,
				color,
				shortRange: true
			});
			blips[player.id].setRotation(adjustRotationForSprite(sprite, h));
		}
	}

	function destroyBlip(player)
	{
		if (blips[player.id]) {
			blips[player.id].destroy();
			delete blips[player.id];
		}
	}

	mp.events.add("playerQuit", (player, exitType, reason) => {
		if (blips[player.id]) destroyBlip(player);
	});

	setInterval(() => {
		heliRotation += Math.round(360 / mp.av.fps);
		if (heliRotation > 360) heliRotation -= 360;

		mp.players.forEach(function(player) {
			if (player !== mp.players.local) {
				if (player.getVariable("following")) {
					if (blips[player.id]) destroyBlip(player);
					return;
				}

				if (player.getVariable("loggedIn")) {
					if (!blips[player.id]) createBlip(player);
					if (!blips[player.id]) return;

					let {x, y, z, h, c} = getPosition(player);
					const sprite = getBlipType(c);

					// cannot change sprite, need to create a new blip
					if (blips[player.id].getSprite() !== sprite) {
						destroyBlip(player);
						createBlip(player);
						return;
					}

					if (!blips[player.id]) return;

					blips[player.id].setAsShortRange(false);
					blips[player.id].setCoords(new mp.Vector3(x, y, z));
					blips[player.id].setColour(getColor(player, sprite));
					blips[player.id].setRotation(Math.round(h));
					blips[player.id].setRotation(adjustRotationForSprite(sprite, h));
				}
			}
		});
	}, 100);
});
