mp.events.add("client_loaded", () => {
	const dist = (a, b, c, d, e, f) => {
		return Math.sqrt((d - a) ** 2 + (e - b) ** 2 + (f - c) ** 2);
	};

	const getClosestVehicle = (x, y, z, r) => {
		let found = null;
		let distance = r;
		mp.vehicles.forEachInStreamRange((vehicle) => {
			if (vehicle.isAnySeatEmpty() && vehicle.getSpeed() < 5) {
				let pos = vehicle.position;
				let d = dist(x, y, z, pos.x, pos.y, pos.z);
				if (Math.abs(d) < distance) {
					found = vehicle;
					distance = Math.abs(d);
				}
			}
		});
		return found;
	};

	mp.events.add("render", () => {
		const controls = mp.game.controls;
		if (!mp.players.local.vehicle) {

			controls.enableControlAction(0, 23, true);
			controls.disableControlAction(0, 58, true);

			if(controls.isDisabledControlJustPressed(0, 58))
			{
				let pos = mp.players.local.position;
				let vehicle = getClosestVehicle(pos.x, pos.y, pos.z, 5);

				if(vehicle) {
					mp.players.local.taskEnterVehicle(vehicle.handle, 5000, 0, 2, 1, 0);
				}
			}
		} else {
			controls.enableControlAction(0, 23, true);
			controls.enableControlAction(0, 58, true);
		}
	});
});
