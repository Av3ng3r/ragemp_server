mp.events.add("client_loaded", () => {
	mp.av.user = {};
	let userData = {};

	mp.av.user.init = (data) => {
		if (data) try {
			userData = JSON.parse(data);
		} catch (ex) {}
		mp.av.doors.unlockAllDoors();
	};

	mp.av.user.getData = (key, dfl) => {
		return userData[key] || dfl;
	};

	mp.av.user.setData = (key, value) => {
		if (userData[key] !== value) {
			userData[key] = value;
			mp.events.callRemote("saveData", key, JSON.stringify(value));
		}
	};

	mp.av.user.setCharacter = (character) => {
		mp.av.user.setData("character", character);
		mp.events.callRemote("character", character);
	};

	mp.av.user.setWarpBlock = (block) => {
		mp.av.user.setData("warpblocked", block);
		mp.events.callRemote("setWarpBlock", block);
	};

	mp.events.add("loggedin", () => {
		if (mp.players.local.getVariable("guest") !== true) {
			savPositionInterval = setInterval(savePosition, 10000);
		}

		let position = mp.av.user.getData("playerPosition", false);
		if (!position) position = new mp.Vector3(-1041.21069, -2744.70068, 21.35940170);
		if (position) mp.players.local.position = position;
	});

	let savePositionInterval = 0;
	const savePosition = () => {
		if (!mp.players.local.vehicle) {
			mp.av.user.setData("playerPosition", mp.players.local.position);
		}
	};
	mp.events.add("playerEnterVehicle", () => {
		if (savePositionInterval) clearInterval(savePosition);
		savePositionInterval = 0;
	});

	mp.events.add("playerExitVehile", () => {
		if (savePositionInterval) clearInterval(savePosition);
		savePositionInterval = setInterval(savePosition, 10000);
	});

	mp.events.add("notify", (msg) => {
		mp.game.graphics.notify(msg);
	});

	mp.events.add("tick_timeout", () => {
		try {
			mp.players.local.name = mp.players.local.getVariable("name");
		} catch (ex) {}
	});
});
