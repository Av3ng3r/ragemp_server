mp.events.add("client_loaded", () => {
	mp.av.time = {};

	mp.av.time.no = false;
	let time = {
		hour: 0,
		min: 0,
		sec: 0,
		day: 0,
		mon: 0,
		year: 0
	};

	mp.av.getTime = () => {
		return time;
	};

	mp.events.add("client_start", () => {
		mp.events.add("render", () => {
			if (mp.av.time.no) return;

			const t = Date.now();
			const d = new Date(t);
			const h1 = d.getUTCHours();
			const m1 = d.getUTCMinutes();
			const s1 = d.getUTCSeconds();
			const ms = d.getUTCMilliseconds();

			const hour = (Math.floor(m1/2) + h1 * 6) %24;
			const min =  (Math.floor(s1/2) + m1 * 30) % 60;
			const sec =  (Math.floor(ms*0.03) + s1 * 30) % 60;

			if (mp.players.local.getVariable("banished")) {
				mp.game.time.setClockTime(0, 0, 0);
			} else {
				mp.game.time.setClockTime(hour, min, sec);
			}

			mp.game.time.setClockDate(d.getUTCDate(), d.getUTCMonth(), d.getUTCFullYear());

			time = {
				hour, min, sec,
				day: d.getUTCDate(),
				mon: d.getUTCMonth(),
				year: d.getUTCFullYear()
			}
		});
	});

	mp.events.add('playerCommand', (command) => {
		const args = command.replace(":", " ").split(/[ ]+/);
		if (args[0] === "time") {
			if (["on", "off"].indexOf(args[1]) !== -1) {
				mp.gui.chat.push("turning time " + args[1]);
				mp.av.time.no = args[1] === "off";
			} else if (args.length > 2) {
				mp.av.time.no = true;
				const t = Date.now();
				const d = new Date(t);
				const hour = parseInt(args[1]);
				const min = parseInt(args[2]);
				const sec = 0;
				mp.game.time.setClockTime(hour, min, sec);
				time = {
					hour, min, sec,
					day: d.getUTCDate(),
					mon: d.getUTCMonth(),
					year: d.getUTCFullYear()
				}
			}
		}
	});

});
