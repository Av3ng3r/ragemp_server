mp.events.add("client_loaded", () => {
	mp.av.speedo = {};
	mp.av.speedo.visible = false;

	let speedo = null;

	let visible = false;
	let enabled = true;

	mp.av.speedo.show = () => {
		enabled = true;
	};

	mp.av.speedo.hide = () => {
		enabled = false;
		if (visible) hide();
	};

	const show = () => {
		mp.av.speedo.visible = visible = true;
		speedo.execute("show();");
	};

	const hide = () => {
		mp.av.speedo.visible = visible = false;
		speedo.execute("hide();");
	};

	mp.events.add("client_start", () => {
		speedo = mp.browsers.new("package://cef/speedo.html");
		const player = mp.players.local;
		/*
		const data = mp.av.cef.get("speedo.html");
		speedo.execute(`document.write(${data});`);
		*/

		let currentMaxSpeed = 100;
		let currentMaxRPM = 100;
		let model = "";

		function updateSpeedo(val, ms) {
			speedo.execute(`updateSpeedo(${val}, ${ms});`);
		}

		function updateTacho(val, ms) {
			speedo.execute(`updateTacho(${val}, ${ms});`);
		}

		setInterval(function() {
			if (!enabled) return;

			try {
				if (player.vehicle && player.vehicle.getPedInSeat(-1) === player.handle) {
					if (player.vehicle.model !== model) {
						model = player.vehicle.model;
						currentMaxSpeed = 10;
						currentMaxRPM = 100;
					}

					if (!visible) show();

					const vel = player.vehicle.getSpeed() * 3.6;
					const maxSpeed = mp.game.vehicle.getVehicleModelMaxSpeed(player.vehicle.model) * 3.6;
					let rpm = player.vehicle.rpm * 1000;
					if (rpm > 1000) rpm = 1000;

					if (maxSpeed > currentMaxSpeed) {
						currentMaxSpeed = Math.ceil(maxSpeed / 100.0) * 100;
						speedo.execute(`updateSpeedoOptions({ max: ${currentMaxSpeed} });`);
					}

					if (1000 > currentMaxRPM) {
						currentMaxRPM = 1000;
						speedo.execute(`updateTachoOptions({ max: 1000 });`);
					}

					updateSpeedo(vel, 250);
					updateTacho(rpm, 250);
				} else {
					if (visible) hide();
				}
			} catch (ex) {}
		}, 250);

		mp.events.add("playerCommand", (command) => {
			const args = command.split(/[ ]+/);
			if (args[0] === "enableSpeedo") mp.av.speedo.show();
			else if (args[0] === "disableSpeedo") mp.av.speedo.hide();
			else return;
		});
	});
});
