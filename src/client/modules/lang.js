mp.events.add("client_loaded", () => {
	mp.av.lang = {};

	global.T = (key) => {
		return languageTable[key] || key;
	};

	let loggedin = false;
	let languageTable = {};

	mp.av.lang.current = "";
	mp.av.lang.languages = [
		"English",
		"русский"
	];

	mp.av.lang.setLanguage = (lang) => {
		try {
			if (lang === "русский") lang = "Russian";
			if (loggedin) mp.av.storage.set("language", lang, true);
			const l = require(`./language/${lang}.js`);
			languageTable = l;
			mp.av.lang.current = lang;
			mp.events.call("languageChanged", lang);
		} catch (ex) {}
	};

	mp.events.add("setlang", mp.av.lang.setLanguage);
	mp.events.add("loggedin", () => {
		const lang = mp.av.storage.get("language", "English");

		loggedin = true;

		// if we have set the language via the login, this will cause
		// it to be stored.
		// otherwise, we're going to be just setting the default.
		if (mp.av.lang.current) {
			mp.av.lang.setLanguage(mp.av.lang.current);
		} else if (lang !== mp.av.lang.current) try {
			const l = require(`./language/${lang}.js`);
			languageTable = l;
			mp.events.call("languageChanged", lang);
		} catch (ex) {}
	});
});
