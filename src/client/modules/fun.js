mp.events.add("client_loaded", () => {
	mp.av.fun = {};

	mp.av.fun.launch = (entity) => {
		if (entity) try {
			entity.applyForceToCenterOfMass(1, 0, 0, 10, false, false, true, false);
		} catch (ex) {}
	};

	mp.keys.bind(66, false, () => {
		const player = mp.players.local;
		const vehicle = mp.players.local.vehicle;
		if (player.getHeightAboveGround() < 3) try {
			if (vehicle && vehicle.getPedInSeat(-1) === player.handle) {
				mp.av.fun.launch(vehicle);
			}
		} catch (ex) {}
	});

	let currentVehicle = null;
	let substitute = null;
	let realModel = null;
	let fakeModel = "besra";
	let autoSub = 0;
	let CTD = null;
	let siren = false;

	mp.av.fun.getCurrentVehicle = () => currentVehicle;
	mp.av.fun.isSubstituteActive = () => !!substitute;
	mp.av.fun.getSubstituteVehicleModel = () => fakeModel;
	mp.av.fun.setSubstituteVehicleModel = (model) => {
		fakeModel = model;
	};

	const clonePlayer = (player) => {
		const newPed = mp.peds.newLegacy || mp.peds.new;
		const { x, y, z } = player.position;
		const clone = newPed(
			player.model,
			new mp.Vector3(x, y, -100),
			player.heading,
			(ped) => {
				ped.setAlpha(0);
				ped.freezePosition(false);
				ped.setProofs(false, false, false, false, false, false, false, false);
			},
			player.dimension
		);
		clone.setAlpha(0);
		clone.setCollision(false, false);
		return clone;
	};

	const attachSubstitute = (sub, clone, vehicle) => {
		try {
			sub.attachTo(vehicle.handle, -1, 0, 0, 0, 0, 0, 0,
				true, false, false, false, 2, true);
			mp.av.mechanic.applyMods(sub, vehicle.getVariable("mods"));
			sub.setEngineOn(true, true, false);
			sub.setLights(2);
			sub.resetAlpha();
			clone.resetAlpha();
		} catch (ex) {
		}
	};

	const cloneVehicle = (vehicle, player) => {
		if (vehicle && player) try {
			if (mp.vehicles.exists(vehicle.id) && mp.players.exists(player.id)) {
				const clone = mp.vehicles.new(realModel, new mp.Vector3(0, 0, -100), {
					dimension: player.dimension,
					heading: player.heading,
					alpha: 0
				});
				clone.setHeading(vehicle.heading);
				clone.setCollision(false, false);
				if (siren) clone.setSiren(true);
				clone.setNumberPlateText(vehicle.getNumberPlateText());
				return clone;
			}
		} catch (ex) {}
		return null;
	};

	const toggleSubstitute = mp.av.fun.toggleSubstitute = (vehicle, fake) => {
		const player = mp.players.local;
		if (vehicle && player.vehicle && vehicle.getPedInSeat(-1) !== player.handle) return;
		vehicle = vehicle || player.vehicle;
		fake = fake || fakeModel;
		if (vehicle) {
			const index = mp.game.invoke(
				mp.game.legacy ? "0x885DE9EE2AE89A2A" : "0xE8AF77C4C06ADC93"
			);
			if (!substitute) try {
				siren = vehicle.isSirenOn();

				for (let i = 0; i < 4; i++) try {
					const h = vehicle.getPedInSeat(i);
					if (h) mp.players.atHandle(h).setAlpha(0);
				} catch (ex) {}

				currentVehicle = vehicle;
				realModel = vehicle.model;
				substitute = cloneVehicle(vehicle, player);
				if (substitute) {
					CTD = clonePlayer(player);
					if (CTD) {
						vehicle.model = mp.game.joaat(fake);
						vehicle.setAlpha(0);
						player.setAlpha(0);
						CTD.setIntoVehicle(substitute.handle, -1);
						setTimeout(() => attachSubstitute(substitute, CTD, vehicle), 1);
					}
				}
			} catch (ex) {
			} else try {
				autoSub = 0;
				if (vehicle) {
					vehicle.model = realModel;
					mp.av.mechanic.applyMods(vehicle);
					if (siren) vehicle.setSiren(true);
					vehicle.setEngineOn(true, true, true);
					vehicle.resetAlpha();

					for (let i = 0; i < 4; i++) try {
						const h = vehicle.getPedInSeat(i);
						if (h) mp.players.atHandle(h).resetAlpha();
					} catch (ex) {}
				}
				player.resetAlpha();

				try { substitute.destroy(); CTD.destroy(); } catch (ex) {}

				currentVehicle = null;
				substitute = null;
				CTD = null;
			} catch (ex) {
			}
			mp.game.invoke(mp.game.legacy ? "0x15DC27C443A94882" : "0xF7F26C6E9CC9EBB8", true);
			mp.game.invoke(mp.game.legacy ? "0xA619B168B8A8570F" : "0x6280DDB9E60D64A", index);
		}
	};

	mp.events.add("entityStreamIn", (entity) => entity.resetAlpha());
	mp.keys.bind(123, false, () => {
		const player = mp.players.local;
		const vehicle = mp.players.local.vehicle;
		if (vehicle.getVariable("sub")) return;
		toggleSubstitute();
	});

	mp.events.add("playerDeath", () => {
		if (currentVehicle) try {
			autoSub = 0;
			toggleSubstitute(currentVehicle);
		} catch (ex) {
		}
	});

	mp.events.add("render", () => {
		const player = mp.players.local;
		const vehicle = player.vehicle;
		if (player.isDead() || player.isDeadOrDying()) return;

		if (vehicle) {
			if (player.vehicle && vehicle.getPedInSeat(-1) !== player.handle) return;
			if (substitute) {
				vehicle.setDirtLevel(0);
			} else {
				if (
					mp.game.vehicle.isThisModelABicycle(vehicle.model) ||
					mp.game.vehicle.isThisModelABike(vehicle.model) ||
					mp.game.vehicle.isThisModelAQuadbike(vehicle.model) ||
					(
						!mp.game.vehicle.isThisModelABoat(vehicle.model) &&
						!mp.game.vehicle.isThisModelAHeli(vehicle.model) &&
						!mp.game.vehicle.isThisModelAPlane(vehicle.model)
					) ||
					vehicle.getVariable("sub")
				) {
					if (vehicle.isInWater()) {
						if (vehicle.getSubmergedLevel() > 0.3 || vehicle.getVariable("sub")) {
							autoSub = 100;
							toggleSubstitute(null, "dinghy");
							return;
						}
					}
				}
			}

			if (autoSub > 0) {
				if (autoSub === 1 && !vehicle.isInWater() && vehicle.getHeightAboveGround() < 1) {
					autoSub = 0;
					toggleSubstitute();
					const sub = vehicle.getVariable("sub");
					if (sub) toggleSubstitute(null, sub);
					return;
				}
				if (autoSub > 1) autoSub--;
			}
		}
		if (!mp.players.local.vehicle && currentVehicle) try {
			toggleSubstitute(currentVehicle);
		} catch (ex) {
		}
	});

	setInterval(() => {
		if (CTD && substitute && currentVehicle) {
			const player = mp.players.local;
			const newSub = cloneVehicle(currentVehicle, player);
			const newCTD = clonePlayer(player);

			if (newSub && newCTD) {
				newCTD.setIntoVehicle(newSub.handle, -1);

				setTimeout(() => {
					try {
						attachSubstitute(newSub, newCTD, currentVehicle);
						CTD.destroy(); substitute.destroy();
						substitute = newSub; CTD = newCTD;
					} catch (ex) {}
				}, 1);
			}
		}
	}, 5000);
});
