mp.events.add("client_loaded", () => {
	mp.av.timing = {};
	mp.av.timing.delay = 0;
	mp.av.timing.serverTime = 0;

	let lastTickTime = 0;
	let timestamp = 0;

	mp.events.add("ack", (data, serverTime) => {
		lastTickTime = Date.now();
		mp.game.gameplay.setRandomSeed(lastTickTime);
		if (data !== timestamp) return;
		const age = (Date.now() - timestamp) / 2.0;
		mp.av.timing.delay = age;
		mp.av.timing.serverTime = parseInt(serverTime);
	});

	mp.events.add("client_start", () => {
		setInterval(() => {
			timestamp = Date.now().toString();
			mp.events.callRemote("syn", timestamp);
			if (lastTickTime) {
				const delta = Date.now() - lastTickTime;
				if (delta > 3000) {
					mp.events.call("tick_timeout");
					lastTickTime = 0;
				}
			}
		}, 1000);
	});
});
