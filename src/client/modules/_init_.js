(function() {
	mp.gui.execute(`chatAPI.clear();`);

	mp.keys.bind(192, false, function() {
		const name = "screenshot_" + Date.now() + ".png";
		mp.gui.takeScreenshot(name, 1, 100, 0);
	});

	mp.gui.chat.push("!{red}Welcome to Avenger's beta server");
	mp.gui.chat.push("!{green}Discord: https://discord.gg/FQ3GK8u");
	mp.gui.chat.push("Please report any errors to the discord server.");

	const setStartCam = () => {
		mp.players.local.position = new mp.Vector3(
			1603.0635986328125,
			204.33453369140625,
			241.062255859375
		);

		mp.players.local.setHeading(121);
		mp.gui.chat.activate(false);

		let cam = mp.cameras.new("default",
			{
				x: 1603.0635986,
				y: 204.33453369,
				z: 260.062255859
			},
			{
				x: 0,
				y: 0,
				z: 121.25821685791016
			},
			90
		);
		cam.setActive(true);
		mp.game.cam.renderScriptCams(true, false, 0, true, false);

		mp.events.add("loggedin", () => {
			if (cam) {
				mp.game.cam.destroyAllCams(false);
				mp.game.cam.renderScriptCams(false, false, 0, true, false);
				cam = false;
			}
			mp.av.chat.clear();
			mp.gui.chat.push(T("!{red}Welcome to Avenger's beta server"));
			mp.gui.chat.push(T("!{green}Discord: https://discord.gg/FQ3GK8u"));
			mp.gui.chat.push(T("!{yellow}F2 - show the menu"));
			mp.gui.chat.push(T("F3 - clear chat | F5 - Toggle Drift | F6 - Repair Vehicle | F7 - Spawn Vehicle | F8 - Save Vehicle | F9 - Toggle Autopilot | F12 - flying vehicles"));
		});
	};

	setStartCam();

	mp.events.call("client_loaded");
	mp.events.call("client_start");
})();
