mp.events.add("client_loaded", () => {
	mp.av.weather = {};
	mp.av.weather.currentWeather = "EXTRASUNNY";
	mp.events.add("setWeather", (weather, duration) => {
		if (mp.players.local.getVariable("banished")) {
			mp.game.gameplay.setWeatherTypeOverTime("HALLOWEEN", duration);
		} else {
			mp.game.gameplay.setWeatherTypeOverTime(weather, duration);
		}
		mp.av.weather.currentWeather = weather;
	});
});
