mp.events.add("client_loaded", () => {
	mp.av.admin = {};

	mp.events.add("client_start", () => {
	});

	mp.events.add("playerBanished", (forever, reason) => {
		mp.av.menu.hideAll();

		mp.players.local.savedPos = mp.players.local.position;
		mp.game.gameplay.setWeatherTypeOverTime("HALLOWEEN", 15);
		if (reason) mp.game.graphics.notify("You've been banished [" + reason + "]");
		else if (forever) mp.game.graphics.notify("Youve been banished");
		else mp.game.graphics.notify("You've been banished for 1 minute");
	});

	mp.events.add("playerUnbanished", () => {
		mp.game.gameplay.setWeatherTypeOverTime(mp.av.weather.currentWeather, 15);
		mp.game.graphics.notify("You are no longer banished");
		if (mp.players.local.savedPos) {
			mp.players.local.position = mp.players.local.savedPos;
			delete mp.players.local.savedPos;
		}
	});

	mp.events.add("status", (data) => {
		const level = parseInt(data);
		mp.players.local.setHealth(level);
	});

	mp.events.add("playerCommand", (command) => {
		const args = command.split(/[ ]+/);
		if (mp.players.local.getVariable("admin")) {
			const command = args.shift();
			const param = args.shift();
			const otherParams = args.join(" ");
			mp.events.callRemote(`am_${command}`, param, otherParams);
		}
	});

	let followCam = null;
	mp.events.add("render", () => {
		const player = mp.players.local;
		const following = player.getVariable("following") || false;
		if (following) {
			const id = player.getVariable("target");
			const target = mp.players.atRemoteId(id);
			if (target) {
				let { x, y, z } = target.position;
				player.setNoCollision(player.handle, false);
				player.freezePosition(true);
				player.setHeading(target.getHeading());

				if (!followCam) {
					mp.game.cam.destroyAllCams(false);
					followCam = mp.cameras.new("default",
						{ x, y, z: z + 4},
						{ x: 0, y: 0, z: target.heading },
						90
					);
					followCam.setActive(true);
					mp.game.cam.renderScriptCams(true, false, 0, true, false);
				}

				let distance = 1.5;
				if (target.vehicle) distance = 5;
				const camera = mp.cameras.new("gameplay");
				const rot = camera.getRot(2);
				const rz = rot.z >= 0 ? rot.z : 360 + rot.z;
				const dx = Math.sin(rz / 180 * Math.PI) * distance;
				const dy = Math.cos(rz / 180 * Math.PI) * distance;

				x = x + dx;
				y = y - dy;
				z += (distance / 2 - (rot.x * distance / 180));
				followCam.setCoord(x, y, z);
				followCam.setRot(rot.x, 0, rot.z, 2);

				const vel = Math.round(target.getSpeed() * 3.6);
				const health = Math.round(target.getHealth());
				const armour = Math.round(target.getArmour());
				let str = "following: " + target.name + "~n~";
				str += "speed: " + vel + "~n~";
				str += "health: " + health + "~n~";
				str += "armour: " + armour + "~n~";
				mp.game.ui.setTextFont(4);
				mp.game.ui.setTextScale(0.5, 0.5);
				if (target.getVariable("wanted")) {
					mp.game.ui.setTextColour(255, 0, 0, 185);
				} else {
					mp.game.ui.setTextColour(255, 255, 0, 185);
				}
				mp.game.ui.setTextCentre(false);
				mp.game.ui.setTextJustification(2);
				mp.game.ui.setTextEntry("STRING");
				mp.game.ui.addTextComponentSubstringPlayerName(str);
				mp.game.ui.drawText(1.0, 0.0);
				if (target.vehicle) {
					const bh = Math.round(target.vehicle.getBodyHealth());
					const eh = Math.round(target.vehicle.getEngineHealth());
					const name = target.vehicle.getVariable("name") || target.vehicle.model;
					str = "======================~n~";
					str += "Vehcile: " + name + "~n~";
					str += "Body: " + bh + "~n~";
					str += "Engine: " + eh + "~n~";
					str += "======================";
					mp.game.ui.clearAdditionalText();
					mp.game.ui.setTextFont(4);
					mp.game.ui.setTextScale(0.5, 0.5);
					mp.game.ui.setTextColour(255, 255, 0, 185);
					mp.game.ui.setTextCentre(false);
					mp.game.ui.setTextJustification(2);
					mp.game.ui.setTextEntry("STRING");
					mp.game.ui.addTextComponentSubstringPlayerName(str);
					mp.game.ui.drawText(1.0, 0.12);
				}
			}
			player.following = true;
		} else if (player.following) {
			player.following = false;
			player.freezePosition(false);
			player.setNoCollision(player.handle, true);
			player.resetAlpha();
			if (followCam) {
				mp.game.cam.destroyAllCams(false);
				mp.game.cam.renderScriptCams(false, false, 0, true, false);
				followCam = false;
			}
		}
	});
});
