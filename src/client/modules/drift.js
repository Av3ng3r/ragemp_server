mp.events.add("client_loaded", () => {
	mp.av.drift = {};

	mp.av.drift.block = 0;

	let driftEnabled = false;
	let driftVehicle = false;

	// this is the logic to determine if we're drifting
	let score = 0;
	let screenScore = 0;
	let anterior = 0;
	let max = 0;
	let total = 0;
	let mult = 1;
	let nowShowing = false;
	let driftInterval = 0;
	let tick, idleTime, multTime, driftTime;

	Math.rad = Math.rad || function(deg) { return deg / 180.0 * Math.PI; }
	Math.deg = Math.deg || function(rad) { return rad * 180.0 / Math.PI; }

	mp.events.add("playerCommand", (command) => {
		const args = command.split(/[ ]+/);
		if (args[0] === "enableDrift") mp.av.drift.enableDriftMode();
		else if (args[0] === "disableDrift") mp.av.drift.disableDriftMode();
	});

	mp.av.drift.driftEnabled = () => {
		return driftEnabled;
	};

	mp.av.drift.toggle = () => {
		if (driftEnabled) mp.av.drift.disableDriftMode();
		else mp.av.drift.enableDriftMode();
	};

	mp.av.drift.enableDriftMode = (quiet) => {
		if (!quiet) mp.game.graphics.notify("enabling drift mode");
		setVehicleHandling(mp.players.local.vehicle, false);
		driftEnabled = true;
		driftInterval = setInterval(calculateDrift, 50);
	};

	mp.av.drift.disableDriftMode = (quiet) => {
		if (!quiet) mp.game.graphics.notify("disabling drift mode");
		if (driftInterval) clearInterval(driftInterval);
		driftInterval = 0;
		driftEnabled = false;
		nowShowing = false;
		setVehicleHandling(mp.players.local.vehicle, false);
		if (mp.players.local.vehicle) mp.players.local.vehicle.setEngineTorqueMultiplier(1.0);
		if (driftVehicle) driftVehicle.setEngineTorqueMultiplier(1.0);
	};

	const setDriftIndicatorVisible = (vis) => {
		nowShowing = vis;
	};

	const isValidVehicle = (vehicle) => {
		if (!vehicle) return false;
		try {
			if (vehicle.getPedInSeat(-1) !== mp.players.local.handle) return false;
			if (!mp.game.vehicle.isThisModelACar(vehicle.model)) return false;
			if (vehicle.model === mp.game.joaat("voltic2")) return false;
		} catch (ex) {
			return false;
		}
		return true;
	};

	const calculateDrift = () => {
		const player = mp.players.local;
		const vehicle = player.vehicle;
		const finalScore = 0;
		if (!vehicle) return setDriftIndicatorVisible(false);
		if (!isValidVehicle(vehicle)) return setDriftIndicatorVisible(false);

		tick = Date.now();
		let [e, f] = angle(vehicle);
		let g = 750 > tick - (idleTime || 0);

		g || 0 == score || (
				anterior = score,
				total += anterior,
				score > max && (max = score)
			);
		0 != e && (
				0 == score && (driftTime = tick),
				0 == score && (multTime = tick),
				g ? score += Math.floor(e*f*mult) : score = Math.floor(e*f*mult),
				screenScore = score,
				idleTime = tick
			);

		if (1000 > tick - (idleTime || 0) && screenScore) {
			setDriftIndicatorVisible(true);
		} else {
			if (250 <= score) mp.events.callRemote("onPlayerDriftEnding", score);
			setDriftIndicatorVisible(false);
			score = 0;
		}
	};

	const angle = (a) => {
		const b = Math.rad;
		let c = a.getVelocity(),
		d = c.x, e = c.y, f = c.z;
		d /= 100, e /= 100, f /= 100;

		let g = Math.sqrt(d * d + e * e);
		if (1.0 < a.getHeightAboveGround()) return [0, g];
		let h = a.getRotation(2),
			i = h.x,
			j = h.y,
			k = h.z,
			[l, m] = [-Math.sin(b(k)), Math.cos(b(k))],
			n = tick - (multTime || 0);
			if (1 != mult && 0.3 >= g && 750 < n) --mult, multTime = tick;
			else if (1500 < n) {
				let a = 1;
				1350 <= score ? a = 5 : 450 <= score ? a = 4 : 150 <= score ? a = 3 :
					50 <= score && (a = 2);
				a > mult && (mult = a, multTime = tick)
			}
			let minSpeed = 0.2;
			if (driftEnabled) minSpeed = 0.12;
			if (minSpeed >= g) return [0, g];
			let o = (l * d + m * e) / g;
			if (0.966 < o || 0 > o) return [0, g];
			return [Math.deg(Math.acos(o)), g];
	};

	mp.events.add("render", () => {
		if (mp.av.drift.block > 0) {
			mp.av.drift.block--;
			return;
		}

		if (nowShowing) {
			mp.game.ui.setTextFont(4);
			mp.game.ui.setTextScale(1.0, 1.0);
			mp.game.ui.setTextColour(255,127,0,255);
			mp.game.ui.setTextCentre(true);
			mp.game.ui.setTextJustification(0);
			mp.game.ui.setTextEntry("STRING");
			mp.game.ui.addTextComponentSubstringPlayerName(screenScore.toString());
			mp.game.ui.drawText(0.5, 0.2);
		}

		let reversing = setVehicleHandling(mp.players.local.vehicle, driftEnabled) ||
		setVehicleHandling(driftVehicle, driftEnabled);

		setDriftTorque(mp.players.local.vehicle, driftEnabled && !reversing) ||
			setDriftTorque(driftVehicle, driftEnabled && !reversing);
	});

	const drawBoost = (boostVal) => {
		if (mp.av.speedo.visible) {
			mp.game.graphics.drawText(`${boostVal}x`, [0.99, 0.97], {
				font: 2,
				color: [255, 255, 255, 185],
				scale: [0.5, 0.5],
				outline: true
			});
		}
	};

	const setDriftTorque = (vehicle, enabled) => {
		if (vehicle) {
			const speed = vehicle.getSpeed();
			const max = mp.game.vehicle.getVehicleModelMaxSpeed(vehicle.model);
			if (!enabled) {
				/*
				if (speed >= max) {
					drawBoost(0);
					vehicle.setEngineTorqueMultiplier(0.0);
				} else {
					drawBoost(1);
					vehicle.setEngineTorqueMultiplier(1.0);
				}
				*/
				drawBoost(1);
				vehicle.setEngineTorqueMultiplier(1.0);
			} else {
				if (speed >= max) {
					drawBoost(0);
					vehicle.setEngineTorqueMultiplier(0.0);
				} else if (speed >= max * 0.75) {
					drawBoost(1);
					vehicle.setEngineTorqueMultiplier(1.0);
				} else if (speed >= max * 0.5) {
					drawBoost(6);
					vehicle.setEngineTorqueMultiplier(6.0);
				} else if (speed >= max * 0.25) {
					drawBoost(12);
					vehicle.setEngineTorqueMultiplier(12.0);
				} else if (speed > 0) {
					drawBoost(24);
					vehicle.setEngineTorqueMultiplier(24);
				} else {
					drawBoost(1);
					vehicle.setEngineTorqueMultiplier(1.0);
				}
			}
			return true;
		}
		return false;
	};

	const setVehicleHandling = (vehicle, driftMode) => {
		let reversing = false;
		if (vehicle) try {
			if (!isValidVehicle(vehicle)) return;

			const vec = vehicle.getSpeedVector(true);
			reversing = mp.game.controls.isControlPressed(0, 72) && (vec.y < 0);
			driftMode &= !reversing;

			if (!vehicle.driftMode && driftMode) {
				vehicle.driftMode = true;
				vehicle.setHandling("FMASS", 1500.0);
				vehicle.setHandling("FINITIALDRAGCOEFF", 0.00062);
				vehicle.setHandling("FINITIALDRIVEFORCE", 5.55);
				vehicle.setHandling("FDRIVEBIASFRONT", 0.0);
				vehicle.setHandling("FDRIVEINERTIA", 1.3);
				vehicle.setHandling("FCLUTCHCHANGERATESCALEUPSHIFT", 1.6);
				vehicle.setHandling("FCLUTCHCHANGERATESCALEDOWNSHIFT", 1.6);
				vehicle.setHandling("FBRAKEFORCE", 1.5);
				vehicle.setHandling("FHANDBRAKEFORCE", 52.0);
				vehicle.setHandling("FSTEERINGLOCK", 1.3963);
				vehicle.setHandling("FTRACTIONCURVEMIN", 1.25);
				vehicle.setHandling("FTRACTIONCURVEMAX", 0.9);
				vehicle.setHandling("FTRACTIONCURVELATERAL", 25.0);
				vehicle.setHandling("FTRACTIONSPRINGELTAMAX", 0.15);
				vehicle.setHandling("FLOWSPEEDTRACTIONLOSSMULT", 1.0);
			} else if (vehicle.driftMode !== false && !driftMode) {
				vehicle.driftMode = false;
				let val = mp.av.vehicle.getHandling(vehicle, "fMass", 1500);
				vehicle.setHandling("FMASS", val);
				val = mp.av.vehicle.getHandling(vehicle, "fInitialDragCoeff", 0.0011);
				vehicle.setHandling("FINITIALDRAGCOEFF", val);
				val = mp.av.vehicle.getHandling(vehicle, "fInitialDriveForce", 0.33);
				vehicle.setHandling("FINITIALDRIVEFORCE", val);
				val = mp.av.vehicle.getHandling(vehicle, "fDriveBiasFront", 0.45);
				vehicle.setHandling("FDRIVEBIASFRONT", val);
				val = mp.av.vehicle.getHandling(vehicle, "fDriveInertia", 1.0);
				vehicle.setHandling("FDRIVEINERTIA", val);
				val = mp.av.vehicle.getHandling(vehicle, "fClutchChangeRateScaleUpShift", 3);
				vehicle.setHandling("FCLUTCHCHANGERATESCALEUPSHIFT", val);
				val = mp.av.vehicle.getHandling(vehicle, "fClutchChangeRateScaleDownShift", 3);
				vehicle.setHandling("FCLUTCHCHANGERATESCALEDOWNSHIFT", val);
				val = mp.av.vehicle.getHandling(vehicle, "fBrakeForce", 0.95);
				vehicle.setHandling("FBRAKEFORCE", val);
				val = mp.av.vehicle.getHandling(vehicle, "fHandBrakeForce", 1.2);
				vehicle.setHandling("FHANDBRAKEFORCE", val);
				val = mp.av.vehicle.getHandling(vehicle, "fSteeringLock", 0.872);
				vehicle.setHandling("FSTEERINGLOCK", val);
				val = mp.av.vehicle.getHandling(vehicle, "fTractionCurveMax", 2.16);
				vehicle.setHandling("FTRACTIONCURVEMAX", val);
				val = mp.av.vehicle.getHandling(vehicle, "fTractionCurveMin", 1.83);
				vehicle.setHandling("FTRACTIONCURVEMIN", val);
				val = mp.av.vehicle.getHandling(vehicle, "fTractionCurveLateral", 24.735);
				vehicle.setHandling("FTRACTIONCURVELATERAL", val);
				val = mp.av.vehicle.getHandling(vehicle, "fTractionSpringDeltaMax", 0.15);
				vehicle.setHandling("FTRACTIONSPRINGDELTAMAX", val);
				val = mp.av.vehicle.getHandling(vehicle, "fLowSpeedTractionLossMult", 0.7);
				vehicle.setHandling("FLOWSPEEDTRACTIONLOSSMULT", val);
			}
		} catch (ex) {}
		return reversing;
	};
	mp.events.add("tick_timeout", () => {
		mp.av.drift.disableDriftMode(true);
	});
});
