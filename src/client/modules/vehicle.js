mp.events.add("client_loaded", () => {
	mp.av.vehicle = {};

	const vehicleHandling = {};

	mp.events.add("handling", (name, data) => {
		vehicleHandling[name] = data;
	});

	mp.av.vehicle.getMods = (vehicle) => {
		let res = {};
		if (vehicle) {
			const mods = vehicle.getVariable("mods");
			if (mods) try {
				res = JSON.parse(mods);
			} catch (ex) {}
		}
		return res || {};
	}

	const convert = (name, value) => {
		name = name.toUpperCase();
		switch (name) {
			case "FSTEERINGLOCK":
				return value / 57.2958;
			case "FINITIALDRAGCOEFF":
				return value / 10000;
			case "FSUSPENSIONCOMPDAMP":
			case "FSUSPENSIONREBOUNDDAMP":
				return value / 10;
			case "FSUSPENSIONBIASFRONT":
				return value * 2.0;
		}

		if (parseFloat(value) == value) {
			return parseFloat(value);
		}

		return value;
	}

	mp.av.vehicle.getHandling = (vehicle, name, dfl) => {
		if (vehicle) {
			let handling = vehicleHandling[vehicle.getVariable("name")];
			if (handling) {
				handling = JSON.parse(handling);
				return convert(name, handling[name].value || handling[name]);
			}
		}
		return dfl;
	}

	const applyPaint = (entity, color1, color2, pearlescentColor, wheelColor, p, s) => {
		entity.setColours(color1, color2);
		entity.setExtraColours(pearlescentColor, wheelColor);
		if (color1 !== 0 && color1 !== 120) {
			entity.clearCustomPrimaryColour();
		} else {
			entity.setCustomPrimaryColour(p.r, p.g, p.b);
		}
		if (color2 !== 0 && color2 !== 120) {
			entity.clearCustomSecondaryColour();
		} else {
			entity.setCustomSecondaryColour(s.r, s.g, s.b);
		}
	};

	const applyPosition = (entity) => {
		const pitch = entity.getVariable("pitch") || 0;
		const roll = entity.getVariable("roll") || 0;
		const heading = entity.getVariable("heading") || 0;
	}

	const applyMods = mp.av.mechanic.applyMods = (entity, mods) => {
		mods = mods || entity.getVariable("mods");
		if (mods) try {
			let color1 = entity.getColor(0) || 0;
			let color2 = entity.getColor(1) || 0;
			let { pearlescentColor, wheelColor } = entity.getExtraColours(0, 0);
			let p = entity.getCustomPrimaryColour(0, 0, 0);
			let s = entity.getCustomSecondaryColour(0, 0, 0);

			mods = JSON.parse(mods);

			for (let type in mods) {
				const val = mods[type];
				switch (type) {
					case "neon":
						let neon = {};
						try {
							neon = JSON.parse(val);
						} catch (ex) {
							neon = { r: 255, g: 255, b: 255 };
						}
						if (neon) {
							entity.setNeonLightsColour(neon.r, neon.g, neon.b);
							entity.setNeonLightEnabled(0, true);
							entity.setNeonLightEnabled(1, true);
							entity.setNeonLightEnabled(2, true);
							entity.setNeonLightEnabled(3, true);
						}
						break;
					case "wheels":
						const data = JSON.parse(val);
						entity.setWheelType(data.type);
						entity.setMod(23, parseInt(data.value));
						entity.setMod(24, parseInt(data.value));
						break;
					case "color1":
						entity.color1 = color1 = parseInt(val);
						applyPaint(entity, color1, color2, pearlescentColor, wheelColor, p, s);
						break;
					case "color2":
						entity.color2 = color2 = parseInt(val);
						applyPaint(entity, color1, color2, pearlescentColor, wheelColor, p, s);
						break;
					case "primaryRgb":
						p = JSON.parse(val);
						applyPaint(entity, color1, color2, pearlescentColor, wheelColor, p, s);
						break;
					case "secondaryRgb":
						s = JSON.parse(val);
						applyPaint(entity, color1, color2, pearlescentColor, wheelColor, p, s);
						break;
					case "pearlColor":
						pearlescentColor = parseInt(val);
						applyPaint(entity, color1, color2, pearlescentColor, wheelColor, p, s);
						break
					case "wheelColor":
						wheelColor = parseInt(val);
						applyPaint(entity, color1, color2, pearlescentColor, wheelColor, p, s);
						break;
					case "windowTint":
						entity.setWindowTint(val);
						break;
					default:
						entity.setMod(parseInt(type), parseInt(val));
						break;
				}
			};
		} catch (ex) {
			// mp.gui.chat.push("ex: " + ex.message);
		}
	};

	let lastSpawnWhileWanted = 0;
	mp.events.add("spawnVehicle", (name, mods) => {
		if (mp.players.local.getVariable("wanted")) {
			const now = Date.now();
			if (now - lastSpawnWhileWanted < 30000) {
				const seconds = 30 - Math.floor((now - lastSpawnWhileWanted) / 1000);
				mp.game.graphics.notify(`Cannot spawn vehicle for another ${seconds} seconds`);
				return;
			}
			lastSpawnWhileWanted = now;
		} else {
			lastSpawnWhileWanted = 0;
		}

		if (mp.av.fun.isSubstituteActive()) mp.av.fun.toggleSubstitute();

		const driftEnabled = mp.av.drift.driftEnabled();
		if (driftEnabled) mp.av.drift.disableDriftMode();
		mp.av.drift.block = mp.av.fps;

		const joaat = mp.game.joaat(name);
		const vehicle = mp.vehicles.new(joaat, new mp.Vector3(0, 0, -50), {
			dimension: Math.floor(Math.random() * 50 + 50)
		});
		const isBig = vehicle.isBig();
		const cls = vehicle.getClass();
		vehicle.destroy();

		mp.events.callRemote("spawnVehicle", name, true, mods || "{}", isBig, cls);
	});

	mp.events.add("entityStreamIn", (entity) => {
		if (entity.type === "vehicle") {
			applyMods(entity);
			applyPosition(entity);
		}
	});

	mp.events.add("vehsync", () => {
		const id = mp.players.local.getVariable("vehicleId");
		if (id !== false) {
			const v = mp.vehicles.atRemoteId(id);
			if (v) {
				const sub = v.getVariable("sub");
				if (sub && !mp.av.fun.isSubstituteActive()) {
					mp.av.fun.toggleSubstitute(null, sub);
				}
			}
		}

		mp.vehicles.forEachInStreamRange(vehicle => {
			applyMods(vehicle);
			applyPosition(vehicle);
		});
	});

	setInterval(() => {
		mp.vehicles.forEachInStreamRange(v => v.setInvincible(false));

		const player = mp.players.local;
		const handle = player.getVehicleIsIn(false);
		const vehicle = handle ? mp.vehicles.atHandle(handle) : null;
		if (player.getVariable("godmode")) {
			if (vehicle && vehicle.getPedInSeat(-1) === player.handle) {
				mp.av.vehicles.disableVehicleWeapon(true, vehicle.handle, player.handle);
			}
		}
	}, 1000);

	let pendingPlayerEnterVehicle = false;
	setInterval(() => {
		if (pendingPlayerEnterVehicle !== false) try {
			const vehicle = pendingPlayerEnterVehicle;
			const player = mp.players.local;
			const handle = vehicle.getPedInSeat(-1);
			if (handle && handle === player.handle) {
				const sub = vehicle.getVariable("sub");
				if (sub && !mp.av.fun.isSubstituteActive()) {
					mp.av.fun.toggleSubstitute(null, sub);
				}
				pendingPlayerEnterVehicle = false;
			} else if (handle) {
				pendingPlayerEnterVehicle = false;
			}
		} catch (ex) {}
	}, 1000);

	mp.events.add("playerEnterVehicle", (vehicle, seat) => {
		pendingPlayerEnterVehicle = vehicle;
	});

	/*
	mp.events.add("playerEnterVehicle", (vehicle, seat) => {
		try {
			if (vehicle.getVariable("abandoned") === true) {
				const id = mp.players.local.getVariable("vehicleId");
				mp.events.callRemote("abandonVehicle", id);
				mp.events.callRemote("claimVehicle", vehicle.remoteId);
			}
		} catch (ex) {
		}
	});
	*/

	mp.events.add("playerCommand", (command) => {
		const args = command.split(/[ ]+/);
		const sound = "Hack_Success";
		const group = "DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS";
		if (args[0] === "lock") {
			mp.events.callRemote("lock", "true");
			mp.game.audio.playSoundFrontend(-1, sound, group, true);
		} else if (args[0] === "unlock") {
			mp.events.callRemote("lock", "false");
			mp.game.audio.playSoundFrontend(-1, sound, group, true);
		}
	});
});
