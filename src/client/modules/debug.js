mp.events.add("client_loaded", () => {
	mp.av.debug = {};

	mp.events.add("client_start", () => {
	});

	mp.events.add("playerCommand", (command) => {
		const args = command.split(/[ ]+/);
		if (args[0] === "getpos") {
			const { x, y, z } = mp.players.local.position;
			const h = mp.players.local.getHeading();
			mp.gui.chat.push(`pos: ${x} ${y} ${z} ${h}`);
		}
	});
});
