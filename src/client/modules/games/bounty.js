mp.events.add("client_loaded", () => {
	mp.av.bounty = {};

	let currentWantedLevel = 0;

	mp.events.add("wanted", (stars) => {
		if (stars > 5) stars = 5;
		if (stars < 0) stars = 0;
		if (currentWantedLevel != stars) try {
			currentWantedLevel = parseInt(stars, 10);
			mp.game.gameplay.setFakeWantedLevel(currentWantedLevel);
		} catch (ex) {}
	});

	setInterval(() => {
		const player = mp.players.local;
		if (player.getVariable("wanted")) {
			const { x, y, z } = player.position;
			let h = mp.game.water.getWaterHeightNoWaves(x, y, z, 0);
			const data = {
				underWater: player.isSwimmingUnderWater(),
				waterHeight: h
			};
			mp.events.callRemote("bounty_update", JSON.stringify(data));
		}
	}, 1000);

	mp.events.add("tick_timeout", () => {
		currentWantedLevel = 0;
		mp.game.gameplay.setFakeWantedLevel(currentWantedLevel);
	});
});
