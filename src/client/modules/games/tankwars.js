mp.events.add("client_loaded", () => {
	mp.av.tankwars = {};

	mp.av.tankwars.state = {
		PLAYING: 1,
		NOTPLAYING: 0
	};

	mp.av.tankwars.playerState = mp.av.tankwars.state.NOTPLAYING;

	let blip = null;
	let mainEntranceColshape = null;
	let rearColshape = null;
	let entryColshape = null;
	let entrycolshape2 = null;
	let leaveColshape = null;

	let enterMarker = null;
	let enterMarker2 = null;
	let leaveMarker = null;

	let scoreboard = null;

	function pointInTriangle(P, A, B, C) {
        // Compute vectors        
        function vec(from, to) {  return [to[0] - from[0], to[1] - from[1]];  }
        var v0 = vec(A, C);
        var v1 = vec(A, B);
        var v2 = vec(A, P);
        // Compute dot products
        function dot(u, v) {  return u[0] * v[0] + u[1] * v[1];  }
        var dot00 = dot(v0, v0);
        var dot01 = dot(v0, v1);
        var dot02 = dot(v0, v2);
        var dot11 = dot(v1, v1);
        var dot12 = dot(v1, v2);
        // Compute barycentric coordinates
        var invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
        var u = (dot11 * dot02 - dot01 * dot12) * invDenom;
        var v = (dot00 * dot12 - dot01 * dot02) * invDenom;
        // Check if point is in triangle
        return (u >= 0) && (v >= 0) && (u + v < 1);
	}

	function leaveTankWars(destruct) {
	}

	mp.events.add("client_start", () => {
	});
});
