mp.events.add("client_loaded", () => {
	mp.av.interiors = {};

	const markerData = [
		{ // disco entry
			pos: { x: -1388.564697, y: -586.213989, z: 30.21807 }, // pos of marker
			target: { x: -1387.422241, y: -588.586347, z: 30.31955, a: 90 }, // warp target
			radioIndex: 6, direction: 0, type: 0, colshapeId: -1, visible: true,
			broadcast: "enter_disco", blip: 93, name: "disco",
		},
		{ // disco exit
			pos: { x: -1387.899990, y: -587.619506, z: 30.31952 },
			target: { x: -1390.528076, y: -583.564758, z: 30.32400, a: 0 },
			radioIndex: 255, direction: 0, type: 0, colshapeId: -1, visible: false,
			broadcast: "exit_disco"
		},
		{ // apartment 1 entry
			pos: { x: -270.270263671875, y: -957.9014892578125, z: 31.2231369019 },
			_pos: { x: -261.425598, y: -970.80524, z: 31.22 },
			target: { x: -273.5682067871094, y: -939.841697265625, z: 92.510894776, a: 79.24 },
			_target: { x: -270.7701416, y: -968.149475, z: 77.23133, a: 270 },
			radioIndex: 255, direction: 0, type: 0, colshapeId: -1, visible: true
		},
		{ // apartment 1 exit
			_pos: { x: -273.1572265, y: -967.1578979, z: 77.23133 },
			pos: { x: -269.68292236328125, y: -941.4090576171875, z: 92.51089477539062 },
			target: { x: -268.524658203125, y: -956.4979248046875, z: 31.223134994506836, a: 208 },
			_target: { x: -261.337921, y: -972.446777, z: 31.21995629, a: 225 },
			radioIndex: 255, direction: 0, type: 0, colshapeId: -1, visible: false
		},
		{ // eclipse tower entry
			pos: { x: -777.0127563, y: 319.646637, z: 85.6626 },
			target: { x: -774.358, y: 333.952, z: 207.621, a: 0 },
			radioIndex: 2, direction: 0, type: 0, colshapeId: -1, visible: true
		},
		{ // eclipse tower exit
			pos: { x: -774.277, y: 330.873, z: 207.621 },
			target: { x: -770.8250732, y: 319.683044, z: 85.6626, a: 180 },
			radioIndex: 255, direction: 0, type: 0, colshapeId: -1, visible: false
		},
		{ // martin madrazo entry
			pos: { x: 1394.747, y: 1152.652, z: 114.399 },
			target: { x: 1397.435, y: 1141.728, z: 114.334, a: 270 },
			radioIndex: 1, direction: 0, type: 0, colshapeId: -1, visible: true
		},
		{ // martin madrazo exit
			pos: { x: 1396.346, y: 1141.711, z: 114.334 },
			target: { x: 1393.865, y: 1152.777, z: 114.443, a: 90 },
			radioIndex: 255, direction: 0, type: 0, colshapeId: -1, visible: false
		},
		{ // vinewood hills  entry
			pos: { x: -173.42807, y: 502.821869, z: 137.42341 },
			target: { x: -173.692307, y: 496.161834, z: 137.66702, a: 200 },
			radioIndex: 1, direction: 0, type: 0, colshapeId: -1, visible: true
		},
		{ // vinewood hills exit
			pos: { x: -174.656589, y: 497.738281, z: 137.653 },
			target: { x: -176.075058, y: 502.170402, z: 137.420654, a: 40 },
			radioIndex: 255, direction: 0, type: 0, colshapeId: -1, visible: false
		}
	];

	const colshapes = [];
	const createMarkers = () => {
		markerData.forEach((data, i) => {
			let { x, y, z } = data.pos;
			if (data.visible) {
				let pos = new mp.Vector3(x, y, z);
				let rot = new mp.Vector3(0, 0, 0);
				let marker = mp.markers.new(0, pos, 1.0, {
					visible: true, direction: rot, rotation: rot,
					color: [ 255, 255, 255, 255 ],
					dimension: mp.players.local.dimension
				});
				if (undefined !== data.blip) {
					mp.blips.new(data.blip, new mp.Vector3(x, y, 0), {
						name: data.name,
						color: 5,
						shortRange: false
					});
				}
			}

			let colshape = mp.colshapes.newCircle(x, y, 1.0);
			if (colshape) markerData[i].colshapeId = colshape.id;
		});
	};

	mp.events.add("playerEnterColshape", (shape) => {
		let data = null;
		if (undefined !== shape.id) {
			data = markerData.find(marker => marker.colshapeId === shape.id);
		} else if (markerData[shape]) {
			data = markerData[shape];
		}

		if (data) {
			let min = Math.floor(mp.players.local.position.z) - 1.0;
			let max = Math.ceil(mp.players.local.position.z) + 1.0;
			let {x, y, z, a} = data.target;
			if (min <= data.pos.z && data.pos.z <= max) {
				mp.players.local.setCoords(x, y, z, true, false, false, false);
				mp.players.local.setHeading(a);
				if (data.broadcast) mp.events.call(data.broadcast);
			}
			if (data.radioIndex === 255) {
				// turn off radio
				mp.game.audio.setMobileRadioEnabledDuringGameplay(false);
			} else if (data.radioIndex) {
				mp.game.audio.setMobileRadioEnabledDuringGameplay(true);
				mp.game.audio.setRadioToStationIndex(data.radioIndex);
			}
		}

	});
	mp.events.add("loggedin", () => {
		createMarkers();
	});
});
