mp.events.add("client_loaded", () => {
	mp.events.add("freezeVehicle", (id, x, y, z, a) => {
		const vehicle = mp.vehicles.atRemoteId(id);
		if (vehicle) {
			// vehicle.freezePosition(true);
			if (x || y || z) vehicle.setVelocity(x, y, z);
			vehicle.setEngineOn(true, true, true);
			vehicle.steeringAngle = a;
		}
	});
	mp.events.add("unfreezeVehicle", (id) => {
		const vehicle = mp.vehicles.atRemoteId(id);
		if (vehicle) vehicle.freezePosition(false);
	});
	/*
	mp.av.replicas = {
		players: [],
		vehicles: []
	};

	mp.events.add("rsync", (data) => {
		const obj = JSON.parse(data);
		if (obj) obj.forEach(updatePlayer);
	});

	mp.events.add("rdelete", (data) => {
		const obj = JSON.parse(data);
		const player = mp.av.replicas[data.id];
		if (player) {
			const ped = player.ped;
			if (ped) ped.destroy();
			delete mp.av.replicas[data.id];
		}
	});

	const updatePlayer = (data, index) => {
		if (data.dimension !== mp.players.local.dimension) return;

		if (undefined === mp.av.replicas.players[data.id]) {
			mp.av.replicas.players[data.id] = {
				ped: mp.game.ped.createPed(
					mp.players.local.dimension,
					data.model,
					data.position.x,
					data.position.y,
					data.position.z,
					data.position.heading,
					false, true
				),
				...data
			}
		}

		// TODO: sync actions, weapons
		const player = mp.av.replicas.players[data.id];
		if (player) {
			const ped = player.ped;
			if (ped) {
				ped.position = data.position;
				ped.setHeading(data.heading);
				if (player.model !== data.model) {
					player.model = ped.model = data.model;
				}
			}
		}
	};
	*/
});
