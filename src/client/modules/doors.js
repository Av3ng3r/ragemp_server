(function() {
	mp.av.doors = {};

	const doors = [
	{
		hash: mp.game.joaat('prop_com_ls_door_01'),
		x: -356.0905,
		y: -134.7714,
		z: 40.01295
	},
	{
		hash: mp.game.joaat('hw1_13_garage_door_01'),
		x: 966.0857,
		y: -114.8387,
		z: 75.24055
	},
	{
		hash: mp.game.joaat('hw1_03_garage01'),
		x: 518.8464,
		y: 169.8174,
		z: 100.9713
	},
	{
		hash: mp.game.joaat('v_ilev_carmod3door'),
		x: 1174.654,
		y: 2645.222,
		z: 38.63961
	},
	{
		hash: mp.game.joaat('prop_com_gar_door_01'),
		x: 1204.57,
		y: -3110.4,
		z: 6.57
	},
	{
		hash: mp.game.joaat('v_ilev_rc_door2'),
		x: 467.3716,
		y: -1014.452,
		z: 26.5362
	},
	{
		hash: mp.game.joaat('v_ilev_gtdoor02'),
		x: 464.3613,
		y: -984.6779,
		z: 43.8344
	},
	{
		hash: mp.game.joaat('v_ilev_ph_door01'),
		x: 434.75,
		y: -980.61,
		z: 30.84
	},
	{
		hash: mp.game.joaat('v_ilev_ph_door002'),
		x: 434.75,
		y: -983.22,
		z: 30.84
	},
	{
		hash: mp.game.joaat('prop_biolab_g_door'),
		x: 3589.1,
		y: 3671.5,
		z: 35
	},
	{
		hash: mp.game.joaat('v_ilev_bl_shutter2'),
		x: 3627.71,
		y: 3746.72,
		z: 27.69
	},
	{
		hash: mp.game.joaat('v_ilev_abbmaindoor'),
		x: 962.1,
		y: -2183.83,
		z: 31.06
	},
	{
		hash: mp.game.joaat('v_ilev_abbmaindoor2'),
		x: 961.79,
		y: -2187.08,
		z: 31.06
	},
	{
		hash: mp.game.joaat('prop_gate_airport_01'),
		x: -1138.47,
		y: -2730.45,
		z: 12.95
	},
	{
		hash: mp.game.joaat('prop_facgate_01'),
		x: -1213.4,
		y: -2079.3,
		z: 12.907
	},
	{
		hash: mp.game.joaat('prop_fnclink_02gate6_r'),
		x: -967.4473,
		y: -2778.495,
		z: 14.409
	},
	{
		hash: mp.game.joaat('prop_fnclink_02gate6_l'),
		x: -974.5734,
		y: -2774.381,
		z: 14.4099
	},
	{
		hash: mp.game.joaat('prop_fnclink_09gate1'),
		x: -828.9456,
		y: -2964.304,
		z: 14.2758
	},
	{
		hash: mp.game.joaat('v_ilev_lester_doorfront'),
		x: 1273.82,
		y: -1720.7,
		z: 54.92
	},
	{
		hash: mp.game.joaat('v_ilev_tort_door'),
		x: 134.4,
		y: -2204.1,
		z: 7.52
	},
	{
		hash: mp.game.joaat('v_ilev_janitor_frontdoor'),
		x: -107.5373,
		y: -9.0181,
		z: 70.6708
	},
	{
		hash: mp.game.joaat('v_ilev_deviantfrontdoor'),
		x: -128.33,
		y: -1457.17,
		z: 37.94
	},
	{
		hash: mp.game.joaat('v_ilev_po_door'),
		x: -1910.58,
		y: -576.01,
		z: 19.25
	},
	{
		hash: mp.game.joaat('v_ilev_ra_door3'),
		x: 1395.92,
		y: 1142.9,
		z: 114.79
	},
	{
		hash: mp.game.joaat('v_ilev_ra_door1_l'),
		x: 1390.52,
		y: 1163.44,
		z: 114.38
	},
	{
		hash: mp.game.joaat('v_ilev_ra_door1_r'),
		x: 1390.52,
		y: 1161.24,
		z: 114.38
	},
	{
		hash: mp.game.joaat('v_ilev_bl_door_l'),
		x: -1387.05,
		y: -586.58,
		z: 30.47
	},
	{
		hash: mp.game.joaat('v_ilev_bl_door_r'),
		x: -1389.24,
		y: -588,
		z: 30.47
	},
	{
		hash: mp.game.joaat('prop_sec_gate_01c'),
		x: 25.03,
		y: -664.6,
		z: 31.04
	},
	{
		hash: mp.game.joaat('prop_sec_gate_01d'),
		x: -72.75,
		y: -682.17,
		z: 33.27
	},
	{
		hash: mp.game.joaat('v_ilev_bank4door02'),
		x: -111.48,
		y: 6463.94,
		z: 31.985
	},
	{
		hash: mp.game.joaat('v_ilev_bank4door01'),
		x: -109.65,
		y: 6462.11,
		z: 31.985
	},
	{
		hash: mp.game.joaat('hei_v_ilev_bk_gate_pris'),
		x: 256.3116,
		y: 220.6579,
		z: 106.4296
	},
	{
		hash: mp.game.joaat('hei_v_ilev_bk_gate2_pris'),
		x: 262.1981,
		y: 222.5188,
		z: 106.4296
	},
	{
		hash: mp.game.joaat('v_ilev_genbankdoor1'),
		x: -2965.821,
		y: 481.63,
		z: 16.048
	},
	{
		hash: mp.game.joaat('v_ilev_genbankdoor2'),
		x: -2965.71,
		y: 484.219,
		z: 16.048
	},
	{
		hash: mp.game.joaat('v_ilev_fibl_door02'),
		x: 106.38,
		y: -742.7,
		z: 46.18
	},
	{
		hash: mp.game.joaat('v_ilev_fibl_door01'),
		x: 105.76,
		y: -746.65,
		z: 46.18
	},
	{
		hash: mp.game.joaat('prop_ch3_01_trlrdoor_l'),
		x: 2333.23,
		y: 2574.97,
		z: 47.03
	},
	{
		hash: mp.game.joaat('prop_ch3_01_trlrdoor_r'),
		x: 2329.65,
		y: 2576.64,
		z: 47.03
	},
	{
		hash: mp.game.joaat('v_ilev_fh_frntdoor'),
		x: 0.2169,
		y: -1823.303,
		z: 29.6391
	},
	{
		hash: mp.game.joaat('v_ilev_bl_doorel_l'),
		x: -2053.16,
		y: 3239.49,
		z: 30.5
	},
	{
		hash: mp.game.joaat('v_ilev_bl_doorel_r'),
		x: -2054.39,
		y: 3237.23,
		z: 30.5
	},
	{
		hash: mp.game.joaat('prop_cs4_05_tdoor'),
		x: 31.918,
		y: 3666.854,
		z: 40.8586
	},
	{
		hash: mp.game.joaat('prop_magenta_door'),
		x: 29.102,
		y: 3661.489,
		z: 40.8547
	},
	{
		hash: mp.game.joaat('hei_prop_heist_cutscene_doorb'),
		x: 776.8,
		y: 4184.64,
		z: 41.91
	},
	{
		hash: mp.game.joaat('apa_prop_apa_cutscene_doora'),
		x: 722.399,
		y: 4187.952,
		z: 41.231
	},
	{
		hash: mp.game.joaat('hei_prop_heist_cutscene_doorc_r'),
		x: 610.594,
		y: -421.83,
		z: 24.979
	},
	{
		hash: mp.game.joaat('hei_prop_heist_cutscene_doorc_l'),
		x: 610.874,
		y: -419.365,
		z: 24.979
	},
	{
		hash: mp.game.joaat('prop_gate_docks_ld'),
		x: 492,
		y: -3116,
		z: 5
	},
	{
		hash: mp.game.joaat('lr_prop_boathousedoor_l'),
		x: 1527.745,
		y: 3778.131,
		z: 34.7106
	},
	{
		hash: mp.game.joaat('lr_prop_boathousedoor_r'),
		x: 1530.762,
		y: 3780.244,
		z: 34.7104
	},
	{
		hash: mp.game.joaat('prop_fnclink_03gate2'),
		x: -687.73,
		y: -2458.82,
		z: 12.9
	},
	{
		hash: mp.game.joaat('prop_gate_military_01'),
		x: 2485.44,
		y: -432.71,
		z: 91.97
	},
	{
		hash: mp.game.joaat('prop_gate_prison_01'),
		x: 2485.09,
		y: -335.84,
		z: 91.98
	},
	{
		hash: mp.game.joaat('prop_fnclink_03gate1'),
		x: 222.07,
		y: -2013.99,
		z: 18.41
	},
	{
		hash: mp.game.joaat('prop_facgate_01b'),
		x: 459.7,
		y: -2002.94,
		z: 22.07
	},
	{
		hash: mp.game.joaat('prop_facgate_08'),
		x: 488.89,
		y: -1011.67,
		z: 27.14
	},
	{
		hash: mp.game.joaat('prop_lrggate_03a'),
		x: -1876.37,
		y: 194.85,
		z: 83.33
	},
	{
		hash: mp.game.joaat('prop_lrggate_02_ld'),
		x: -1583.28,
		y: 40.14,
		z: 59.32
	},
	{
		hash: mp.game.joaat('prop_lrggate_04a'),
		x: -914.32,
		y: 184.54,
		z: 68.42
	},
	{
		hash: mp.game.joaat('v_ilev_trev_doorfront'),
		x: -1149.71,
		y: -1521.09,
		z: 10.79
	},
	{
		hash: mp.game.joaat('v_ilev_ct_door01'),
		x: -2343.53,
		y: 3265.37,
		z: 32.96
	},
	{
		hash: mp.game.joaat('v_ilev_housedoor1'),
		x: 347.8678,
		y: -1003.316,
		z: -99.0952
	},
	{
		hash: mp.game.joaat('v_ilev_mm_door'),
		x: 257.2896,
		y: -1001.255,
		z: -98.8587
	},
	{
		hash: mp.game.joaat('v_ilev_mm_doorw'),
		x: 348.2157,
		y: -993.1122,
		z: -99.043
	},
	{
		hash: mp.game.joaat('v_ilev_fh_bedrmdoor'),
		x: -789.3017,
		y: 332.0119,
		z: 201.5596
	},
	{
		hash: mp.game.joaat('prop_ld_garaged_01'),
		x: -815.3282,
		y: 185.9571,
		z: 72.99
	},
	{
		hash: mp.game.joaat('v_ilev_mm_windowwc'),
		x: -802.7333,
		y: 167.5041,
		z: 77.5824
	},
	{
		hash: mp.game.joaat('v_ilev_mm_doorm_l'),
		x: -816.716,
		y: 179.098,
		z: 72.84
	},
	{
		hash: mp.game.joaat('v_ilev_mm_doorm_r'),
		x: -816.1068,
		y: 177.5109,
		z: 72.8274
	},
	{
		hash: mp.game.joaat('prop_bh1_48_backdoor_l'),
		x: -796.5657,
		y: 177.2214,
		z: 73.0405
	},
	{
		hash: mp.game.joaat('prop_bh1_48_backdoor_r'),
		x: -794.5051,
		y: 178.0124,
		z: 73.0405
	},
	{
		hash: mp.game.joaat('prop_gate_cult_01_l'),
		x: -1041.268,
		y: 4906.097,
		z: 209.2002
	},
	{
		hash: mp.game.joaat('prop_gate_cult_01_r'),
		x: -1044.749,
		y: 4914.972,
		z: 209.1932
	},
	{
		hash: mp.game.joaat('v_ilev_trevtraildr'),
		x: 1972.769,
		y: 3815.366,
		z: 33.6633
	},
	{
		hash: mp.game.joaat('v_ilev_fa_frontdoor'),
		x: -14.8689,
		y: -1441.182,
		z: 31.192
	},
	{
		hash: mp.game.joaat('v_ilev_ss_door8'),
		x: 716.7808,
		y: -975.4207,
		z: 25.057
	},
	{
		hash: mp.game.joaat('v_ilev_ss_door7'),
		x: 719.3818,
		y: -975.4185,
		z: 25.0057
	},
	{
		hash: mp.game.joaat('v_ilev_ss_door02'),
		x: 710,
		y: -964,
		z: 31
	},
	{
		hash: mp.game.joaat('v_ilev_store_door'),
		x: 708,
		y: -962,
		z: 31
	},
	{
		hash: mp.game.joaat('p_jewel_door_l'),
		x: -631.9554,
		y: -236.3333,
		z: 38.2065
	},
	{
		hash: mp.game.joaat('p_jewel_door_r1'),
		x: -630.4265,
		y: -238.4375,
		z: 38.2065
	},
	{
		hash: mp.game.joaat('v_ilev_ss_door04'),
		x: 1395.613,
		y: 3609.327,
		z: 35.1308
	},
	{
		hash: mp.game.joaat('v_ilev_ss_doorext'),
		x: 1388.499,
		y: 3614.828,
		z: 39.0919
	},
	{
		hash: mp.game.joaat('v_ilev_cs_door'),
		x: 482.8112,
		y: -1311.953,
		z: 29.3506
	},
	{
		hash: mp.game.joaat('v_ilev_epsstoredoor'),
		x: 241.3621,
		y: 361.0471,
		z: 105.003
	},
	{
		hash: mp.game.joaat('v_ilev_fh_frontdoor'),
		x: 7.5179,
		y: 539.526,
		z: 176.1781
	},
	{
		hash: mp.game.joaat('prop_ch_025c_g_door_01'),
		x: 18.6504,
		y: 546.3401,
		z: 176.3448
	},
	{
		hash: mp.game.joaat('v_ilev_csr_door_l'),
		x: -59.893,
		y: -1092.952,
		z: 26.88362
	},
	{
		hash: mp.game.joaat('v_ilev_csr_door_r'),
		x: -60.5458,
		y: -1094.749,
		z: 26.88872
	},
	{
		hash: mp.game.joaat('v_ilev_rc_door3_l'),
		x: -608.7289,
		y: -1610.315,
		z: 27.1589
	},
	{
		hash: mp.game.joaat('v_ilev_rc_door3_r'),
		x: -611.32,
		y: -1610.089,
		z: 27.1589
	},
	{
		hash: mp.game.joaat('v_ilev_shrf2door'),
		x: -442.66,
		y: 6015.222,
		z: 31.8663
	},
	{
		hash: mp.game.joaat('v_ilev_shrfdoor'),
		x: 1855.685,
		y: 3683.93,
		z: 34.5928
	},
	{
		hash: mp.game.joaat('prop_gar_door_01'),
		x: -1067.002,
		y: -1665.609,
		z: 4.7898
	},
	{
		hash: mp.game.joaat('prop_gar_door_02'),
		x: -1064.763,
		y: -1668.762,
		z: 4.8084
	},
	{
		hash: mp.game.joaat('prop_gar_door_03_ld'),
		x: -1074.648,
		y: -1676.131,
		z: 4.684
	},
	{
		hash: mp.game.joaat('prop_gar_door_05'),
		x: 201.4,
		y: -153.3652,
		z: 57.8522
	},
	{
		hash: mp.game.joaat('v_ilev_fb_doorshortl'),
		x: -1045.12,
		y: -232.004,
		z: 39.4379
	},
	{
		hash: mp.game.joaat('v_ilev_fb_doorshortr'),
		x: -1046.516,
		y: -229.3581,
		z: 39.4379
	},
	{
		hash: mp.game.joaat('v_ilev_fb_door01'),
		x: -1083.62,
		y: -260.4167,
		z: 38.1867
	},
	{
		hash: mp.game.joaat('v_ilev_fb_door02'),
		x: -1080.974,
		y: -259.0204,
		z: 38.1867
	},
	{
		hash: mp.game.joaat('v_ilev_gtdoor'),
		x: -1042.57,
		y: -240.6,
		z: 38.11
	},
	{
		hash: mp.game.joaat('v_ilev_door_orange'),
		x: -1063.804,
		y: -240.832,
		z: 39.883
	},
	{
		hash: mp.game.joaat('v_ilev_door_orangesolid'),
		x: -1055.958,
		y: -236.425,
		z: 44.171
	},
	{
		hash: mp.game.joaat('v_ilev_roc_door4'),
		x: -565.1712,
		y: 276.6259,
		z: 83.2863
	},
	{
		hash: mp.game.joaat('prop_ron_door_01'),
		x: 1065.237,
		y: -2006.079,
		z: 32.2329
	},
	{
		hash: mp.game.joaat('prop_abat_slide'),
		x: 962.9084,
		y: -2105.814,
		z: 34.6432
	},
	{
		hash: mp.game.joaat('prop_gar_door_04'),
		x: 778.31,
		y: -1867.49,
		z: 30.66
	},
	{
		hash: mp.game.joaat('prop_ch3_04_door_01l'),
		x: 2514.32,
		y: -317.34,
		z: 93.32
	},
	{
		hash: mp.game.joaat('prop_ch3_04_door_01r'),
		x: 2512.42,
		y: -319.26,
		z: 93.32
	},
	{
		hash: mp.game.joaat('prop_sc1_12_door'),
		x: -58.47,
		y: -1530.51,
		z: 34.54
	},
	{
		hash: mp.game.joaat('prop_ch1_07_door_01l'),
		x: -2255.194,
		y: 322.2593,
		z: 184.9264
	},
	{
		hash: mp.game.joaat('prop_ch1_07_door_01r'),
		x: -2254.056,
		y: 319.7009,
		z: 184.9264
	},
	{
		hash: mp.game.joaat('prop_facgate_07b'),
		x: 397.8846,
		y: -1607.384,
		z: 28.3301
	},
	{
		hash: mp.game.joaat('prop_sc1_21_g_door_01'),
		x: -25.28,
		y: -1431.06,
		z: 30.84
	},
	{
		hash: mp.game.joaat('prop_ss1_14_garage_door'),
		x: -62.38,
		y: 352.7173,
		z: 113.2499
	},
	{
		hash: mp.game.joaat('prop_motel_door_09'),
		x: 549.2567,
		y: -1773.115,
		z: 33.7309
	},
	{
		hash: mp.game.joaat('prop_ss1_10_door_l'),
		x: -720.39,
		y: 256.86,
		z: 80.29
	},
	{
		hash: mp.game.joaat('prop_ss1_10_door_r'),
		x: -718.42,
		y: 257.79,
		z: 80.29
	},
	{
		hash: mp.game.joaat('v_ilev_ph_cellgate'),
		x: 461.8065,
		y: -994.4086,
		z: 25.0644
	},
	{
		hash: mp.game.joaat('v_ilev_cf_officedoor'),
		x: -70.5223,
		y: 6254.584,
		z: 31.2331
	},
	{
		hash: mp.game.joaat('prop_sec_barrier_ld_01a'),
		x: -1588.27,
		y: 2794.21,
		z: 16.85
	},
	{
		hash: mp.game.joaat('v_ilev_gb_vaubar'),
		x: -1207.328,
		y: -335.129,
		z: 38.079
	},
	{
		hash: mp.game.joaat('v_ilev_j2_door'),
		x: -629.134,
		y: -230.152,
		z: 38.207
	},
	{
		hash: mp.game.joaat('v_ilev_bs_door'),
		x: 133,
		y: -1711,
		z: 29
	},
	{
		hash: mp.game.joaat('v_ilev_hd_door_l'),
		x: -823.2001,
		y: -187.0831,
		z: 37.819
	},
	{
		hash: mp.game.joaat('v_ilev_hd_door_r'),
		x: -822.4442,
		y: -188.3924,
		z: 37.819
	},
	{
		hash: mp.game.joaat('v_ilev_cs_door01'),
		x: 82.3186,
		y: -1392.752,
		z: 29.5261
	},
	{
		hash: mp.game.joaat('v_ilev_cs_door01_r'),
		x: 82.3186,
		y: -1390.476,
		z: 29.5261
	},
	{
		hash: mp.game.joaat('v_ilev_clothmiddoor'),
		x: -1201.435,
		y: -776.8566,
		z: 17.9918
	},
	{
		hash: mp.game.joaat('v_ilev_ch_glassdoor'),
		x: -716.6754,
		y: -155.42,
		z: 37.6749
	},
	{
		hash: mp.game.joaat('v_ilev_ta_door'),
		x: 321.81,
		y: 178.36,
		z: 103.68
	},
	{
		hash: mp.game.joaat('v_ilev_ml_door1'),
		x: 1859.89,
		y: 3749.79,
		z: 33.18
	},
	{
		hash: mp.game.joaat('prop_bh1_48_gate_1'),
		x: -849,
		y: 179,
		z: 70
	},
	{
		hash: mp.game.joaat('prop_cs4_10_tr_gd_01'),
		x: 1972.787,
		y: 3824.554,
		z: 32.5831
	},
	{
		hash: mp.game.joaat('prop_com_ls_door_01'),
		x: -1145.9,
		y: -1991.14,
		z: 14.18
	},
	{
		hash: mp.game.joaat('prop_id2_11_gdoor'),
		x: 723.12,
		y: -1088.83,
		z: 23.28
	},
	{
		hash: mp.game.joaat('v_ilev_carmod3door'),
		x: 108.8502,
		y: 6617.876,
		z: 32.673
	},
	{
		hash: mp.game.joaat('lr_prop_supermod_door_01'),
		x: -205.7007,
		y: -1310.692,
		z: 30.2957
	},
	{
		hash: mp.game.joaat('v_ilev_ss_door03'),
		x: 709.9894,
		y: -960.6675,
		z: 30.5453
	},
	{
		hash: mp.game.joaat('v_ilev_lester_doorveranda'),
		x: 1271.89,
		y: -1707.57,
		z: 53.79
	},
	{
		hash: mp.game.joaat('prop_strip_door_01'),
		x: 128,
		y: -1299,
		z: 29
	},
	{
		hash: mp.game.joaat('v_ilev_gangsafedoor'),
		x: 974,
		y: -1839,
		z: 36
	},
	{
		hash: mp.game.joaat('prop_cs6_03_door_l'),
		x: 1396,
		y: 1143,
		z: 115
	},
	{
		hash: mp.game.joaat('prop_cs6_03_door_r'),
		x: 1396,
		y: 1141,
		z: 115
	},
	{
		hash: mp.game.joaat('prop_map_door_01'),
		x: -1104.66,
		y: -1638.48,
		z: 4.68
	},
	{
		hash: mp.game.joaat('v_ilev_fib_door1'),
		x: -31.72,
		y: -1101.85,
		z: 26.57
	},
	{
		hash: mp.game.joaat('v_ilev_fh_door4'),
		x: 1988.353,
		y: 3054.411,
		z: 47.3204
	},
	{
		hash: mp.game.joaat('prop_epsilon_door_l'),
		x: -700.17,
		y: 47.31,
		z: 44.3
	},
	{
		hash: mp.game.joaat('prop_epsilon_door_r'),
		x: -697.94,
		y: 48.35,
		z: 44.3
	},
	{
		hash: mp.game.joaat('prop_ch2_09c_garage_door'),
		x: -689.11,
		y: 506.97,
		z: 110.64
	},
	{
		hash: mp.game.joaat('p_cut_door_03'),
		x: 23.34,
		y: -1897.6,
		z: 23.05
	},
	{
		hash: mp.game.joaat('p_cut_door_02'),
		x: 524.2,
		y: 3081.14,
		z: 41.16
	},
	{
		hash: mp.game.joaat('ap1_02_door_l'),
		x: -1041.933,
		y: -2748.167,
		z: 22.0308
	},
	{
		hash: mp.game.joaat('ap1_02_door_r'),
		x: -1044.841,
		y: -2746.489,
		z: 22.0308
	},
	{
		hash: mp.game.joaat('prop_damdoor_01'),
		x: 1385.258,
		y: -2079.949,
		z: 52.7638
	},
	{
		hash: mp.game.joaat('prop_sec_barrier_ld_02a'),
		x: -1210.957,
		y: -580.8765,
		z: 27.2373
	},
	{
		hash: mp.game.joaat('prop_ld_bankdoors_01'),
		x: 231.62,
		y: 216.23,
		z: 106.4
	},
	{
		hash: mp.game.joaat('hei_prop_hei_bankdoor_new'),
		x: 258.32,
		y: 203.84,
		z: 106.43
	},
	{
		hash: mp.game.joaat('v_ilev_bk_door'),
		x: 266.36,
		y: 217.57,
		z: 110.43
	},
	{
		hash: mp.game.joaat('prop_bhhotel_door_l'),
		x: -1223.35,
		y: -172.41,
		z: 39.98
	},
	{
		hash: mp.game.joaat('prop_bhhotel_door_r'),
		x: -1220.93,
		y: -173.68,
		z: 39.98
	},
	{
		hash: mp.game.joaat('v_ilev_gc_door04'),
		x: 16.1279,
		y: -1114.605,
		z: 29.9469
	},
	{
		hash: mp.game.joaat('v_ilev_gc_door03'),
		x: 18.572,
		y: -1115.495,
		z: 29.9469
	},
	{
		hash: mp.game.joaat('v_ilev_gc_door01'),
		x: 6.8179,
		y: -1098.209,
		z: 29.9469
	},
	{
		hash: mp.game.joaat('prop_lrggate_01c_l'),
		x: -1107.01,
		y: 289.38,
		z: 64.76
	},
	{
		hash: mp.game.joaat('prop_lrggate_01c_r'),
		x: -1101.62,
		y: 290.36,
		z: 64.76
	},
	{
		hash: mp.game.joaat('v_ilev_cbankcountdoor01'),
		x: -108.91,
		y: 6469.11,
		z: 31.91
	},
	{
		hash: mp.game.joaat('prop_fnclink_03gate5'),
		x: -182.91,
		y: 6168.37,
		z: 32.14
	},
	{
		hash: mp.game.joaat('prop_ch3_04_door_02'),
		x: 2508.43,
		y: -336.63,
		z: 115.76
	},
	{
		hash: mp.game.joaat('prop_gate_tep_01_l'),
		x: -721.35,
		y: 91.01,
		z: 56.68
	},
	{
		hash: mp.game.joaat('prop_gate_tep_01_r'),
		x: -728.84,
		y: 88.64,
		z: 56.68
	},
	{
		hash: mp.game.joaat('prop_artgallery_02_dr'),
		x: -2287.62,
		y: 363.9,
		z: 174.93
	},
	{
		hash: mp.game.joaat('prop_artgallery_02_dl'),
		x: -2289.78,
		y: 362.91,
		z: 174.93
	},
	{
		hash: mp.game.joaat('prop_fnclink_07gate1'),
		x: 1803.94,
		y: 3929.01,
		z: 33.72
	},
	{
		hash: mp.game.joaat('prop_lrggate_02_ld'),
		x: -844.051,
		y: 155.9619,
		z: 66.03221
	}
	];

	mp.av.doors.unlockAllDoors = function() {
		doors.forEach((door) => {
			mp.game.object.doorControl(door.hash, door.x, door.y, door.z, false, 0, 50, 0);
			mp.game.object.setStateOfClosestDoorOfType(door.hash, door.x, door.y, door.z, true, 0, false);

		});
	}

	mp.av.doors.lockAllDoors = function() {
		doors.forEach((door) => {
			mp.game.object.doorControl(door.hash, door.x, door.y, door.z, true, 0, 50, 0);
		});
	}
})();
