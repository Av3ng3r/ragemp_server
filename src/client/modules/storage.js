mp.events.add("client_loaded", () => {
	mp.av.storage = {};

	const localStorage = {};

	mp.av.storage.get = (name, dfl) => {
		let rv = localStorage[name];
		if (undefined === rv) rv = dfl;
		return rv;
	};

	mp.av.storage.set = (name, val, persistent) => {
		if (undefined === persistent) persistent = false;
		if (persistent) mp.events.callRemote("saveData", name, JSON.stringify(val));
		localStorage[name] = val;
	};

	mp.events.add("successfulLogin", (data) => {
		if (data) {
			const parsed = JSON.parse(data);
			for (const entry in parsed) {
				mp.av.storage.set(entry, parsed[entry], false);
			}
		}
		mp.events.call("loggedin");
	});
});
