mp.events.add("client_loaded", () => {
	mp.av.vehicles = {};
	mp.av.vehicles.weapons = {
		"VEHICLE_WEAPON_AKULA_BARRAGE": 2282558706,
		"VEHICLE_WEAPON_AKULA_MINIGUN": 431576697,
		"VEHICLE_WEAPON_AKULA_MISSILE": 2092838988,
		"VEHICLE_WEAPON_AKULA_TURRET_DUAL": 476907586,
		"VEHICLE_WEAPON_AKULA_TURRET_SINGLE": 3048454573,
		"VEHICLE_WEAPON_APC_CANNON": 328167896,
		"VEHICLE_WEAPON_APC_MG": 190244068,
		"VEHICLE_WEAPON_APC_MISSILE": 1151689097,
		"VEHICLE_WEAPON_ARDENT_MG": 3293463361,
		"VEHICLE_WEAPON_AVENGER_CANNON": 2556895291,
		"VEHICLE_WEAPON_BARRAGE_REAR_GL": 2756453005,
		"VEHICLE_WEAPON_BARRAGE_REAR_MG": 1200179045,
		"VEHICLE_WEAPON_BARRAGE_REAR_MINIGUN": 525623141,
		"VEHICLE_WEAPON_BARRAGE_TOP_MG": 4148791700,
		"VEHICLE_WEAPON_BARRAGE_TOP_MINIGUN": 1000258817,
		"VEHICLE_WEAPON_BOMBUSHKA_CANNON": 3628350041,
		"VEHICLE_WEAPON_BOMBUSHKA_DUALMG": 741027160,
		"VEHICLE_WEAPON_CANNON_BLAZER": 3959029566,
		"VEHICLE_WEAPON_CARACARA_MG": 1817275304,
		"VEHICLE_WEAPON_CARACARA_MINIGUN": 1338760315,
		"VEHICLE_WEAPON_CHERNO_MISSILE": 2722615358,
		"VEHICLE_WEAPON_COMET_MG": 3936892403,
		"VEHICLE_WEAPON_DELUXO_MG": 2600428406,
		"VEHICLE_WEAPON_DELUXO_MISSILE": 3036244276,
		"VEHICLE_WEAPON_DOGFIGHTER_MG": 1595421922,
		"VEHICLE_WEAPON_DOGFIGHTER_MISSILE": 3393648765,
		"VEHICLE_WEAPON_DUNE_GRENADELAUNCHER": 2700898573,
		"VEHICLE_WEAPON_DUNE_MG": 3507816399,
		"VEHICLE_WEAPON_DUNE_MINIGUN": 1416047217,
		"VEHICLE_WEAPON_ENEMY_LASER": 1566990507,
		"VEHICLE_WEAPON_HACKER_MISSILE": 1987049393,
		"VEHICLE_WEAPON_HACKER_MISSILE_HOMING": 2011877270,
		"VEHICLE_WEAPON_HALFTRACK_DUALMG": 1331922171,
		"VEHICLE_WEAPON_HALFTRACK_QUADMG": 1226518132,
		"VEHICLE_WEAPON_HAVOK_MINIGUN": 855547631,
		"VEHICLE_WEAPON_HUNTER_BARRAGE": 785467445,
		"VEHICLE_WEAPON_HUNTER_CANNON": 704686874,
		"VEHICLE_WEAPON_HUNTER_MG": 1119518887,
		"VEHICLE_WEAPON_HUNTER_MISSILE": 153396725,
		"VEHICLE_WEAPON_INSURGENT_MINIGUN": 2861067768,
		"VEHICLE_WEAPON_KHANJALI_CANNON": 507170720,
		"VEHICLE_WEAPON_KHANJALI_CANNON_HEAVY": 2206953837,
		"VEHICLE_WEAPON_KHANJALI_GL": 394659298,
		"VEHICLE_WEAPON_KHANJALI_MG": 711953949,
		"VEHICLE_WEAPON_MENACER_MG": 3754621092,
		"VEHICLE_WEAPON_MICROLIGHT_MG": 3303022956,
		"VEHICLE_WEAPON_MOBILEOPS_CANNON": 3846072740,
		"VEHICLE_WEAPON_MOGUL_DUALNOSE": 3857952303,
		"VEHICLE_WEAPON_MOGUL_DUALTURRET": 3123149825,
		"VEHICLE_WEAPON_MOGUL_NOSE": 4128808778,
		"VEHICLE_WEAPON_MOGUL_TURRET": 3808236382,
		"VEHICLE_WEAPON_MULE4_MG": 2220197671,
		"VEHICLE_WEAPON_MULE4_MISSILE": 1198717003,
		"VEHICLE_WEAPON_MULE4_TURRET_GL": 3708963429,
		"VEHICLE_WEAPON_NIGHTSHARK_MG": 2786772340,
		"VEHICLE_WEAPON_NOSE_TURRET_VALKYRIE": 1097917585,
		"VEHICLE_WEAPON_OPPRESSOR_MG": 3643944669,
		"VEHICLE_WEAPON_OPPRESSOR_MISSILE": 2344076862,
		"VEHICLE_WEAPON_OPPRESSOR2_CANNON": 3595383913,
		"VEHICLE_WEAPON_OPPRESSOR2_MG": 3796180438,
		"VEHICLE_WEAPON_OPPRESSOR2_MISSILE": 1966766321,
		"VEHICLE_WEAPON_PLANE_ROCKET": 3473446624,
		"VEHICLE_WEAPON_PLAYER_BUZZARD": 1186503822,
		"VEHICLE_WEAPON_PLAYER_LAZER": 3800181289,
		"VEHICLE_WEAPON_PLAYER_SAVAGE": 1638077257,
		"VEHICLE_WEAPON_POUNDER2_BARRAGE": 2456521956,
		"VEHICLE_WEAPON_POUNDER2_GL": 2467888918,
		"VEHICLE_WEAPON_POUNDER2_MINI": 2263283790,
		"VEHICLE_WEAPON_POUNDER2_MISSILE": 162065050,
		"VEHICLE_WEAPON_RADAR": 3530961278,
		"VEHICLE_WEAPON_REVOLTER_MG": 3177079402,
		"VEHICLE_WEAPON_ROGUE_CANNON": 3878337474,
		"VEHICLE_WEAPON_ROGUE_MG": 158495693,
		"VEHICLE_WEAPON_ROGUE_MISSILE": 1820910717,
		"VEHICLE_WEAPON_RUINER_BULLET": 50118905,
		"VEHICLE_WEAPON_RUINER_ROCKET": 84788907,
		"VEHICLE_WEAPON_SAVESTRA_MG": 3946965070,
		"VEHICLE_WEAPON_SCRAMJET_MG": 231629074,
		"VEHICLE_WEAPON_SCRAMJET_MISSILE": 3169388763,
		"VEHICLE_WEAPON_SEABREEZE_MG": 1371067624,
		"VEHICLE_WEAPON_SEARCHLIGHT": 3450622333,
		"VEHICLE_WEAPON_SPACE_ROCKET": 4171469727,
		"VEHICLE_WEAPON_SPEEDO4_MG": 3355244860,
		"VEHICLE_WEAPON_SPEEDO4_TURRET_MG": 3595964737,
		"VEHICLE_WEAPON_SPEEDO4_TURRET_MINI": 2667462330,
		"VEHICLE_WEAPON_STRIKEFORCE_BARRAGE": 968648323,
		"VEHICLE_WEAPON_STRIKEFORCE_CANNON": 955522731,
		"VEHICLE_WEAPON_STRIKEFORCE_MISSILE": 519052682,
		"VEHICLE_WEAPON_SUBCAR_MG": 1176362416,
		"VEHICLE_WEAPON_SUBCAR_MISSILE": 3565779982,
		"VEHICLE_WEAPON_SUBCAR_TORPEDO": 3884172218,
		"VEHICLE_WEAPON_TAMPA_DUALMINIGUN": 1744687076,
		"VEHICLE_WEAPON_TAMPA_FIXEDMINIGUN": 3670375085,
		"VEHICLE_WEAPON_TAMPA_MISSILE": 2656583842,
		"VEHICLE_WEAPON_TAMPA_MORTAR": 1015268368,
		"VEHICLE_WEAPON_TANK": 1945616459,
		"VEHICLE_WEAPON_TECHNICAL_MINIGUN": 3683206664,
		"VEHICLE_WEAPON_THRUSTER_MG": 1697521053,
		"VEHICLE_WEAPON_THRUSTER_MISSILE": 1177935125,
		"VEHICLE_WEAPON_TRAILER_DUALAA": 2156678476,
		"VEHICLE_WEAPON_TRAILER_MISSILE": 341154295,
		"VEHICLE_WEAPON_TRAILER_QUADMG": 1192341548,
		"VEHICLE_WEAPON_TULA_DUALMG": 2966510603,
		"VEHICLE_WEAPON_TULA_MG": 1217122433,
		"VEHICLE_WEAPON_TULA_MINIGUN": 376489128,
		"VEHICLE_WEAPON_TULA_NOSEMG": 1100844565,
		"VEHICLE_WEAPON_TURRET_BOXVILLE": 3041872152,
		"VEHICLE_WEAPON_TURRET_INSURGENT": 1155224728,
		"VEHICLE_WEAPON_TURRET_LIMO": 729375873,
		"VEHICLE_WEAPON_TURRET_TECHNICAL": 2144528907,
		"VEHICLE_WEAPON_TURRET_VALKYRIE": 2756787765,
		"VEHICLE_WEAPON_VIGILANTE_MG": 4094131943,
		"VEHICLE_WEAPON_VIGILANTE_MISSILE": 1347266149,
		"VEHICLE_WEAPON_VISERIS_MG": 2275421702,
		"VEHICLE_WEAPON_VOLATOL_DUALMG": 1150790720,
		"VEHICLE_WEAPON_WATER_CANNON": 1741783703
	};

	mp.av.vehicles.disableVehicleWeapon = (toggle, vehicleHash, playerHash) => {
		Object.values(mp.av.vehicles.weapons).forEach(hash => {
			try {
				mp.game.vehicle.disableVehicleWeapon(toggle, hash, vehicleHash, playerHash);
			} catch (ex) {}
		});
	};

	const vehiclesConfig = [
		{
			hash: "6774487",
			name: "chimera",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "11251904",
			name: "carbonrs",
			type: "Motorcycle"
		},
		{
			hash: "16646064",
			name: "virgo3",
			type: "Muscle"
		},
		{
			hash: "37348240",
			name: "hotknife",
			type: "Muscle"
		},
		{
			hash: "48339065",
			name: "tiptruck",
			type: "Truck"
		},
		{
			hash: "55628203",
			name: "faggio2",
			type: "Motorcycle"
		},
		{
			hash: "65402552",
			name: "youga",
			type: "Van"
		},
		{
			hash: "75131841",
			name: "glendale",
			type: "Sedan",
			update: "Hipster"
		},
		{
			hash: "80636076",
			name: "dominator",
			type: "Muscle"
		},
		{
			hash: "86520421",
			name: "bf400",
			type: "Motorcycle",
			update: "Cunning Stunts"
		},
		{
			hash: "92612664",
			name: "kalahari",
			type: "Offroad",
			update: "Beach Bum"
		},
		{
			hash: "101905590",
			name: "trophytruck",
			type: "Offroad",
			update: "Cunning Stunts"
		},
		{
			hash: "108773431",
			name: "coquette",
			type: "Sport"
		},
		{
			hash: "117401876",
			name: "btype",
			type: "SportsClassic"
		},
		{
			hash: "121658888",
			name: "boxville3",
			type: "Truck"
		},
		{
			hash: "142944341",
			name: "baller2",
			type: "SUV"
		},
		{
			hash: "159274291",
			name: "ardent",
			tw: true,
			type: "SportsClassic",
			update: "GUNRUNNING"
		},
		{
			hash: "165154707",
			name: "miljet",
			type: "Plane",
			update: "Flight School"
		},
		{
			hash: "177270108",
			name: "phantom3",
			type: "Commercial",
			update: "GUNRUNNING"
		},
		{
			hash: "184361638",
			name: "freightcar",
			type: "Train"
		},
		{
			hash: "196747873",
			name: "elegy",
			type: "Sport",
			update: "Import/Export"
		},
		{
			hash: "223240013",
			name: "cheetah2",
			type: "SportsClassic",
			update: "GUNRUNNING"
		},
		{
			hash: "223258115",
			name: "sabregt2",
			type: "Muscle"
		},
		{
			hash: "231083307",
			name: "speeder",
			type: "Boat",
			update: "Beach Bum"
		},
		{
			hash: "234062309",
			name: "reaper",
			type: "Super",
			update: "Finance&Felony"
		},
		{
			hash: "237764926",
			name: "buffalo3",
			type: "Sport"
		},
		{
			hash: "240201337",
			name: "freightcont2",
			type: "Train"
		},
		{
			hash: "272929391",
			name: "tempesta",
			type: "Super",
			update: "Import/Export"
		},
		{
			hash: "276773164",
			name: "dinghy2",
			type: "Boat"
		},
		{
			hash: "290013743",
			name: "tropic",
			type: "Boat"
		},
		{
			hash: "296357396",
			name: "gburrito2",
			type: "Van"
		},
		{
			hash: "301427732",
			name: "hexer",
			type: "Motorcycle"
		},
		{
			hash: "321739290",
			name: "crusader",
			type: "Emergency"
		},
		{
			hash: "330661258",
			name: "cogcabrio",
			type: "Coupe"
		},
		{
			hash: "338562499",
			name: "vacca",
			type: "Super"
		},
		{
			hash: "349315417",
			name: "gauntlet2",
			type: "Muscle"
		},
		{
			hash: "349605904",
			name: "chino",
			type: "Muscle",
			update: "IGG2"
		},
		{
			hash: "353883353",
			name: "polmav",
			type: "Helicopter"
		},
		{
			hash: "356391690",
			name: "proptrailer",
			type: "Trailer"
		},
		{
			hash: "368211810",
			name: "cargoplane",
			type: "Plane"
		},
		{
			hash: "384071873",
			name: "surano",
			type: "Sport"
		},
		{
			hash: "387748548",
			name: "hauler2",
			type: "Commercial",
			update: "GUNRUNNING"
		},
		{
			hash: "390201602",
			name: "cliffhanger",
			type: "Motorcycle",
			update: "Cunning Stunts"
		},
		{
			hash: "390902130",
			name: "raketrailer",
			type: "Trailer"
		},
		{
			hash: "400514754",
			name: "squalo",
			type: "Boat"
		},
		{
			hash: "408192225",
			name: "turismor",
			type: "Super"
		},
		{
			hash: "410882957",
			name: "kuruma2",
			type: "Sport",
			world: 1
		},
		{
			hash: "418536135",
			name: "infernus",
			type: "Super"
		},
		{
			hash: "433954513",
			name: "nightshark",
			tw: true,
			type: "Offroad",
			update: "GUNRUNNING"
		},
		{
			hash: "437538602",
			name: "speeder2",
			type: "Boat"
		},
		{
			hash: "444171386",
			name: "boxville4",
			type: "Truck"
		},
		{
			hash: "444583674",
			name: "handler",
			type: "Truck"
		},
		{
			hash: "448402357",
			name: "cruiser",
			type: "Bicycle"
		},
		{
			hash: "456714581",
			name: "policet",
			type: "Emergency"
		},
		{
			hash: "464687292",
			name: "tornado",
			type: "SportsClassic"
		},
		{
			hash: "469291905",
			name: "lguard",
			type: "Emergency"
		},
		{
			hash: "470404958",
			name: "baller5",
			type: "SUV"
		},
		{
			hash: "475220373",
			name: "mixer2",
			type: "Truck"
		},
		{
			hash: "482197771",
			name: "lynx",
			type: "Sport",
			update: "Cunning Stunts"
		},
		{
			hash: "486987393",
			name: "huntley",
			type: "SUV",
			update: "High Life"
		},
		{
			hash: "499169875",
			name: "fusilade",
			type: "Sport"
		},
		{
			hash: "509498602",
			name: "dinghy3",
			type: "Boat"
		},
		{
			hash: "516990260",
			name: "utillitruck",
			type: "Service"
		},
		{
			hash: "523724515",
			name: "voodoo2",
			type: "Muscle"
		},
		{
			hash: "524108981",
			name: "boattrailer",
			type: "Trailer"
		},
		{
			hash: "525509695",
			name: "moonbeam",
			type: "Van",
			update: "Lowrider CC"
		},
		{
			hash: "534258863",
			name: "dune2",
			type: "Offroad"
		},
		{
			hash: "544021352",
			name: "khamelion",
			type: "Sport"
		},
		{
			hash: "562680400",
			name: "apc",
			tw: true,
			type: "Military",
			update: "GUNRUNNING"
		},
		{
			hash: "569305213",
			name: "packer",
			type: "Truck"
		},
		{
			hash: "586013744",
			name: "tankercar",
			type: "Train"
		},
		{
			hash: "621481054",
			name: "luxor",
			type: "Plane"
		},
		{
			hash: "627094268",
			name: "romero",
			type: "Sedan"
		},
		{
			hash: "627535535",
			name: "fcr",
			type: "Motorcycle",
			update: "Import/Export"
		},
		{
			hash: "630371791",
			name: "barracks3",
			type: "Emergency"
		},
		{
			hash: "633712403",
			name: "banshee2",
			type: "Super"
		},
		{
			hash: "634118882",
			name: "baller4",
			type: "SUV"
		},
		{
			hash: "640818791",
			name: "lectro",
			type: "Motorcycle",
			update: "Heists"
		},
		{
			hash: "642617954",
			name: "freightgrain",
			type: "Train"
		},
		{
			hash: "666166960",
			name: "baller6",
			type: "SUV"
		},
		{
			hash: "682434785",
			world: 1,
			name: "boxville5",
			type: "Truck"
		},
		{
			hash: "683047626",
			name: "contender",
			type: "Pickup",
			update: "Cunning Stunts"
		},
		{
			hash: "699456151",
			name: "surfer",
			type: "Van"
		},
		{
			hash: "704435172",
			name: "cog552",
			type: "Sedan"
		},
		{
			hash: "710198397",
			name: "supervolito",
			type: "Helicopter",
			update: "Exec/Criminals"
		},
		{
			hash: "712162987",
			name: "trailersmall",
			type: "Trailer"
		},
		{
			hash: "719660200",
			name: "ruston",
			type: "Sport",
			update: "CS:SVC"
		},
		{
			hash: "723973206",
			name: "dukes",
			type: "Muscle",
			update: "Next Gen"
		},
		{
			hash: "728614474",
			name: "speedo2",
			type: "Van"
		},
		{
			hash: "729783779",
			name: "slamvan",
			type: "Muscle",
			update: "Festive2"
		},
		{
			hash: "734217681",
			name: "sadler2",
			type: "Pickup"
		},
		{
			hash: "736902334",
			name: "buffalo2",
			type: "Sport"
		},
		{
			hash: "741090084",
			name: "gargoyle",
			type: "Motorcycle",
			update: "Cunning Stunts"
		},
		{
			hash: "741586030",
			name: "pranger",
			type: "Emergency"
		},
		{
			hash: "743478836",
			name: "sovereign",
			type: "Motorcycle",
			update: "IndependenceDay"
		},
		{
			hash: "744705981",
			name: "frogger",
			type: "Helicopter"
		},
		{
			hash: "745926877",
			name: "buzzard2",
			type: "Helicopter"
		},
		{
			hash: "758895617",
			name: "ztype",
			type: "SportsClassic"
		},
		{
			hash: "767087018",
			name: "alpha",
			type: "Sport",
			update: "Business"
		},
		{
			hash: "771711535",
			name: "submersible",
			type: "Boat"
		},
		{
			hash: "777714999",
			name: "ruiner3",
			type: "Sport"
		},
		{
			hash: "782665360",
			name: "rhino",
			tw: true,
			type: "Military"
		},
		{
			hash: "784565758",
			name: "coquette3",
			type: "SportsClassic"
		},
		{
			hash: "788045382",
			name: "sanchez",
			type: "Motorcycle"
		},
		{
			hash: "788747387",
			name: "buzzard",
			world: 1,
			type: "Helicopter"
		},
		{
			hash: "819197656",
			name: "sheava",
			type: "Super"
		},
		{
			hash: "822018448",
			name: "defiler",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "833469436",
			name: "slamvan2",
			type: "Muscle"
		},
		{
			hash: "837858166",
			name: "annihilator",
			world: 1,
			type: "Helicopter"
		},
		{
			hash: "841808271",
			name: "rhapsody",
			type: "Compact",
			update: "Hipster"
		},
		{
			hash: "850565707",
			name: "bjxl",
			type: "SUV"
		},
		{
			hash: "850991848",
			name: "biff",
			type: "Truck"
		},
		{
			hash: "861409633",
			name: "jetmax",
			type: "Boat"
		},
		{
			hash: "867467158",
			name: "dinghy4",
			type: "Boat"
		},
		{
			hash: "868868440",
			name: "metrotrain",
			type: "Train"
		},
		{
			hash: "873639469",
			name: "sentinel2",
			type: "Coupe"
		},
		{
			hash: "884422927",
			name: "habanero",
			type: "SUV"
		},
		{
			hash: "884483972",
			name: "oppressor",
			world: 1,
			type: "Motorcycle",
			update: "GUNRUNNING"
		},
		{
			hash: "886934177",
			name: "intruder",
			type: "Sedan"
		},
		{
			hash: "887537515",
			name: "utillitruck2",
			type: "Service"
		},
		{
			hash: "893081117",
			name: "burrito4",
			type: "Van"
		},
		{
			hash: "904750859",
			name: "mule",
			type: "Truck"
		},
		{
			hash: "906642318",
			name: "cog55",
			type: "Sedan"
		},
		{
			hash: "908897389",
			name: "toro2",
			type: "Boat"
		},
		{
			hash: "914654722",
			name: "mesa",
			type: "SUV"
		},
		{
			hash: "917809321",
			name: "xa21",
			type: "Super",
			update: "GUNRUNNING"
		},
		{
			hash: "920453016",
			name: "freightcont1",
			type: "Train"
		},
		{
			hash: "941494461",
			name: "ruiner2",
			premium: true,
			sub: "ruiner3",
			type: "Sport",
			update: "Import/Export"
		},
		{
			hash: "941800958",
			name: "casco",
			type: "SportsClassic",
			update: "Heists"
		},
		{
			hash: "943752001",
			name: "pony2",
			type: "Van"
		},
		{
			hash: "970356638",
			name: "duster",
			type: "Plane"
		},
		{
			hash: "970385471",
			name: "hydra",
			world: 1,
			type: "Plane",
			update: "Heists"
		},
		{
			hash: "970598228",
			name: "sultan",
			type: "Sport"
		},
		{
			hash: "972671128",
			name: "tampa",
			type: "Muscle",
			update: "#VALUE!"
		},
		{
			hash: "989294410",
			name: "voltic2",
			type: "Super",
			tw: true,
			premium: true,
			sub: "voltic",
			update: "Import/Export"
		},
		{
			hash: "989381445",
			name: "sandking2",
			type: "Offroad"
		},
		{
			hash: "1011753235",
			name: "coquette2",
			type: "SportsClassic"
		},
		{
			hash: "1019737494",
			name: "graintrailer",
			type: "Trailer"
		},
		{
			hash: "1026149675",
			name: "youga2",
			type: "Van"
		},
		{
			hash: "1030400667",
			name: "freight",
			type: "Train"
		},
		{
			hash: "1032823388",
			name: "ninef",
			type: "Sport"
		},
		{
			hash: "1033245328",
			name: "dinghy",
			type: "Boat"
		},
		{
			hash: "1034187331",
			name: "nero",
			type: "Super",
			update: "Import/Export"
		},
		{
			hash: "1036591958",
			name: "nokota",
			world: 1,
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "1039032026",
			name: "blista2",
			type: "Sport"
		},
		{
			hash: "1043222410",
			name: "tula",
			world: 1,
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "1044954915",
			name: "skylift",
			type: "Helicopter"
		},
		{
			hash: "1051415893",
			name: "jb700",
			type: "SportsClassic"
		},
		{
			hash: "1058115860",
			name: "jet",
			type: "Plane"
		},
		{
			hash: "1069929536",
			name: "bobcatxl",
			type: "Pickup"
		},
		{
			hash: "1070967343",
			name: "toro",
			type: "Boat",
			update: "IGG2"
		},
		{
			hash: "1074326203",
			name: "barracks2",
			type: "Emergency"
		},
		{
			hash: "1074745671",
			name: "specter2",
			type: "Sport",
			update: "Import/Export"
		},
		{
			hash: "1075432268",
			name: "swift2",
			type: "Helicopter"
		},
		{
			hash: "1077420264",
			name: "velum2",
			type: "Plane"
		},
		{
			hash: "1078682497",
			name: "pigalle",
			type: "SportsClassic",
			update: "Hipster"
		},
		{
			hash: "1093792632",
			name: "nero2",
			type: "Super",
			update: "Import/Export"
		},
		{
			hash: "1102544804",
			name: "verlierer2",
			type: "Sport",
			update: "Exec/Criminals"
		},
		{
			hash: "1119641113",
			name: "slamvan3",
			type: "Muscle"
		},
		{
			hash: "1123216662",
			name: "superd",
			type: "Sedan"
		},
		{
			hash: "1126868326",
			name: "bfinjection",
			type: "Offroad"
		},
		{
			hash: "1127131465",
			name: "fbi",
			type: "Emergency"
		},
		{
			hash: "1127861609",
			name: "tribike",
			type: "Bicycle"
		},
		{
			hash: "1131912276",
			name: "bmx",
			type: "Bicycle"
		},
		{
			hash: "1132262048",
			name: "burrito5",
			type: "Van"
		},
		{
			hash: "1147287684",
			name: "caddy",
			type: "Service"
		},
		{
			hash: "1162065741",
			name: "rumpo",
			type: "Van"
		},
		{
			hash: "1171614426",
			name: "ambulance",
			type: "Emergency"
		},
		{
			hash: "1177543287",
			name: "dubsta",
			type: "SUV"
		},
		{
			hash: "1180875963",
			name: "technical2",
			tw: true,
			type: "Offroad",
			update: "Import/Export"
		},
		{
			hash: "1203490606",
			name: "xls",
			type: "SUV",
			update: "Finance&Felony"
		},
		{
			hash: "1221512915",
			name: "seminole",
			type: "SUV"
		},
		{
			hash: "1233534620",
			name: "marshall",
			type: "Offroad",
			update: "Next Gen"
		},
		{
			hash: "1234311532",
			name: "gp1",
			type: "Super",
			update: "CS:SVC"
		},
		{
			hash: "1265391242",
			name: "hakuchou",
			type: "Motorcycle",
			update: "Last Team Std"
		},
		{
			hash: "1269098716",
			name: "landstalker",
			type: "SUV"
		},
		{
			hash: "1274868363",
			name: "bestiagts",
			type: "Sport",
			update: "Finance&Felony"
		},
		{
			hash: "1283517198",
			name: "airbus",
			type: "Service"
		},
		{
			hash: "1337041428",
			name: "serrano",
			type: "SUV"
		},
		{
			hash: "1341619767",
			name: "vestra",
			type: "Plane",
			update: "Business"
		},
		{
			hash: "1348744438",
			name: "oracle",
			type: "Sedan"
		},
		{
			hash: "1349725314",
			name: "sentinel",
			type: "Coupe"
		},
		{
			hash: "1353720154",
			name: "flatbed",
			type: "Truck"
		},
		{
			hash: "1356124575",
			name: "technical3",
			tw: true,
			type: "Offroad",
			update: "GUNRUNNING"
		},
		{
			hash: "1373123368",
			name: "warrener",
			type: "Sedan",
			update: "Hipster"
		},
		{
			hash: "1392481335",
			name: "cyclone",
			type: "Super",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "1394036463",
			name: "cargobob3",
			type: "Helicopter"
		},
		{
			hash: "1426219628",
			name: "fmj",
			type: "Super",
			update: "Finance&Felony"
		},
		{
			hash: "1445631933",
			name: "tractor3",
			type: "Service"
		},
		{
			hash: "1448677353",
			name: "tropic2",
			type: "Boat"
		},
		{
			hash: "1475773103",
			name: "rumpo3",
			type: "Van"
		},
		{
			hash: "1488164764",
			name: "paradise",
			type: "Van",
			update: "Beach Bum"
		},
		{
			hash: "1489967196",
			name: "schafter4",
			type: "Sedan"
		},
		{
			hash: "1491277511",
			name: "sanctus",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "1491375716",
			name: "forklift",
			type: "Service"
		},
		{
			hash: "1502869817",
			name: "trailerlarge",
			tw: true,
			world: 1,
			type: "Utility",
			update: "GUNRUNNING"
		},
		{
			hash: "1504306544",
			name: "torero",
			type: "SportsClassic",
			update: "GUNRUNNING"
		},
		{
			hash: "1507916787",
			name: "picador",
			type: "Muscle"
		},
		{
			hash: "1518533038",
			name: "hauler",
			type: "Truck"
		},
		{
			hash: "1531094468",
			name: "tornado2",
			type: "SportsClassic"
		},
		{
			hash: "1543134283",
			name: "valkyrie2",
			world: 1,
			type: "Helicopter"
		},
		{
			hash: "1545842587",
			name: "stinger",
			type: "SportsClassic"
		},
		{
			hash: "1549126457",
			name: "brioso",
			type: "Compact"
		},
		{
			hash: "1560980623",
			name: "airtug",
			type: "Service"
		},
		{
			hash: "1565978651",
			name: "molotok",
			world: 1,
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "1581459400",
			name: "windsor",
			type: "Coupe",
			update: "IGG1"
		},
		{
			hash: "1621617168",
			name: "cargobob2",
			type: "Helicopter"
		},
		{
			hash: "1641462412",
			name: "tractor",
			type: "Service"
		},
		{
			hash: "1645267888",
			name: "rancherxl",
			type: "Offroad"
		},
		{
			hash: "1663218586",
			name: "t20",
			type: "Super",
			update: "IGG2"
		},
		{
			hash: "1672195559",
			name: "akuma",
			type: "Motorcycle"
		},
		{
			hash: "1682114128",
			name: "dilettante2",
			type: "Compact"
		},
		{
			hash: "1723137093",
			name: "stratum",
			type: "Sedan"
		},
		{
			hash: "1737773231",
			name: "rapidgt2",
			type: "Sport"
		},
		{
			hash: "1739845664",
			name: "bison3",
			type: "Pickup"
		},
		{
			hash: "1747439474",
			name: "stockade",
			type: "Truck"
		},
		{
			hash: "1753414259",
			name: "enduro",
			type: "Motorcycle",
			update: "Heists"
		},
		{
			hash: "1762279763",
			name: "tornado3",
			type: "SportsClassic"
		},
		{
			hash: "1770332643",
			name: "dloader",
			type: "Offroad"
		},
		{
			hash: "1777363799",
			name: "washington",
			type: "Sedan"
		},
		{
			hash: "1783355638",
			name: "mower",
			type: "Service"
		},
		{
			hash: "1784254509",
			name: "tr3",
			type: "Trailer"
		},
		{
			hash: "1790834270",
			name: "diablous2",
			type: "Motorcycle"
		},
		{
			hash: "1824333165",
			name: "besra",
			type: "Plane",
			update: "Flight School"
		},
		{
			hash: "1830407356",
			name: "peyote",
			type: "SportsClassic"
		},
		{
			hash: "1836027715",
			name: "thrust",
			type: "Motorcycle",
			update: "High Life"
		},
		{
			hash: "1841130506",
			name: "retinue",
			type: "SportsClassic",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "1873600305",
			name: "ratbike",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "1876516712",
			name: "camper",
			type: "Van"
		},
		{
			hash: "1878062887",
			name: "baller3",
			type: "SUV"
		},
		{
			hash: "1886268224",
			name: "specter",
			type: "Sport",
			update: "Import/Export"
		},
		{
			hash: "1886712733",
			name: "bulldozer",
			type: "Service"
		},
		{
			hash: "1887331236",
			name: "tropos",
			type: "Sport"
		},
		{
			hash: "1896491931",
			name: "moonbeam2",
			type: "Van"
		},
		{
			hash: "1897744184",
			name: "dune3",
			tw: true,
			type: "Offroad",
			update: "GUNRUNNING"
		},
		{
			hash: "1909141499",
			name: "fugitive",
			type: "Sedan"
		},
		{
			hash: "1912215274",
			name: "police3",
			type: "Emergency"
		},
		{
			hash: "1917016601",
			name: "trash",
			type: "Service"
		},
		{
			hash: "1922255844",
			name: "schafter6",
			type: "Sedan"
		},
		{
			hash: "1922257928",
			name: "sheriff2",
			type: "Emergency"
		},
		{
			hash: "1923400478",
			name: "stalion",
			type: "Muscle",
			update: "Next Gen"
		},
		{
			hash: "1933662059",
			name: "rancherxl2",
			type: "Offroad"
		},
		{
			hash: "1938952078",
			name: "firetruk",
			type: "Emergency"
		},
		{
			hash: "1939284556",
			name: "vagner",
			world: 1,
			type: "Super",
			update: "GUNRUNNING"
		},
		{
			hash: "1941029835",
			name: "tourbus",
			type: "Service"
		},
		{
			hash: "1949211328",
			name: "frogger2",
			type: "Helicopter"
		},
		{
			hash: "1951180813",
			name: "taco",
			type: "Van"
		},
		{
			hash: "1956216962",
			name: "tanker2",
			type: "Trailer"
		},
		{
			hash: "1981688531",
			name: "titan",
			type: "Plane"
		},
		{
			hash: "1987142870",
			name: "osiris",
			type: "Super",
			update: "IGG1"
		},
		{
			hash: "2006142190",
			name: "daemon",
			type: "Motorcycle"
		},
		{
			hash: "2006667053",
			name: "voodoo",
			type: "Muscle"
		},
		{
			hash: "2006918058",
			name: "cavalcade",
			type: "SUV"
		},
		{
			hash: "2016027501",
			name: "trailerlogs",
			type: "Trailer"
		},
		{
			hash: "2016857647",
			name: "futo",
			type: "Sport"
		},
		{
			hash: "2025593404",
			name: "cargobob4",
			type: "Helicopter"
		},
		{
			hash: "2035069708",
			name: "esskey",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "2046537925",
			name: "police",
			type: "Emergency"
		},
		{
			hash: "2049897956",
			name: "rapidgt3",
			type: "SportsClassic",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "2053223216",
			name: "benson",
			type: "Truck"
		},
		{
			hash: "2067820283",
			name: "tyrus",
			type: "Super",
			update: "Cunning Stunts"
		},
		{
			hash: "2068293287",
			name: "lurcher",
			type: "SportsClassic"
		},
		{
			hash: "2071877360",
			name: "insurgent2",
			tw: true,
			type: "Offroad"
		},
		{
			hash: "2072156101",
			name: "bison2",
			type: "Pickup"
		},
		{
			hash: "2072687711",
			name: "carbonizzare",
			type: "Sport"
		},
		{
			hash: "2078290630",
			name: "tr2",
			type: "Trailer"
		},
		{
			hash: "2091594960",
			name: "tr4",
			type: "Trailer"
		},
		{
			hash: "2112052861",
			name: "pounder",
			type: "Truck"
		},
		{
			hash: "2123327359",
			name: "prototipo",
			type: "Super"
		},
		{
			hash: "2132890591",
			name: "utillitruck3",
			type: "Service"
		},
		{
			hash: "2134395284",
			name: "faggion",
			type: "Motorcycle"
		},
		{
			hash: "2136773105",
			name: "rocoto",
			type: "SUV"
		},
		{
			hash: "2154536131",
			name: "bagger",
			type: "Motorcycle"
		},
		{
			hash: "2154757102",
			name: "docktrailer",
			type: "Trailer"
		},
		{
			hash: "2157618379",
			name: "phantom",
			type: "Truck"
		},
		{
			hash: "2164484578",
			name: "dump",
			type: "Truck"
		},
		{
			hash: "2166734073",
			name: "blazer",
			type: "Offroad"
		},
		{
			hash: "2170765704",
			name: "manana",
			type: "SportsClassic"
		},
		{
			hash: "2172210288",
			name: "stunt",
			type: "Plane"
		},
		{
			hash: "2175389151",
			name: "faction",
			type: "Coupe",
			update: "Lowrider CC"
		},
		{
			hash: "2179174271",
			name: "avarus",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "2186977100",
			name: "guardian",
			type: "Offroad",
			update: "Heists"
		},
		{
			hash: "2191146052",
			name: "rallytruck",
			type: "Truck"
		},
		{
			hash: "2194326579",
			name: "tug",
			type: "Boat",
			update: "Finance&Felony"
		},
		{
			hash: "2196019706",
			name: "stingergt",
			type: "SportsClassic"
		},
		{
			hash: "2198148358",
			name: "technical",
			tw: true,
			type: "Offroad",
			update: "Heists"
		},
		{
			hash: "2199527893",
			name: "phoenix",
			type: "Muscle"
		},
		{
			hash: "2218488798",
			name: "tractor2",
			type: "Service"
		},
		{
			hash: "2222034228",
			name: "coach",
			type: "Service"
		},
		{
			hash: "2230595153",
			name: "mesa3",
			type: "Offroad"
		},
		{
			hash: "2236089197",
			name: "trailers3",
			type: "Trailer"
		},
		{
			hash: "2242229361",
			name: "mule3",
			type: "Truck"
		},
		{
			hash: "2246633323",
			name: "italigtb",
			type: "Super",
			update: "Import/Export"
		},
		{
			hash: "2249373259",
			name: "rebel2",
			type: "Offroad"
		},
		{
			hash: "2254540506",
			name: "primo2",
			type: "Sedan"
		},
		{
			hash: "2255212070",
			name: "faction3",
			type: "Muscle"
		},
		{
			hash: "2261744861",
			name: "tornado4",
			type: "SportsClassic"
		},
		{
			hash: "2264796000",
			name: "cognoscenti",
			type: "Sedan",
			update: "Exec/Criminals"
		},
		{
			hash: "2272483501",
			name: "comet3",
			type: "Sport",
			update: "Import/Export"
		},
		{
			hash: "2287941233",
			name: "pbus",
			type: "Emergency"
		},
		{
			hash: "2299640309",
			name: "feltzer2",
			type: "Sport"
		},
		{
			hash: "2307837162",
			name: "boxville",
			type: "Truck"
		},
		{
			hash: "2310691317",
			name: "havok",
			type: "Helicopter",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "2321795001",
			name: "police4",
			type: "Emergency"
		},
		{
			hash: "2333339779",
			name: "stretch",
			type: "Sedan"
		},
		{
			hash: "2351681756",
			name: "nightshade",
			type: "Muscle",
			update: "Exec/Criminals"
		},
		{
			hash: "2360515092",
			name: "rapidgt",
			type: "Sport"
		},
		{
			hash: "2364918497",
			name: "windsor2",
			type: "Coupe"
		},
		{
			hash: "2370534026",
			name: "insurgent3",
			tw: true,
			type: "Offroad",
			update: "GUNRUNNING"
		},
		{
			hash: "2382949506",
			name: "wastelander",
			type: "Offroad",
			update: "Import/Export"
		},
		{
			hash: "2391954683",
			name: "asterope",
			type: "Sedan"
		},
		{
			hash: "2400073108",
			name: "surge",
			type: "Sedan"
		},
		{
			hash: "2411098011",
			name: "premier",
			type: "Sedan"
		},
		{
			hash: "2411965148",
			name: "emperor2",
			type: "Sedan"
		},
		{
			hash: "2413121211",
			name: "trailersmall2",
			tw: true,
			type: "Military",
			update: "GUNRUNNING"
		},
		{
			hash: "2434067162",
			name: "insurgent",
			tw: true,
			type: "Offroad"
		},
		{
			hash: "2449479409",
			name: "volatus",
			type: "Helicopter",
			update: "Finance&Felony"
		},
		{
			hash: "2452219115",
			name: "faggio",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "2465164804",
			name: "pfister811",
			type: "Super",
			update: "Finance&Felony"
		},
		{
			hash: "2485144969",
			name: "asea",
			type: "Sedan"
		},
		{
			hash: "2487343317",
			name: "asea2",
			type: "Sedan"
		},
		{
			hash: "2494797253",
			name: "gauntlet",
			type: "Muscle"
		},
		{
			hash: "2497353967",
			name: "tornado5",
			type: "Muscle"
		},
		{
			hash: "2504420315",
			name: "faction2",
			type: "Coupe"
		},
		{
			hash: "2515846680",
			name: "policeold2",
			type: "Emergency"
		},
		{
			hash: "2518351607",
			name: "rumpo2",
			type: "Van"
		},
		{
			hash: "2519238556",
			name: "granger",
			type: "SUV"
		},
		{
			hash: "2524324030",
			name: "tvtrailer",
			type: "Trailer"
		},
		{
			hash: "2531412055",
			name: "microlight",
			world: 1,
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "2536829930",
			name: "penetrator",
			type: "Super",
			update: "Import/Export"
		},
		{
			hash: "2537130571",
			name: "seven70",
			type: "Sport",
			update: "Finance&Felony"
		},
		{
			hash: "2548391185",
			name: "mammatus",
			type: "Plane"
		},
		{
			hash: "2549763894",
			name: "gburrito",
			type: "Van"
		},
		{
			hash: "2551651283",
			name: "burrito3",
			type: "Van"
		},
		{
			hash: "2589662668",
			name: "rubble",
			type: "Truck"
		},
		{
			hash: "2594093022",
			name: "starling",
			world: 1,
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "2594165727",
			name: "scrap",
			type: "Truck"
		},
		{
			hash: "2598821281",
			name: "bullet",
			type: "Super"
		},
		{
			hash: "2609945748",
			name: "sabregt",
			type: "Muscle"
		},
		{
			hash: "2611638396",
			name: "sheriff",
			type: "Emergency"
		},
		{
			hash: "2621610858",
			name: "velum",
			type: "Plane"
		},
		{
			hash: "2623428164",
			name: "supervolito2",
			type: "Helicopter"
		},
		{
			hash: "2623969160",
			name: "double",
			type: "Motorcycle"
		},
		{
			hash: "2633113103",
			name: "dune",
			type: "Offroad"
		},
		{
			hash: "2634021974",
			name: "mamba",
			type: "Sport",
			update: "Exec/Criminals"
		},
		{
			hash: "2634305738",
			name: "maverick",
			type: "Helicopter"
		},
		{
			hash: "2643899483",
			name: "radi",
			type: "SUV"
		},
		{
			hash: "2645431192",
			name: "phantom2",
			type: "Truck",
			update: "Import/Export"
		},
		{
			hash: "2647026068",
			name: "fbi2",
			type: "Emergency"
		},
		{
			hash: "2657817814",
			name: "armytrailer2",
			type: "Trailer"
		},
		{
			hash: "2667966721",
			name: "police2",
			type: "Emergency"
		},
		{
			hash: "2672523198",
			name: "voltic",
			type: "Super"
		},
		{
			hash: "2688780135",
			name: "nightblade",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "2694714877",
			name: "valkyrie",
			world: 1,
			type: "Helicopter",
			update: "Heists"
		},
		{
			hash: "2704629607",
			name: "blazer5",
			tw: true,
			type: "Offroad",
			update: "Import/Export"
		},
		{
			hash: "2715434129",
			name: "trailers2",
			type: "Trailer"
		},
		{
			hash: "2728226064",
			name: "feltzer3",
			type: "SportsClassic"
		},
		{
			hash: "2736567667",
			name: "tornado6",
			type: "Muscle"
		},
		{
			hash: "2751205197",
			name: "gresley",
			type: "SUV"
		},
		{
			hash: "2758042359",
			name: "policeold1",
			type: "Emergency"
		},
		{
			hash: "2771347558",
			name: "alphaz1",
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "2771538552",
			name: "manchez",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "2809443750",
			name: "schafter3",
			type: "Sedan"
		},
		{
			hash: "2815302597",
			name: "brawler",
			type: "Offroad",
			update: "IGG2"
		},
		{
			hash: "2817386317",
			name: "stanier",
			type: "Sedan"
		},
		{
			hash: "2818520053",
			name: "armytrailer",
			type: "Trailer"
		},
		{
			hash: "2833484545",
			name: "ninef2",
			type: "Sport"
		},
		{
			hash: "2841686334",
			name: "sanchez2",
			type: "Motorcycle"
		},
		{
			hash: "2844316578",
			name: "prairie",
			type: "Compact"
		},
		{
			hash: "2859047862",
			name: "bodhi2",
			type: "Offroad"
		},
		{
			hash: "2889029532",
			name: "infernus2",
			type: "Sport"
		},
		{
			hash: "2890830793",
			name: "daemon2",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "2891838741",
			name: "zentorno",
			type: "Super",
			update: "High Life"
		},
		{
			hash: "2908775872",
			name: "pyro",
			world: 1,
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "2922118804",
			name: "kuruma",
			type: "Sport",
			update: "Heists"
		},
		{
			hash: "2933279331",
			name: "chino2",
			type: "Muscle"
		},
		{
			hash: "2941886209",
			name: "vindicator",
			type: "Motorcycle",
			update: "IGG2"
		},
		{
			hash: "2942498482",
			name: "trflat",
			type: "Trailer"
		},
		{
			hash: "2948279460",
			name: "burrito",
			type: "Van"
		},
		{
			hash: "2971866336",
			name: "towtruck",
			type: "Truck"
		},
		{
			hash: "2983726598",
			name: "surfer2",
			type: "Van"
		},
		{
			hash: "2983812512",
			name: "cheetah",
			type: "Super"
		},
		{
			hash: "2997294755",
			name: "jester",
			type: "Sport",
			update: "Business"
		},
		{
			hash: "2999939664",
			name: "nimbus",
			type: "Plane",
			update: "Finance&Felony"
		},
		{
			hash: "3003014393",
			name: "entityxf",
			type: "Super"
		},
		{
			hash: "3005245074",
			name: "ingot",
			type: "Sedan"
		},
		{
			hash: "3005788552",
			name: "faggio3",
			type: "Motorcycle"
		},
		{
			hash: "3013282534",
			name: "lazer",
			world: 1,
			type: "Plane"
		},
		{
			hash: "3025077634",
			name: "blazer3",
			type: "Offroad"
		},
		{
			hash: "3039269212",
			name: "trash2",
			type: "Service"
		},
		{
			hash: "3039514899",
			name: "schafter2",
			type: "Sedan"
		},
		{
			hash: "3052358707",
			name: "vigilante",
			tw: true,
			type: "Military",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "3053254478",
			name: "emperor3",
			type: "Sedan"
		},
		{
			hash: "3057713523",
			name: "dubsta3",
			type: "Offroad"
		},
		{
			hash: "3061159916",
			name: "tribike2",
			type: "Bicycle"
		},
		{
			hash: "3062131285",
			name: "le7b",
			type: "Super"
		},
		{
			hash: "3078201489",
			name: "adder",
			type: "Super"
		},
		{
			hash: "3080461301",
			name: "shamal",
			type: "Plane"
		},
		{
			hash: "3080673438",
			name: "luxor2",
			type: "Plane"
		},
		{
			hash: "3084515313",
			name: "tampa3",
			premium: true,
			sub: "tampa2",
			type: "Muscle",
			update: "GUNRUNNING"
		},
		{
			hash: "3087195462",
			name: "rebel",
			type: "Offroad"
		},
		{
			hash: "3087536137",
			name: "armytanker",
			type: "Trailer"
		},
		{
			hash: "3089165662",
			name: "blade",
			type: "Muscle",
			update: "I'mNotaHipster"
		},
		{
			hash: "3089277354",
			name: "riot",
			type: "Emergency"
		},
		{
			hash: "3101863448",
			name: "zion2",
			type: "Coupe"
		},
		{
			hash: "3105951696",
			name: "sandking",
			type: "Offroad"
		},
		{
			hash: "3117103977",
			name: "issi2",
			type: "Compact"
		},
		{
			hash: "3144368207",
			name: "primo",
			type: "Sedan"
		},
		{
			hash: "3157435195",
			name: "fq2",
			type: "SUV"
		},
		{
			hash: "3164157193",
			name: "dilettante",
			type: "Compact"
		},
		{
			hash: "3168702960",
			name: "minivan2",
			type: "Muscle"
		},
		{
			hash: "3172678083",
			name: "zion",
			type: "Coupe"
		},
		{
			hash: "3188613414",
			name: "jester2",
			type: "Sport"
		},
		{
			hash: "3194418602",
			name: "trailers4",
			type: "Utility",
			update: "GUNRUNNING"
		},
		{
			hash: "3196165219",
			name: "rentalbus",
			type: "Service"
		},
		{
			hash: "3205927392",
			name: "furoregt",
			type: "Sport",
			update: "Last Team Std"
		},
		{
			hash: "3223586949",
			name: "tampa2",
			type: "Muscle"
		},
		{
			hash: "3228633070",
			name: "submersible2",
			type: "Boat"
		},
		{
			hash: "3244501995",
			name: "mule2",
			type: "Truck"
		},
		{
			hash: "3249425686",
			name: "comet2",
			type: "Sport"
		},
		{
			hash: "3251507587",
			name: "marquis",
			type: "Boat"
		},
		{
			hash: "3253274834",
			name: "banshee",
			type: "Sport"
		},
		{
			hash: "3264692260",
			name: "seashark",
			type: "Boat"
		},
		{
			hash: "3264692260",
			name: "landshark",
			type: "Motorcycle"
		},
		{
			hash: "3281516360",
			name: "buccaneer2",
			type: "Muscle"
		},
		{
			hash: "3285698347",
			name: "zombiea",
			type: "Motorcycle"
		},
		{
			hash: "3286105550",
			name: "tailgater",
			type: "Sedan"
		},
		{
			hash: "3287439187",
			name: "howard",
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "3288047904",
			name: "cutter",
			type: "Truck"
		},
		{
			hash: "3296789504",
			name: "visione",
			type: "Super",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "3312836369",
			name: "turismo2",
			type: "Super",
			update: "CS:SVC"
		},
		{
			hash: "3319621991",
			name: "rogue",
			type: "Plane",
			world: 1,
			update: "SMUGGLERS RUN"
		},
		{
			hash: "3334677549",
			name: "cablecar",
			type: "Service"
		},
		{
			hash: "3338918751",
			name: "taxi",
			type: "Sedan"
		},
		{
			hash: "3347205726",
			name: "tiptruck2",
			type: "Truck"
		},
		{
			hash: "3379262425",
			name: "dominator2",
			type: "Muscle"
		},
		{
			hash: "3385765638",
			name: "pcj",
			type: "Motorcycle"
		},
		{
			hash: "3387490166",
			name: "burrito2",
			type: "Van"
		},
		{
			hash: "3393804037",
			name: "dodo",
			type: "Plane",
			update: "Next Gen"
		},
		{
			hash: "3395457658",
			name: "virgo2",
			type: "Muscle"
		},
		{
			hash: "3401388520",
			name: "ruffian",
			type: "Motorcycle"
		},
		{
			hash: "3403504941",
			name: "bati2",
			type: "Motorcycle"
		},
		{
			hash: "3406724313",
			name: "schafter5",
			type: "Sedan"
		},
		{
			hash: "3410276810",
			name: "docktug",
			type: "Truck"
		},
		{
			hash: "3417488910",
			name: "trailers",
			type: "Trailer"
		},
		{
			hash: "3448987385",
			name: "ripley",
			type: "Service"
		},
		{
			hash: "3449006043",
			name: "monster",
			type: "Offroad"
		},
		{
			hash: "3458454463",
			name: "fixter",
			type: "Bicycle"
		},
		{
			hash: "3463132580",
			name: "btype2",
			type: "Muscle"
		},
		{
			hash: "3467805257",
			name: "dune4",
			type: "Offroad",
			update: "Import/Export"
		},
		{
			hash: "3469130167",
			name: "vigero",
			type: "Muscle"
		},
		{
			hash: "3471458123",
			name: "barracks",
			type: "Emergency"
		},
		{
			hash: "3484649228",
			name: "speedo",
			type: "Van"
		},
		{
			hash: "3486135912",
			name: "baller",
			type: "SUV"
		},
		{
			hash: "3486509883",
			name: "patriot",
			type: "SUV"
		},
		{
			hash: "3505073125",
			name: "cavalcade2",
			type: "SUV"
		},
		{
			hash: "3510150843",
			name: "mixer",
			type: "Truck"
		},
		{
			hash: "3517691494",
			name: "freighttrailer",
			type: "Trailer"
		},
		{
			hash: "3517794615",
			name: "omnis",
			type: "Sport",
			update: "Cunning Stunts"
		},
		{
			hash: "3525819835",
			name: "caddy3",
			type: "Utility",
			update: "GUNRUNNING"
		},
		{
			hash: "3537231886",
			name: "fcr2",
			type: "Motorcycle",
			update: "Import/Export"
		},
		{
			hash: "3545667823",
			name: "mogul",
			type: "Plane",
			world: 1,
			update: "SMUGGLERS RUN"
		},
		{
			hash: "3546958660",
			name: "mesa2",
			type: "SUV"
		},
		{
			hash: "3548084598",
			name: "schwarzer",
			type: "Sport"
		},
		{
			hash: "3564062519",
			name: "tanker",
			type: "Trailer"
		},
		{
			hash: "3581397346",
			name: "bus",
			type: "Service"
		},
		{
			hash: "3609690755",
			name: "emperor",
			type: "Sedan"
		},
		{
			hash: "3612755468",
			name: "buccaneer",
			type: "Muscle"
		},
		{
			hash: "3620039993",
			name: "raptor",
			type: "Sport",
			update: "Bikers"
		},
		{
			hash: "3627815886",
			name: "ratloader",
			type: "Pickup"
		},
		{
			hash: "3631668194",
			name: "trophytruck2",
			type: "Offroad"
		},
		{
			hash: "3650256867",
			name: "cuban800",
			type: "Plane"
		},
		{
			hash: "3660088182",
			name: "nemesis",
			type: "Motorcycle"
		},
		{
			hash: "3663206819",
			name: "massacro2",
			type: "Sport"
		},
		{
			hash: "3670438162",
			name: "jackal",
			type: "Coupe"
		},
		{
			hash: "3676349299",
			name: "wolfsbane",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "3678636260",
			name: "seashark2",
			type: "Boat"
		},
		{
			hash: "3681241380",
			name: "blimp2",
			type: "Helicopter"
		},
		{
			hash: "3685342204",
			name: "vortex",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "3690124666",
			name: "cognoscenti2",
			type: "Sedan"
		},
		{
			hash: "3692679425",
			name: "btype3",
			type: "SportsClassic"
		},
		{
			hash: "3695398481",
			name: "sadler",
			type: "Pickup"
		},
		{
			hash: "3703315515",
			name: "blista3",
			type: "Sport"
		},
		{
			hash: "3703357000",
			name: "f620",
			type: "Coupe"
		},
		{
			hash: "3705788919",
			name: "ratloader2",
			type: "Pickup"
		},
		{
			hash: "3724934023",
			name: "zombieb",
			type: "Motorcycle"
		},
		{
			hash: "3728579874",
			name: "elegy2",
			type: "Sport"
		},
		{
			hash: "3757070668",
			name: "caddy2",
			type: "Service"
		},
		{
			hash: "3783366066",
			name: "oracle2",
			type: "Sedan"
		},
		{
			hash: "3796912450",
			name: "virgo",
			type: "Muscle",
			update: "IGG1"
		},
		{
			hash: "3806844075",
			name: "predator",
			type: "Boat"
		},
		{
			hash: "3812247419",
			name: "italigtb2",
			type: "Super",
			update: "Import/Export"
		},
		{
			hash: "3852654278",
			name: "towtruck2",
			type: "Truck"
		},
		{
			hash: "3854198872",
			name: "blazer4",
			type: "Offroad"
		},
		{
			hash: "3861591579",
			name: "monroe",
			type: "SportsClassic"
		},
		{
			hash: "3862958888",
			name: "xls2",
			type: "SUV"
		},
		{
			hash: "3863274624",
			name: "panto",
			type: "Compact",
			update: "Hipster"
		},
		{
			hash: "3889340782",
			name: "shotaro",
			type: "Motorcycle",
			update: "Bikers"
		},
		{
			hash: "3893323758",
			name: "stalion2",
			type: "Muscle"
		},
		{
			hash: "3894672200",
			name: "tribike3",
			type: "Bicycle"
		},
		{
			hash: "3895125590",
			name: "baletrailer",
			type: "Trailer"
		},
		{
			hash: "3900892662",
			name: "dubsta2",
			type: "SUV"
		},
		{
			hash: "3902291871",
			name: "seabreeze",
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "3903372712",
			name: "felon",
			type: "Coupe"
		},
		{
			hash: "3917501776",
			name: "penumbra",
			type: "Sport"
		},
		{
			hash: "3945366167",
			name: "bifta",
			type: "Offroad",
			update: "Beach Bum"
		},
		{
			hash: "3950024287",
			name: "blista",
			type: "Compact"
		},
		{
			hash: "3955379698",
			name: "swift",
			type: "Helicopter",
			update: "Flight School"
		},
		{
			hash: "3968823444",
			name: "dukes2",
			type: "Muscle"
		},
		{
			hash: "3982671785",
			name: "dune5",
			type: "Offroad",
			update: "Import/Export"
		},
		{
			hash: "3983945033",
			name: "seashark3",
			type: "Boat"
		},
		{
			hash: "3984502180",
			name: "minivan",
			type: "Van"
		},
		{
			hash: "3989239879",
			name: "brickade",
			type: "Service",
			update: "Finance&Felony"
		},
		{
			hash: "3990165190",
			name: "buffalo",
			type: "Sport"
		},
		{
			hash: "3999278268",
			name: "sultanrs",
			type: "Super",
			update: "Drop Zone"
		},
		{
			hash: "4012021193",
			name: "suntrap",
			type: "Boat"
		},
		{
			hash: "4039289119",
			name: "hakuchou2",
			type: "Motorcycle"
		},
		{
			hash: "4055125828",
			name: "diablous",
			type: "Motorcycle",
			update: "Import/Export"
		},
		{
			hash: "4061868990",
			name: "boxville2",
			type: "Truck"
		},
		{
			hash: "4067225593",
			name: "ruiner",
			type: "Muscle"
		},
		{
			hash: "4080511798",
			name: "stockade3",
			type: "Truck"
		},
		{
			hash: "4108429845",
			name: "scorcher",
			type: "Bicycle"
		},
		{
			hash: "4135840458",
			name: "innovation",
			type: "Motorcycle",
			update: "Last Team Std"
		},
		{
			hash: "4143991942",
			name: "blimp",
			type: "Helicopter"
		},
		{
			hash: "4152024626",
			name: "massacro",
			type: "Sport",
			update: "High Life"
		},
		{
			hash: "4154065143",
			name: "vader",
			type: "Motorcycle"
		},
		{
			hash: "4174679674",
			name: "journey",
			type: "Van"
		},
		{
			hash: "4175309224",
			name: "pony",
			type: "Van"
		},
		{
			hash: "4180339789",
			name: "limo2",
			tw: true,
			type: "Sedan"
		},
		{
			hash: "4180675781",
			name: "bati",
			type: "Motorcycle"
		},
		{
			hash: "4205676014",
			name: "felon2",
			type: "Coupe"
		},
		{
			hash: "4212341271",
			name: "savage",
			type: "Helicopter",
			world: 1,
			update: "Heists"
		},
		{
			hash: "4244420235",
			name: "cargobob",
			type: "Helicopter"
		},
		{
			hash: "4246935337",
			name: "blazer2",
			type: "Offroad"
		},
		{
			hash: "4252008158",
			name: "hunter",
			world: 1,
			type: "Helicopter",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "4260343491",
			name: "policeb",
			type: "Emergency"
		},
		{
			hash: "4262088844",
			name: "bombushka",
			type: "Plane",
			update: "SMUGGLERS RUN"
		},
		{
			hash: "4262731174",
			name: "halftrack",
			tw: true,
			type: "Military",
			update: "GUNRUNNING"
		},
		{
			hash: "4278019151",
			name: "bison",
			type: "Pickup"
		},
		{
			hash: "4280472072",
			name: "regina",
			type: "Sedan"
		},
		{
			hash: "4289813342",
			name: "exemplar",
			type: "Coupe"
		}
	];

	mp.av.vehicles.getVehicleNames = function() {
		const v = vehiclesConfig.map(entry => entry.name);
		return v;
	}

	mp.av.vehicles.getVehicleNameFromModel = function(model) {
		const v = vehiclesConfig.find((entry) => entry.hash == model) || false;
		if (v) return v.name;
		return false;
	};

	mp.av.vehicles.getVehicleFromName = function(name) {
		return vehiclesConfig.find((entry) => entry.name == name) || false;
	}

	mp.av.vehicles.getVehiclesForType = function(type, tw) {
		let world = 0;
		try {
			world = mp.players.local.dimension;
		} catch (ex) {
			world = 0;
		}

		tw = tw || false;
		return vehiclesConfig.filter(entry => {
			if (world !== 1) {
				if (entry.world) return false;
				if (!tw && entry.tw === true) return false;
			}
			return entry.type == type;
		}).map(entry => entry.name).sort();
	}

	mp.av.vehicles.isTankwarsVehicle = function(name) {
		return vehiclesConfig.find((entry) => entry.name == name).tw;
	}

	mp.av.vehicles.getVehicleTypeForHash = function(hash) {
		return vehiclesConfig.find((entry) => entry.hash == hash).type;
	}

	mp.av.vehicles.getVehicleTypes = function() {
		if (mp.players.local.restrictedVehicles) {
			return mp.players.local.restrictedVehicles;
		}
		return [...new Set(vehiclesConfig.map(entry => entry.type))].sort();
	}

	mp.av.vehicles.getAllVehicleMods = function(vehicle) {
		let rv = [];
		for (let modType = 0; modType < 62; modType++) {
			if (modType === 23) continue;
			const slotName = vehicle.getModSlotName(modType);
			let s = "item " + modType;
			if (slotName) {
				s = mp.game.ui.getLabelText(slotName);
				if (s === "NULL") s = slotName;
			} else {
				s = getModNameFromUnknownValue(modType);
			}

			const entry = {
				"slotName": s,
				"modType": modType,
				"mods": mp.av.vehicles.getModValues(vehicle, modType)
			}
			if (entry["mods"].length > 0) rv.push(entry);
		}
		return rv;
	}

	mp.av.vehicles.getModValues = function(vehicle, modType) {
		const rv = [];
		let numMods = vehicle.getNumMods(modType);
		if (numMods === 0) switch (modType) {
			case 22: numMods = 1; break;
			case 46: numMods = 3; break;
		}
		for (let modValue = 0; modValue < numMods; modValue++) {
			const modName = vehicle.getModTextLabel(modType, modValue);
			let s = "item " + modValue;
			if (modName) {
				s = mp.game.ui.getLabelText(modName);
				if (s === "NULL") s = modName;
			} else {
				s = getModNameFromUnknownValue(modType, modValue);
			}
			rv.push({ "modName": s, "modValue": modValue });
		}
		return rv;
	}

	// TODO: finish this
	const unknownMods = {
		11: {
			name: "Engine",
			values: [ "Upgrade 1", "Upgrade 2", "Upgrade 3", "Upgrade 4" ]
		},
		12: {
			name: "Brakes",
			values: [ "Street", "Sports", "Racing" ]
		},
		13: {
			name: "Transmission",
			values: [ "Street", "Sports", "Racing" ]
		},
		14: {
			name: "Horn",
			values: [ "Truck", "Police", "Clown", "Musical 1", "Musical 2", "Musical 3",
					"Musical 4", "Musical 5", "Sad trombone", "Classical 1", "Classical 2",
					"Classical 3", "Classical 4", "Classical 5", "Classical 6", "Classical 7",
					"DO", "RAE", "MI", "FA", "SO", "LA", "TI", "DO ", "Jazz 1", "Jazz 2",
					"Jazz 3", "Jazz loop", "Star 1", "Star 2", "Star 3", "Star 4",
					"Classical loop 1", "Classical 8", "Classical loop 2" ]
		},
		15: {
			name: "Suspension",
			values: [ "Lower", "Street", "Sports", "Racing" ]
		},
		16: {
			name: "Armour",
			values: [ "20%", "40%", "60%", "80%", "100%" ]
		},
		22: {
			name: "Xenon Lights",
			values: [ "on" ]
		},
		25: {
			name: "Plate holder",
			values: []
		},
		27: {
			name: "Trim design",
			values: []
		},
		28: {
			name: "Ornaments",
			values: []
		},
		29: {
			name: "Dashboard",
			values: []
		},
		30: {
			name: "Instruments",
			values: []
		},
		31: {
			name: "Door Speaker",
			values: []
		},
		32: {
			name: "Seats",
			values: []
		},
		33: {
			name: "Steering wheel",
			values: []
		},
		34: {
			name: "Shift lever",
			values: []
		},
		35: {
			name: "Plaques",
			values: []
		},
		36: {
			name: "Speakers",
			values: []
		},
		37: {
			name: "Trunk",
			values: []
		},
		38: {
			name: "Hydrolics",
			values: []
		},
		39: {
			name: "Engine block",
			values: []
		},
		40: {
			name: "Air filter",
			values: []
		},
		41: {
			name: "Struts",
			values: []
		},
		42: {
			name: "Arch Cover",
			values: []
		},
		43: {
			name: "Aerials",
			values: []
		},
		44: {
			name: "Trim",
			values: []
		},
		45: {
			name: "Tank",
			values: []
		},
		48: {
			name: "Livery",
			values: []
		}
	};

	function getModNameFromUnknownValue(modType, modValue)
	{
		let rv = "";
		if (undefined === modValue) {
			rv = "item " + modType;
			if (unknownMods[modType] !== undefined) {
				rv = unknownMods[modType].name;
			}
			return rv;
		}

		rv = "item " + modValue;
		const mod = unknownMods[modType];
		if (mod && mod.values[modValue]) {
			rv = mod.values[modValue];
		}
		return rv;
	}
});
