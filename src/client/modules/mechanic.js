mp.events.add("client_loaded", () => {
	mp.av.mechanic = {};

	mp.av.mechanic.currentSlot = 0;

	let lastRepairAt = 0;

	mp.av.mechanic.saveVehicle = (vehicle, slot) => {
		if (mp.players.local.getVariable("banished")) return;
		if (slot < 0) slot = mp.av.mechanic.currentSlot;
		if (vehicle) {
			const data = {
				model: vehicle.model,
				name: vehicle.getVariable("sname") || vehicle.getVariable("name"),
				mods: vehicle.getVariable("mods") || {}
			}
			mp.av.user.setData(`vehicle${slot}`, data);
			mp.av.mechanic.currentSlot = slot;
		}
	};

	mp.av.mechanic.restoreVehicle = (slot) => {
		if (mp.players.local.getVariable("banished")) return;
		if (slot < 0) slot = mp.av.mechanic.currentSlot;
		const data = mp.av.user.getData(`vehicle${slot}`, null);
		if (data) {
			const { name, model, mods } = data;
			mp.events.call("spawnVehicle", name, mods);
		}
		mp.av.mechanic.currentSlot = slot;
	};

	mp.av.mechanic.repairVehicle = (vehicle) => {
		if (mp.players.local.getVariable("banished")) return;
		const delay = mp.players.local.getVariable("wanted") ? 30000 : 5000;
		const now = Date.now();
		if (now - lastRepairAt < delay) {
			const seconds =  Math.floor((delay - (now - lastRepairAt)) / 1000);
			mp.game.graphics.notify(`Cannot repair vehicle for another ${seconds} seconds`);
			return;
		}
		lastRepairAt = now;

		if (vehicle) {
			vehicle.setDirtLevel(0.0);
			mp.events.callRemote("repairVehicle", vehicle.remoteId);
		}
	};

	mp.av.mechanic.toggleAutopilot = () => {
		if (mp.players.local.getVariable("banished")) return;
		const player = mp.players.local;
		const vehicle = player.vehicle;
		if (vehicle && player.isInVehicle(vehicle.handle, false)) {
			if (player.getIsTaskActive(151)) {
				mp.game.graphics.notify("disabling autopilot");
				player.clearTasks();
			} else {
				mp.game.graphics.notify("Enabling autopilot");
				player.taskVehicleDriveWander(vehicle.handle, 1000.0, 1074528293);
				player.setDriverAggressiveness(100.0);
				player.setDriveTaskCruiseSpeed(1000.0);
			}
		} else {
			mp.game.graphics.notify("not in a vehicle");
		}
	}
});
