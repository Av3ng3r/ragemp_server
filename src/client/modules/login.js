mp.events.add("client_loaded", () => {
	let loginScreen = null;

	mp.events.add("client_start", () => {
		try {
			loginScreen = mp.browsers.new("package://cef/login.html");

			mp.gui.chat.activate(false);
			mp.game.graphics.transitionToBlurred(1.0);

			// const data = mp.av.cef.get("login.html");
			// loginScreen.execute(`document.write(${data});`);
		} catch (ex) {
			mp.gui.chat.push("got ex: " + ex.message);
		}
	});

	mp.events.add("playerCommand", (command) => {
		const args = command.split(/[ ]+/);
		if (args[0] === "login") {
			mp.events.callRemote("requestLogin", args[1], args[2]);
		} else if (args[0] === "newuser") {
			if (args[1].length < 6 || args[1] > 16) {
				loginScreen.execute(`error('username must be between 6 and 16 characters');`);
			} else if (/[^A-Za-z0-9\-\ _]/.test(args[1])) {
				loginScreen.execute(`error("username must only contain A-Z, a-z, 0-9, -, _, and space");`);
			} else {
				mp.events.callRemote("requestNewUser", args[1], args[2]);
			}
		} else if (args[0] === "guest") {
			mp.events.callRemote("requestGuest");
		}
	});

	mp.events.add("successfulLogin", (data) => {
		mp.av.user.init(data);
		mp.gui.chat.activate(true);
		mp.game.graphics.transitionFromBlurred(1.0);
		mp.game.graphics.notify("login successful");

		loginScreen.destroy();
		loginScreen = null;

		mp.events.call("onClientPlayerLoggedIn", false);
		mp.gui.execute(`mp.invoke('focus', false);`);
	});

	let hideErrorTimeout = 0;
	mp.events.add("failedLogin", (reason) => {
		if (loginScreen) {
			switch (reason) {
				case "already":
					reason = "You are already logged in";
					break;
				case "wrong password":
					reason = "Username or password incorrect";
					break;
				default:
					break;
			}
			loginScreen.execute(`error('${reason}');`);
			if (hideErrorTimeout) clearTimeout(hideErrorTimeout);
			hideErrorTimeout = setTimeout(
				() => loginScreen && loginScreen.execute(`hideError();`),
				3000
			);
		}
	});
});
