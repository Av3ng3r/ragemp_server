mp.events.add("client_loaded", () => {
	mp.av.anticheat = {};

	// START SUPERJUMP 
	let startJumpingAt = 0;
	const superJumpCheck = (player) => {
		if (player.getIsTaskActive(421)) {
			let z = player.position.z;
			let delta = z - startJumpingAt;
			if (delta > 5) {
				player.clearTasksImmediately();
				player.setHighFallTask(9000, 10000, 4);
				setTimeout(() => {
					player.freezePosition(true);
					mp.game.fire.addExplosion(
						player.position.x, player.position.y, player.position.z,
						5, 10.0, true, false, 1.0
					);
					setTimeout(() => {
						player.freezePosition(false);
						player.applyForceToCenterOfMass(1, 0, 0, -1000000, false, false, true, false);
						player.setHealth(0);
						player.setArmour(0);
					}, 250);
					mp.events.callRemote("cheat", "superjump");
				}, 1000);
			}
		} else {
			startJumpingAt = player.position.z;
		}
	};
	// END SUPERJUMP

	// START WEAPON CHECK
	// note that this will check god mode and block weapons for god mode users.
	let lastNotify = 0;
	const weaponCheck = (player) => {
		const UNARMED = 2725352035;
		const a = player.weapon;
		const b = player.getVariable("godmode");
		if (b && a !== 2725352035) {
			if (Date.now() > lastNotify + 10000) {
				mp.game.graphics.notify("no weapons in god mode");
				lastNotify = Date.now();
			}
			mp.players.local.weapon = UNARMED;
		}
		return false
	};
	// END WEAPON CHECK

	// START GODMODE CHECK
	// this will allow users to be invincible, but they will be flagged
	const godModeCheck = (player) => {
		const a = mp.game.player.getInvincible();
		if (a && player.getVariable("godmode") !== true) {
			mp.events.callRemote("godmode", "on");
			mp.game.graphics.notify("Invicibility mode enabled");
		} else if (!a && player.getVariable("godmode") === true) {
			mp.events.callRemote("godmode", "off");
			mp.game.graphics.notify("Invicibility mode disabled");
		}
		mp.game.player.disableFiring(!!a);
	};
	// END GODMODE CHECK

	// START HEALTH CHECK
	let lastPlayerHealth = mp.players.local.getHealth();
	let healthCheckCounter = 5;
	const healthCheck = (player) => {
		if (lastPlayerHealth > 200) {
			player.setHealth(200);
			healthCheckCounter--;
		}

		lastPlayerHealth = player.getHealth();

		if (healthCheckCounter === 0) {
			mp.events.callRemote("am_banish", player.remoteId, "HEALTH CHEAT");
			player.setHealth(0);
			healthCheckCounter = 5;
		}
	};
	// END HEALTH CHECK

	// ensure that those who are invincible cannot do harm to others.
	const fixGodVehicles = () => {
		mp.players.forEachInStreamRange((player) => {
			if (player.id === mp.players.local.id) return;

			const handle = player.getVehicleIsIn(false);
			const vehicle = handle ? mp.vehicles.atHandle(handle) : null;
			if (player.getVariable("godmode")) {
				if (vehicle) try {
					if (vehicle.getPedInSeat(-1) === player.handle) {
						vehicle.setNoCollision(mp.players.local.handle, true);
						mp.av.vehicles.disableVehicleWeapon(true, vehicle.handle, player.handle);
					} else {
						vehicle.setCollision(true, true);
						mp.av.vehicles.disableVehicleWeapon(false, vehicle.handle, player.handle);
					}
				} catch (ex) {}
			} else if (vehicle) try {
				vehicle.setCollision(true, true);
			} catch (ex) {}
		});
	};

	const player = mp.players.local;
	setInterval(() => {
		superJumpCheck(player);
		weaponCheck(player);
		godModeCheck(player);
		healthCheck(player);
		fixGodVehicles();
	}, 100);
});
