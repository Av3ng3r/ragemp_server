mp.events.add("client_loaded", () => {
	mp.av.chat = {};
	try {

	mp.av.chat.currentPosition = "left";
	mp.av.chat.clear = () => {
		for (let i = 0; i < 50; i++) mp.gui.chat.push(" ");
	};
		//mp.gui.execute(`chatAPI.clear();`);

	mp.events.add("client_start", () => {
		mp.av.chat.position("left");
	});

	mp.events.add("loggedin", () => {
		const pos = mp.av.storage.get("chatpos", "bottom");
		mp.av.chat.position(pos);
	});

	const updateChatStyle = (css) => {
		mp.av.chat.currentPosition = css;
		const chat = mp.av.css[css];
		if (chat) {
			mp.gui.execute(
				`if (!document.chatstyle) {` +
					`document.chatstyle=document.createElement("style");` +
					`document.head.appendChild(document.chatstyle);` +
				`}` +
				`document.chatstyle.innerHTML="${chat}";`
			);
		}
	}

	mp.av.chat.position = (position, save) => {
		if (undefined === save) save = true;
		switch (position) {
			case "left":
			case T("left"):
				updateChatStyle("left.css");
				break;
			case "bottom":
			case T("bottom"):
				updateChatStyle("bottom.css");
				break;
			case "right":
			case T("right"):
				updateChatStyle("right.css");
				break;
		}
		mp.av.storage.set("chatpos", position, save);
	}

	} catch (ex) {
		mp.gui.chat.push("got ex: " + ex.message);
	}
});
