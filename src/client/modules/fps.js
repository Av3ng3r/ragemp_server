mp.events.add("client_loaded", () => {
	mp.av.fps = 1;

	let count = 0;
	let fps = 0;
	let visible = true;

	mp.av.fps.getCurrentFPS = () => fps;
	mp.av.fps.showFPS = () => { visible = true; }
	mp.av.fps.hideFPS = () => { visible = false; }

	mp.events.add("render", () => {
		count++;

		if (visible) {
			mp.game.graphics.drawText(`${fps}`, [0.015, 0.0], { 
				font: 2, 
				color: [255, 255, 255, 185], 
				scale: [0.5, 0.5],
				outline: true
			});
		}
	});

	setInterval(function() {
		mp.av.fps = fps = count; count = 0;
	}, 1000);
});
