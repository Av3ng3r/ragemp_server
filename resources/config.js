(function(){
	const config = {
		launchCommand: "%RAND%",
		server: "ragemp_server"
	};

	if (module) module.exports = config;
	if (exports) exports = config;
})();
