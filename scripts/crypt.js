#!/usr/bin/env node

global.mp = {};
const fs = require("fs");

require("../src/server/crypt/index");

let command = "decrypt";
let infile = "";
let key = "";

var args = process.argv.slice(1);
if (args.length < 2) {
    console.log("no arguments, nothing to do\n");
    process.exit(0);
}

for (let i = 0; i < args.length; i++) {
	switch (args[i]) {
		case "-key":
			i++; if (i === args.length) {
				console.log("missing key value");
				process.exit(1);
			}
			key = args[i];
			break;
		case "-decrypt":
			command = "decrypt";
			break;
		case "-encrypt":
			command = "encrypt";
			break;
		default:
			if (args[i][0] === "-") {
				console.log("unknown option " + args[i][0]);
				process.exit(1);
			}
			infile = args[i];
			break;
	}
}

if (infile && (infile[0] !== "." && infile[0] !== "/")) {
    infile = "./" + infile;
}

let data = fs.readFileSync(infile, 'utf-8');
switch (command) {
	case "encrypt":
		console.log(mp.crypt.encrypt(data, key));
		break;
	case "decrypt":
		console.log(mp.crypt.decrypt(data, key));
		break;
}

