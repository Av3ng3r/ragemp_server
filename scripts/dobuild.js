#!/usr/bin/env node

const args = process.argv.slice(1);
const debug = args.includes('-debug');

const config = require("../resources/config");
const child_process = require("child_process");
const rimraf = require("rimraf");
const fs = require("fs");
const glob = require("glob");
const browserify = require("browserify");

// copy a file to another
const cp = (src, dst) => {
	const contents = fs.readFileSync(src, "binary");
	fs.writeFileSync(dst, contents, "binary");
};

// recursively copy a directory
const cpdir = (src, dst) => {
	glob("**", { cwd: src }, (err, files) => {
		files.forEach((file) => {
			if (fs.lstatSync(`${src}/${file}`).isDirectory()) {
				fs.mkdirSync(`${dst}/${file}`);
			} else {
				cp(`${src}/${file}`, `${dst}/${file}`);
			}
		});
	});
};

// create all the needed directories
const createDirectories = (then) => {
	try {
		console.log("Creating directories");
		rimraf.sync("dist");
		fs.mkdirSync("dist");
		fs.mkdirSync("dist/packages");
		fs.mkdirSync("dist/packages/ragemp_server");
		fs.mkdirSync("dist/packages/ragemp_server/bootstrap");
		fs.mkdirSync("dist/packages/ragemp_server/cef");
		fs.mkdirSync("dist/client_packages");
		fs.mkdirSync("dist/client_packages/cef");
		fs.mkdirSync("dist/client_packages/language");
	} catch (ex) {
		console.log("building directories got ex: ", ex.message);
		process.exit(0);
	}

	if (then) then();
};

// create the temporary bootstrap contents
const clientModules = (then) => {
	console.log("Creating client modules");
	cpdir("src/client/modules", "dist/packages/ragemp_server/bootstrap");

	// bundle the scaleform messages pacakges
	let b = browserify();
	b.add("src/client/scaleform_messages/index.js");
	b.bundle((err, buf) => {
		fs.writeFileSync("dist/packages/ragemp_server/bootstrap/scaleform.js", buf.toString());
		// bundle the nativeui code
		b = browserify();
		b.add("src/client/nativeui");
		b.bundle((err, buf) => {
			fs.writeFileSync("dist/packages/ragemp_server/bootstrap/nativeui.js", buf.toString());
			// bundle the bison code
			b = browserify();
			b.add("src/client/index.js");
			b.bundle((err, buf) => {
				fs.writeFileSync("dist/packages/ragemp_server/bootstrap/index.js", buf.toString());
				if (then) then();
			});
		});
	});
};

// build the distributable client bundle
const obfuscateClientFiles = (files) => {
	let blob = fs.readFileSync("dist/packages/ragemp_server/bootstrap/index.js", "utf-8");
	files.forEach(file => {
		if (file !== "_init_.js" && file !== "index.js") {
			blob += "\n";
			blob += fs.readFileSync(`dist/packages/ragemp_server/bootstrap/${file}`, "utf-8");
		}
	});
	blob += "\n";
	blob += fs.readFileSync(`dist/packages/ragemp_server/bootstrap/_init_.js`, "utf-8");

	if (debug) return blob;

	console.log("Obfuscating client modules");
	try {
	const obfuscator = require("javascript-obfuscator");
	const res = obfuscator.obfuscate(blob, {
		compact: false,
		controlFlowFlattening: true
	});
	return res.getObfuscatedCode();
	} catch (ex) {
		console.log("got ex: ", ex);
		fs.writeFileSync(`/tmp/blob.js`, blob);
		throw(ex);
	}
};

// build the client files list. Only the cef list is used
const createClientModulesJSON = (then) => {
	console.log("Creating client indexes");
	glob("**/*", { cwd: "dist/packages/ragemp_server/cef" }, (err, f) => {
		f = f.filter(i=>!fs.lstatSync(`dist/packages/ragemp_server/cef/${i}`).isDirectory());
		let blob = "mp.av.css = {\n";
		f.forEach(css => {
			const contents = fs
				.readFileSync(`dist/packages/ragemp_server/cef/${css}`, "utf-8")
				.replace(/[\n\r]/g, " ");

			blob += `"${css}": '${contents}',\n`;
		});
		blob += "}\n";
		fs.writeFileSync(
			"dist/packages/ragemp_server/bootstrap/css.js", blob);

		glob("**/*.js", { cwd: "dist/packages/ragemp_server/bootstrap" }, (err, files) => {
			const blob = obfuscateClientFiles(files);
			fs.writeFileSync("dist/client_packages/index.js", blob);
			if (then) then();
		});
	});
};

const obfuscateServerFiles = (then) => {
	if (debug) {
		if (then) then();
		return;
	}

	console.log("Obfuscating server files");
	const obfuscator = require("javascript-obfuscator");
	glob("**/*js", { cwd: "dist/packages/ragemp_server" }, (err, files) => {
		files.forEach(f => {
			const blob = fs.readFileSync(`dist/packages/ragemp_server/${f}`, "utf-8");
			const res = obfuscator.obfuscate(blob, {
				compact: false,
				controlFlowFlattening: true
			});
			fs.writeFileSync(`dist/packages/ragemp_server/${f}`, res.getObfuscatedCode());
		});
		if (then) then();
	});
};

const serverFiles = (then) => {
	console.log("Creating server files");
	cpdir("src/server", "dist/packages/ragemp_server");
	cp("resources/config.js", "dist/packages/ragemp_server/config.js");

	// bundle the server modules with their deps
	let b = browserify();
	b.add("src/server/lib/voice/index.js");
	b.bundle((err, buf) => {
		if (err) console.log(err);
		fs.writeFileSync("dist/packages/ragemp_server/modules/voice.js", buf.toString());
		b = browserify();
		b.add("src/server/crypt/index.js");
		b.bundle((err, buf) => {
			if (err) console.log(err);
			fs.writeFileSync("dist/packages/ragemp_server/modules/crypt.js", buf.toString());

			// for some reason, browserify does not include readline.js
			let blob = fs.readFileSync("node_modules/readline/readline.js", "utf-8");
			blob += fs.readFileSync("src/server/console/index.js", "utf-8");
			fs.writeFileSync("dist/packages/ragemp_server/modules/console.js", blob.toString());
			obfuscateServerFiles(then);
		});
	});
};

// bundle the cef files. These are still sent dynamically
const cefFiles = (then) => {
	console.log("Creating CEF files");
	const l = child_process.execSync("html-inline -i src/client/cef/login/login.html").toString();
	fs.writeFileSync("dist/client_packages/cef/login.html", l);
	const s = child_process.execSync("html-inline -i src/client/cef/speedo/speedo.html").toString();
	fs.writeFileSync("dist/client_packages/cef/speedo.html", s);

	// these will get bundled into the client index.js
	let c = child_process.execSync("html-inline -i src/client/cef/chat/left.css").toString();
	fs.writeFileSync("dist/packages/ragemp_server/cef/left.css", c);
	c = child_process.execSync("html-inline -i src/client/cef/chat/bottom.css").toString();
	fs.writeFileSync("dist/packages/ragemp_server/cef/bottom.css", c);
	c = child_process.execSync("html-inline -i src/client/cef/chat/right.css").toString();
	fs.writeFileSync("dist/packages/ragemp_server/cef/right.css", c);

	if (then) then();
}

const copyLanguages = (then) => {
	cpdir("src/client/language", "dist/client_packages/language");
	if (then) then();
}

// We don't need the bootstrap directory any more
const cleanup = (then) => {
	console.log("Cleaning up");
	const rimraf = require("rimraf");
	rimraf("dist/packages/ragemp_server/bootstrap", (er) => {
		if (er) console.log("got error cleaning up: ", er);
	});
	rimraf("dist/packages/ragemp_server/cef", (er) => {
		if (er) console.log("got error cleaning up: ", er);
	});
	rimraf("dist/packages/ragemp_server/lib", (er) => {
		if (er) console.log("got error cleaning up: ", er);
	});

	if (debug) fs.writeFileSync("dist/debug", "debug");
	else if (then) then();

	console.log("Done");
};
if (debug) console.log("WARNING: this is a debug build");

createDirectories(
	() => clientModules(
	() => cefFiles(
	() => createClientModulesJSON(
	() => serverFiles(
	() => copyLanguages(
	() => cleanup(
)))))));
